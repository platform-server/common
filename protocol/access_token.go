package protocol

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"strconv"
	"strings"
	"time"
)

var (
	kDefaultTokenLifetime = time.Duration(4) * time.Hour
	kDefaultTokenSalt     = []byte("pt@*a!cd)~")
)

type AccessToken interface {
	String() string

	GetPtId() string
	GetHmac() []byte
	GetUser() string
	GetGameId() string
	GetGameVersion() string
	GetDevice() string
	GetClientId() string
	GetScopes() []string
	IsIncludeAnyScope(scopes ...string) bool
}

type tDefaultAccessToken struct {
	raw         string
	ptId        string
	hmac        []byte
	scopes      []string
	user        string
	gameId      string
	gameVersion string
	device      string
	expire      time.Time
}

func NewDefaultToken(pt, client, scopes, user, device string, expire time.Time) (AccessToken, error) {

	parts := strings.SplitN(client, ":", 2)
	if len(parts) != 2 {
		return nil, Error{ErrInvalidParams, "invalid-client-id."}
	}

	raw := fmt.Sprintf("%s,%s,%s,%s,%s,%d",
		pt, client, scopes, user, device, expire.UTC().Unix())

	encoder := hmac.New(sha1.New, kDefaultTokenSalt)
	encoder.Write([]byte(raw))

	return &tDefaultAccessToken{
		raw:         raw,
		ptId:        pt,
		scopes:      strings.Split(scopes, " "),
		gameId:      parts[0],
		gameVersion: parts[1],
		user:        user,
		device:      device,
		expire:      expire,
		hmac:        encoder.Sum(nil),
	}, nil
}

//Obsolete 此方法在升级到动态token验证时已经失效。因为要兼容升级之前的token所以才保留。
//如果要解析一个token只获取其中属性而不判断是否合法或过期，则用 protocol.ParseAccessTokenBase
func ParseAccessToken(raw string) (AccessToken, error) {

	parts := strings.Split(raw, "|")
	if len(parts) != 2 {
		return nil, Error{ErrInvalidAccessToken, "invalid-default-access-token."}
	}

	v := strings.Split(parts[0], ",")
	if len(v) != 6 {
		return nil, Error{ErrInvalidAccessToken, "invalid-default-access-token."}
	}

	unixsec, err := strconv.ParseInt(v[5], 10, 64)
	if err != nil {
		return nil, Error{ErrInvalidAccessToken, "invalid-default-access-token."}
	}

	if unixsec <= time.Now().UTC().Unix() {
		return nil, Error{ErrInvalidAccessToken, "expired-access-token."}
	}

	expire := time.Unix(unixsec, 0)
	token, err := NewDefaultToken(v[0], v[1], v[2], v[3], v[4], expire)
	if err != nil {
		return nil, err
	}

	hash, err := hex.DecodeString(parts[1])
	if err != nil || !hmac.Equal(token.GetHmac(), hash) {
		return nil, Error{ErrInvalidAccessToken, "failed-checksum."}
	}

	return token, nil
}

func ParseAccessTokenBase(raw string) (AccessToken, error) {

	parts := strings.Split(raw, "|")
	if len(parts) != 2 {
		return nil, Error{ErrInvalidAccessToken, "invalid-default-access-token."}
	}

	v := strings.Split(parts[0], ",")
	if len(v) != 6 {
		return nil, Error{ErrInvalidAccessToken, "invalid-default-access-token."}
	}

	unixsec, err := strconv.ParseInt(v[5], 10, 64)
	if err != nil {
		return nil, Error{ErrInvalidAccessToken, "invalid-default-access-token."}
	}

	expire := time.Unix(unixsec, 0)
	token, err := NewDefaultToken(v[0], v[1], v[2], v[3], v[4], expire)
	if err != nil {
		return nil, err
	}

	return token, nil
}

// GetGameId returns game_id from access_token string without verifying validation
func GetTokenGameId(raw string) string {
	parts := strings.Split(raw, ",")
	if len(parts) != 6 {
		return ""
	}
	return strings.Split(parts[1], ":")[0]
}

func (self *tDefaultAccessToken) String() string {

	return fmt.Sprintf("%s|%s", self.raw, hex.EncodeToString(self.hmac))
}

func (self *tDefaultAccessToken) IsIncludeAnyScope(scopes ...string) bool {

	for _, scope := range scopes {
		for _, has := range self.scopes {
			if scope == has {
				return true
			}
		}
	}

	return false
}

func (self *tDefaultAccessToken) GetPtId() string {
	return self.ptId
}

func (self *tDefaultAccessToken) GetUser() string {
	return self.user
}

func (self *tDefaultAccessToken) GetGameId() string {
	return self.gameId
}

func (self *tDefaultAccessToken) GetGameVersion() string {
	return self.gameVersion
}

func (self *tDefaultAccessToken) GetHmac() []byte {
	return self.hmac
}

func (self *tDefaultAccessToken) GetClientId() string {
	return self.gameId + ":" + self.gameVersion
}

func (self *tDefaultAccessToken) GetDevice() string {
	return self.device
}

func (self *tDefaultAccessToken) GetScopes() []string {
	return self.scopes
}
