package protocol

import (
	"git.tap4fun.com/platform-server/common/testify/require"
	"testing"
)

func TestGetTokenGameId(t *testing.T) {
	token1 := "1234567890,pf:1.0.0,game:test,auth,,1751184955|696f36d8f72a21591a200c849819962f6a4f5f50"
	token2 := "abcd"
	require.Equal(t, "pf", GetTokenGameId(token1))
	require.Equal(t, "", GetTokenGameId(token2))

}

func TestParseAccessTokenBase(t *testing.T) {
	token1 := "lU9fzkWH8o,G301:0.0.1b,auth chat pay,anonymous:nameaaa,none,1452393535|709459999318c837e06f31335b3dd3d040ea1296"
	token, err := ParseAccessTokenBase(token1)
	if err != nil {
		t.Errorf("parse access token fail %s", err.Error())
	}
	require.Equal(t, "G301", token.GetGameId())
	require.Equal(t, "anonymous:nameaaa", token.GetUser())
}
