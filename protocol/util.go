package protocol

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

const (
	USERTYPE_ANONYMOUS = "anonymous"
)

// look up gameId from clientId. current format of clientId: <game id>:<version>
func LookupGameId(clientId string) string {

	parts := strings.SplitN(clientId, ":", 2)
	if len(parts) != 2 {
		return ""
	}
	return parts[0]
}

// look up game version from clientId. current format of clientId: <game id>:<version>
func LookupGameVersion(clientId string) string {

	parts := strings.SplitN(clientId, ":", 2)
	if len(parts) != 2 {
		return ""
	}
	return parts[1]
}

func ParseUser(user string) (usertype, username string) {
	tmp := strings.SplitN(user, ":", 2)
	if len(tmp) != 2 {
		return
	}
	usertype, username = tmp[0], tmp[1]
	return
}

func CombineUser(usertype, username string) (user string) {
	return fmt.Sprintf("%s:%s", usertype, username)
}

func IsUserAnonymous(user string) bool {
	usertype, _ := ParseUser(user)
	return usertype == USERTYPE_ANONYMOUS
}
func ToString(v interface{}) (string, error) {
	switch v := v.(type) {
	case int:
		return strconv.Itoa(v), nil
	case string:
		return v, nil
	default:
		return "error", errors.New("Covert to string error")
	}
}
