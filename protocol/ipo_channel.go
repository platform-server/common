package protocol

const (
	ChannelIOS         = 1
	ChannelGplay       = 2
	ChannelAWS         = 3
	Channel360         = 4
	ChannelAlipay      = 5
	ChannelMycard      = 6
	ChannelUc          = 10
	ChannelBaidu       = 11
	ChannelMobo        = 12
	ChannelBazzare     = 13
	ChannelFortumo     = 14
	ChannelPaypal      = 15
	ChannelPaymentwall = 16
	ChannelLongyuan    = 17
	ChannelYYB         = 18
	ChannelXiaomi      = 19
	ChannelHuawei      = 20
	ChannelVivo        = 21
	ChannelDangle      = 24
	ChannelOppo        = 26
	Channel1SDK        = 44
	ChannelWeixin      = 49
	ChannelFenghuang   = 51
	ChannelMOL         = 52
	ChannelBluepay     = 53
)

var ipo_channel_name_map = map[int]string{
	ChannelIOS:         "ios",
	ChannelGplay:       "gplay",
	ChannelAWS:         "aws",
	Channel360:         "360",
	ChannelAlipay:      "alipay",
	ChannelMycard:      "mycard",
	ChannelUc:          "uc",
	ChannelBaidu:       "baidu",
	ChannelMobo:        "mobo",
	ChannelBazzare:     "bazzare",
	ChannelFortumo:     "fortumo",
	ChannelPaypal:      "paypal",
	ChannelPaymentwall: "paymentwall",
	ChannelLongyuan:    "longyuan",
	ChannelYYB:         "yingyongbao",
	ChannelXiaomi:      "xiaomi",
	ChannelHuawei:      "huawei",
	ChannelVivo:        "vivo",
	ChannelDangle:      "dangle",
	ChannelOppo:        "oppo",
	Channel1SDK:        "1sdk",
	ChannelWeixin:      "weixin",
	ChannelFenghuang:   "fenghuang",
	ChannelMOL:         "mol",
	ChannelBluepay:     "bluepay",
}

func IpoChanneName(channel int) string {
	name, _ := ipo_channel_name_map[channel]
	return name
}
