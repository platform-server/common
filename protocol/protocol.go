package protocol

type PtRsp interface {
	Marshal() []byte
}