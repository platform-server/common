/*
* @Author: Jingkai Mao (oammix@gmail.com)
* @Date:   2015-01-23 17:02:08
 */

package protocol

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"strconv"
	"strings"
)

type tAccessTokenEx struct {
	raw         string
	ptId        string
	hmac        []byte
	scopes      []string
	user        string
	gameId      string
	device      string
	gameVersion string
	serial      int64
}

func NewDynamicalToken(pt, client, scopes, user, device string, serial int64, hmac []byte) (AccessToken, error) {

	parts := strings.SplitN(client, ":", 2)
	if len(parts) != 2 {
		return nil, Error{ErrInvalidParams, "invalid-client-id."}
	}

	raw := fmt.Sprintf("%s,%s,%s,%s,%s,%d", pt, client, scopes, user, device, serial)
	return &tAccessTokenEx{
		raw:         raw,
		ptId:        pt,
		scopes:      strings.Split(scopes, " "),
		gameId:      parts[0],
		gameVersion: parts[1],
		user:        user,
		device:      device,
		serial:      serial,
		hmac:        hmac}, nil
}

func NewAccessTokenEx(pt, client, scopes, user, device string, serial int64, key string) (AccessToken, error) {

	parts := strings.SplitN(client, ":", 2)
	if len(parts) != 2 {
		return nil, Error{ErrInvalidParams, "invalid-client-id."}
	}

	raw := fmt.Sprintf("%s,%s,%s,%s,%s,%d", pt, client, scopes, user, device, serial)
	h := hmac.New(sha1.New, []byte(key))
	h.Write([]byte(raw))
	hmac := h.Sum(nil)
	return &tAccessTokenEx{
		raw:         raw,
		scopes:      strings.Split(scopes, " "),
		gameId:      parts[0],
		gameVersion: parts[1],
		user:        user,
		device:      device,
		serial:      serial,
		hmac:        hmac}, nil
}

// parse accesstoken string to accesstoken object
func ParseAccessTokenEx(raw string, salt string) (AccessToken, error) {

	parts := strings.Split(raw, "|")
	if len(parts) != 2 {
		return nil, Error{ErrInvalidAccessToken, "invalid-access-token-ex."}
	}

	v := strings.Split(parts[0], ",")
	if len(v) != 6 {
		return nil, Error{ErrInvalidAccessToken, "invalid-access-token-ex."}
	}

	serial, err := strconv.ParseInt(v[5], 10, 64)
	if err != nil {
		return nil, Error{ErrInvalidAccessToken, "invalid-serial-number."}
	}

	encoder := hmac.New(sha1.New, []byte(salt))
	encoder.Write([]byte(parts[0]))
	hash, err := hex.DecodeString(parts[1])
	if err != nil || !hmac.Equal(encoder.Sum(nil), hash) {
		return nil, Error{ErrInvalidAccessToken, "failed-checksum."}
	}

	return NewDynamicalToken(v[0], v[1], v[2], v[3], v[4], serial, hash)
}

func GetSerialFromAccessTokenEx(raw string) (int64, error) {

	parts := strings.Split(raw, "|")
	if len(parts) != 2 {
		return 0, Error{ErrInvalidAccessToken, "invalid-access-token-ex."}
	}

	v := strings.Split(parts[0], ",")
	if len(v) != 6 {
		return 0, Error{ErrInvalidAccessToken, "invalid-access-token-ex."}
	}

	serial, err := strconv.ParseInt(v[5], 10, 64)
	if err != nil {
		return 0, Error{ErrInvalidAccessToken, "invalid-serial-number."}
	}

	return serial, nil
}

func (self *tAccessTokenEx) String() string {
	return fmt.Sprintf("%s|%s", self.raw, hex.EncodeToString(self.hmac))
}

func (self *tAccessTokenEx) IsIncludeAnyScope(scopes ...string) bool {

	for _, scope := range scopes {
		for _, has := range self.scopes {
			if scope == has {
				return true
			}
		}
	}

	return false
}

func (self *tAccessTokenEx) GetPtId() string {
	return self.ptId
}

func (self *tAccessTokenEx) GetUser() string {
	return self.user
}

func (self *tAccessTokenEx) GetGameId() string {
	return self.gameId
}

func (self *tAccessTokenEx) GetGameVersion() string {
	return self.gameVersion
}

func (self *tAccessTokenEx) GetHmac() []byte {
	return self.hmac
}

func (self *tAccessTokenEx) GetClientId() string {
	return self.gameId + ":" + self.gameVersion
}

func (self *tAccessTokenEx) GetDevice() string {
	return self.device
}

func (self *tAccessTokenEx) GetScopes() []string {
	return self.scopes
}
