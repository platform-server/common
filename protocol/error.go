/*
* @Author: Jingkai Mao (oammix@gmail.com)
* @Date:   2015-01-08 14:59:48
 */

package protocol

import (
	"encoding/json"
	"fmt"
)

type Errcode int

const (
	// general codes
	Ok                           Errcode = 1000
	ErrConn3rdServer                     = 2009
	ErrInvalidParams                     = 5000
	ErrNoRequiredParams                  = 5001
	ErrInvalidAccessToken                = 5002
	ErrPermissionDenied                  = 5003
	ErrInternal                          = 5004
	ErrNoRecords                         = 5005
	ErrInvalidCaptcha                    = 5006
	ErrInvalidCaptchaNeedRefresh         = 5007
	ErrTimeout                           = 5008
	ErrUnknownResponse                   = 5009
	ErrJsonMarshalFailed                 = 5010
	ErrJsonUnmarshalFailed               = 5011
	ErrDatabaseSelectFailed              = 5012
	ErrZookeeperFailed                   = 5013
	ErrNotMatch                          = 5014
	ErrFailedNetworkRequest              = 5015
	ErrAlreadyExists                     = 5016
	ErrNotSupport                        = 5017
	ErrServiceUnavailable                = 5018
	ErrAborted                           = 5019
	// ErrServiceUnavailable           = 5006
	// ErrAborted                      = 5007
)

// standard error of platform
type Error struct {
	Code Errcode `json:"error_code"`
	Hint string  `json:"error_msg"`
}

// Error returns a formated string in json.
func (self Error) Error() string {
	return self.Marshal()
}

func (self Error) Marshal() string {
	bytes, _ := json.Marshal(self)
	return string(bytes)
}

func NewError(code Errcode, msg string) Error {
	return Error{Code: code, Hint: msg}
}

func MarshalHttpErrorRsp(code Errcode, msg string) []byte {
	return []byte(MarshalError2Response(NewError(code, msg)))
}

// MarshalError converts error to a formated string json.
func MarshalError2Response(err error) string {

	switch err := err.(type) {
	case Error:
		return err.Marshal()
	default:
		return fmt.Sprintf("{ \"error_code\" : %d, \"error_msg\" : \"%s\" }", ErrInternal, err.Error())
	}
}

func UnmarshalResponse2Error(rsp string) Error {

	var ret Error
	err := json.Unmarshal([]byte(rsp), &ret)
	if err != nil {
		return Error{ErrJsonUnmarshalFailed, rsp}
	}
	return ret
}

func MakeSimpleResponse(rsp bool) string {
	return fmt.Sprintf("{ \"success\" : %t }", rsp)
}
