package launch

import (
	"errors"
	framework "git.tap4fun.com/platform-server/rpcframework/lib/rpcframework"
	"time"
)

type FrameworkStatsd struct {
}

func (this *FrameworkStatsd) init(context Context) (bool, error) {

	if !context.PFStatstSet() {
		GetLogger().ILog("not need to init statsd")
		return true, nil
	}

	statsd_section := "framework-statsd"
	projectName := context.ProjectName()

	if projectName == "" {
		GetLogger().ELog("project name failed")
		return false, errors.New("project name failed")
	}

	addr := framework.AssertGetRemoteConfig(statsd_section, "Address")
	itrv := framework.AssertGetRemoteConfigInt64(statsd_section, "Interval")
	tval := framework.AssertGetRemoteConfigInt64(statsd_section, "Timeout")

	tout := time.Duration(tval) * time.Millisecond
	ival := time.Duration(itrv) * time.Millisecond
	framework.AssertInit(framework.InitStatsDeamon2(projectName, addr, tout, ival))

	if g_config.HasSection("Framework-StatReq") {
		itrv := framework.AssertGetRemoteConfigInt64("Framework-StatReq", "Interval")
		ival := time.Duration(itrv) * time.Millisecond
		framework.AssertInit(framework.InitStatReqDeamon(projectName, ival))
	} else {
		framework.AssertInit(framework.InitStatReqDeamon(projectName, ival))
	}

	return true, nil
}
