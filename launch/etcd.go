package launch

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"git.tap4fun.com/platform-server/common/netext"
	"github.com/coreos/etcd/clientv3"
	"golang.org/x/net/context"
	"strings"
	"text/template"
	"time"
)

var EtcdStore *FrameworkEtcdStore

func GetEtcdInstace() *clientv3.Client {

	if EtcdStore != nil {
		return EtcdStore.etcd_client
	}
	return nil
}

type FrameworkEtcdStore struct {
	etcd_client *clientv3.Client
}

func (this *FrameworkEtcdStore) init(context Context) (bool, error) {

	if !context.PFEtcdSet() {
		GetLogger().ILog("not need to init etcd")
		return true, nil
	}

	endpoints, user, pwd := context.EtcdStoreAddress()

	if endpoints == nil || len(endpoints) == 0 {
		return false, errors.New("etcd node is empty")
	}

	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   endpoints,
		DialTimeout: 5 * time.Second,
		Username:    user,
		Password:    pwd,
	})

	if err != nil {
		return false, err
	}

	this.etcd_client = cli
	EtcdStore = this
	return true, nil
}

func (this *FrameworkEtcdStore) register(c Context) (bool, error) {

	if !c.PFEtcdSet() {
		GetLogger().ILog("not need to init etcd")
		return true, nil
	}

	projectName := c.ProjectName()
	if projectName == "" {
		GetLogger().ELog("project name is empty")
		return false, errors.New("empty project name")
	}

	port := c.ListenPort()
	if port == 0 {
		return false, errors.New("invalid port")
	}

	address, err := this.getLocalAddress()
	if err != nil {
		GetLogger().ELog("get local address failed(%+v)", err)
		return false, errors.New("local address err")
	}

	addr := fmt.Sprintf("%s:%d", address, port)

	GetLogger().ILog("start register node")
	if err = this.registerNode(addr, projectName); err != nil {
		return false, err
	}

	GetLogger().ILog("start register node meta")
	if err = this.registerNodeMeta(addr, projectName); err != nil {
		return false, err
	}

	GetLogger().ILog("start register cluster")
	if err = this.registerCluster(projectName); err != nil {
		return false, err
	}

	GetLogger().ILog("start register cluster meta")
	if err = this.registerClusterMeta(projectName); err != nil {
		return false, err
	}

	GetLogger().ILog("start register api")
	if err = this.registerApi(projectName); err != nil {
		return false, err
	}

	GetLogger().ILog("start register api meta")
	if err = this.registerApiMeta(projectName); err != nil {
		return false, err
	}

	GetLogger().ILog("register service to etcd success")
	return true, nil
}

func (this *FrameworkEtcdStore) getLocalAddress() (string, error) {

	localAddr := GetGlobalConfig().GetString("framework-local", "local_address")
	url, err := netext.GetRedirectLocalUrl(localAddr)

	if err != nil {
		url, err = netext.GetExternalIpv4Address()
		if err != nil {
			return "", nil
		}
	}

	return strings.Trim(url, "/"), nil
}

func (this *FrameworkEtcdStore) makeValueTemplate(templateContent string, project, addr string) (string, error) {

	type TemplateStruct struct {
		ProjectName string
		Addr        string
	}

	ts := &TemplateStruct{
		ProjectName: project,
		Addr:        addr,
	}

	t := template.Must(template.New("template").Parse(templateContent))
	buffer := &bytes.Buffer{}

	if err := t.Execute(buffer, ts); err != nil {
		GetLogger().ELog("execute template failed(%+v)", err)
		return "", err
	}

	return string(buffer.Bytes()), nil

}

func (this *FrameworkEtcdStore) readEtcdValue(path string) ([]byte, error) {
	rsp, err := this.etcd_client.Get(context.Background(), path)
	if err != nil {
		GetLogger().ELog("read etcd path(%+s) value failed with error(%+v)", path, err)
		return nil, err
	}

	if rsp.Count <= 0 {
		return nil, nil
	}
	//因为这里是使用的绝对路径的方式可以保证值要么是1要么是0
	return rsp.Kvs[0].Value, nil
}

func (this *FrameworkEtcdStore) writeEtcdValue(path string, value string) error {
	if _, err := this.etcd_client.Put(context.Background(), path, value); err != nil {
		GetLogger().ELog("write value to etcd store(%s) failed(%+v)", path, err)
		return err
	}
	return nil
}

func (this *FrameworkEtcdStore) convertStruct(data []byte, v interface{}) error {
	if err := json.Unmarshal(data, v); err != nil {
		GetLogger().ELog("convert data(%s) to struct failed(%+v)", data, v)
		return err
	}
	return nil
}

func (this *FrameworkEtcdStore) regsiterValue(path, pv string) error {
	v, err := this.readEtcdValue(path)
	if err != nil {
		return err
	}
	vlist := make([]string, 0)
	if v != nil {
		if err = this.convertStruct(v, &vlist); err != nil {
			return err
		}
	}

	for _, vl := range vlist {
		//若已经存在相同的v
		if vl == pv {
			return nil
		}
	}

	vlist = append(vlist, pv)
	v, _ = json.Marshal(vlist)
	return this.writeEtcdValue(path, string(v))
}

func (this *FrameworkEtcdStore) registerMetaValue(path string, templatepath, project, addr string) error {
	v, err := this.readEtcdValue(path)
	//etcd发生了错误
	if err != nil {
		return err
	}
	//该路径下已经存在了值
	if v != nil {
		return nil
	}

	//获取模板数据
	value, _ := this.makeValueTemplate(templatepath, project, addr)
	//写入etcd
	return this.writeEtcdValue(path, value)
}

//register to path /api_route/apis
func (this *FrameworkEtcdStore) registerApi(projectName string) error {
	return this.regsiterValue(API_PATH, projectName)
}

//register api meta to etcd
func (this *FrameworkEtcdStore) registerApiMeta(projectName string) error {
	path := API_META_PREFIX_PATH + "/" + projectName
	return this.registerMetaValue(path, API_ROUTER_API, projectName, "")
}

func (this *FrameworkEtcdStore) registerCluster(projectName string) error {
	return this.regsiterValue(CLUSTER_PATH, projectName)
}

func (this *FrameworkEtcdStore) registerClusterMeta(projectName string) error {
	path := CLUSTER_META_PREFIX_PATH + "/" + projectName
	return this.registerMetaValue(path, API_ROUTER_CLUSTER, projectName, "")
}

func (this *FrameworkEtcdStore) registerNode(addr, projectName string) error {
	path := SERVICE_PATH + "/" + projectName
	return this.regsiterValue(path, addr)
}

func (this *FrameworkEtcdStore) registerNodeMeta(addr, projectName string) error {
	path := SERVICE_META_PREFIX_PATH + "/" + projectName + "/" + addr
	return this.registerMetaValue(path, API_ROUTER_SERVICE, projectName, addr)
}
