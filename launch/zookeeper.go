package launch

import (
	"git.tap4fun.com/platform-server/common/zookeeper"
	//"net"
	"strconv"
	"strings"
	"time"
)

type FrameworkZKeeper struct {
	zk_client zookeeper.ZookeeperClient
}

var g_zookeeper *FrameworkZKeeper

func (this *FrameworkZKeeper) init(context Context) (bool, error) {

	if !context.PFZKSet() {
		GetLogger().ILog("not need to init zookeeper")
		return true, nil
	}

	zk_section := "framework-zookeeper"

	zklists := g_config.GetString(zk_section, "zklist")
	zkUsername := g_config.GetString(zk_section, "username")
	zkPasswd := g_config.GetString(zk_section, "password")
	zkTimeout := g_config.GetInt(zk_section, "timeout")
	localAddr := g_config.GetString("framework-local", "local_address")
	client, err := zookeeper.NewClient(strings.Split(zklists, ","), zkUsername, zkPasswd, time.Duration(zkTimeout)*time.Millisecond, localAddr, g_config.conf)
	if err != nil {
		GetLogger().Errorf("Init zookeeper err:%s", err.Error())
		return false, err
	}
	this.zk_client = client
	g_zookeeper = this

	return true, nil
}

func GetZookeeper() zookeeper.ZookeeperClient {
	if g_zookeeper == nil {
		return zookeeper.Noop()
	}
	return g_zookeeper.zk_client
}

// RegisterService register service's name as znode to zookeeper in empheral mode,
// send heartbeat every second to keep node from expired.
func (this *FrameworkZKeeper) RegisterService(context Context) error {

	if !context.PFZKSet() {
		GetLogger().ILog("do not need register server to zk")
		return nil
	}

	projectName := context.ProjectName()
	version := context.Version()
	port := strconv.Itoa(context.ListenPort())
	servType := context.Schema()
	GetLogger().ILog("begin register zookeeper %s:%s:%s:%s", servType, projectName, version, port)
	if err := this.zk_client.RegisterService(projectName, version, servType, port, true); err != nil {
		GetLogger().ELog("register zookeeper error %s", err.Error())
		return err
	}

	return nil
}
