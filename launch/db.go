package launch

import (
	"fmt"
	"git.tap4fun.com/platform-server/common/database"
	"github.com/jinzhu/gorm"
	//_ "github.com/jinzhu/gorm/dialects/mysql"
)

type FrameworkGormDB struct {
	db *gorm.DB
}

func (this *FrameworkGormDB) init(context Context) (bool, error) {

	if !context.PFDBSet() {
		GetLogger().ILog("not need to init db")
		return true, nil
	}

	user, pwd, ipaddress, dbs, port := context.DbAddress()

	//ip, port, user, password, dbname
	db, err := database.NewGormDB1(ipaddress, fmt.Sprintf("%d", port), user, pwd, dbs)
	// addr := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?%s", user,
	// 	pwd, ipaddress, port, dbs, "charset=utf8&parseTime=True")
	// db, err := gorm.Open("mysql", addr)
	if err != nil {
		GetLogger().Errorf("gorm open faild(%s,%s,%s,%s,%d),failed(%s)", user, pwd, ipaddress, dbs, port, err)
		return false, err
	}
	this.db = db
	g_gorm_db = this
	return true, nil
}

var g_gorm_db *FrameworkGormDB

func GetDBClient() *gorm.DB {
	if g_gorm_db != nil {
		return g_gorm_db.db
	}
	return nil
}
