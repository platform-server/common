package launch

import (
	"fmt"
	"git.tap4fun.com/platform-server/common/config"
	framework "git.tap4fun.com/platform-server/rpcframework/lib/rpcframework"
	"os"
	"path/filepath"
)

//全局的配置的配置对象
var g_config *FrameworkConfig

func GetGlobalConfig() *FrameworkConfig {
	return g_config
}

func GetConfigs() goconfig.IConfig {
	return g_config.cfg
}

type FrameworkConfig struct {
	conf *goconfig.RemoteConfig
	cfg  goconfig.IConfig
}

func (this *FrameworkConfig) ConfPath(context Context) string {
	dir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	dir, _ = filepath.Split(dir)
	config_dir := filepath.Join(dir, "conf")
	return fmt.Sprintf("%s/%s.conf", config_dir, context.ProjectName())
}

func (this *FrameworkConfig) init(context Context) (bool, error) {

	if context.PFConfigSet() {
		path := this.ConfPath(context)
		proName := context.ProjectName()
		version := context.Version()
		framework.AssertInit(framework.InitConfigFromFile(path))
		framework.AssertInit(framework.InitRemoteConfig(proName, version, framework.GetConfigs()))
		framework.AssertInit(framework.InitBootManager(proName, version, framework.GetConfigs()))
	}
	this.conf = framework.GetRemoteConfigs()
	this.cfg = framework.GetConfigs()
	framework.DisplayConfigs()
	g_config = this
	return true, nil
}

func (this *FrameworkConfig) HasSection(section string) bool {
	return this.conf.HasSection(section)
}

func (this *FrameworkConfig) GetString(section string, key string) string {
	v, _ := this.conf.GetString(section, key)
	return v
}

func (this *FrameworkConfig) GetInt(section string, key string) int {
	v, _ := this.conf.GetInt(section, key)
	return v
}
