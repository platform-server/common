package launch

import (
	"git.tap4fun.com/platform-server/common/logext"
	"git.tap4fun.com/platform-server/common/logext/forward"
	framework "git.tap4fun.com/platform-server/rpcframework/lib/rpcframework"
)

// var g_logger logext.Logger = logext.Noop()

func InitStdLogger(level logext.LogLevel, stack int) error {

	// logger, err := logext.NewStdLogger(level, stack)
	// if err != nil {
	// 	return err
	// }

	// g_logger = logger
	// forward.SetGlobalLogger(logger)
	return framework.InitStdLogger(level, stack)
}

func GetLogger() logext.Logger {
	return forward.GetGlobalLogger()
}
