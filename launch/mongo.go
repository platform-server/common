package launch

import (
	"gopkg.in/mgo.v2"
)

type FrameworkMongo struct {
	session *mgo.Session
}

var mongo_instance *FrameworkMongo

func (this *FrameworkMongo) init(context Context) (bool, error) {

	if !context.PFMongoSet() {
		GetLogger().ILog("not need to init mongo")
		return true, nil
	}

	session, err := mgo.Dial(context.MongoUrl())

	if err != nil {
		GetLogger().ELog("framework mongo dial failed, err(%+v)", err)
		return false, err
	}

	this.session = session
	mongo_instance = this
	return true, nil
}

func (this *FrameworkMongo) GetSession() *mgo.Session {
	return this.session
}

func GetMongoClient() *FrameworkMongo {
	return mongo_instance
}

func UpdateMongoSession(url string) (*FrameworkMongo, error) {

	session, err := mgo.Dial(url)

	if err != nil {
		GetLogger().ELog("update mongo session failed, err(%+v)", err)
		return nil, err
	}

	if mongo_instance != nil {
		mongo_instance.session = session
	} else {
		mongo_instance = &FrameworkMongo{
			session: session,
		}
	}

	return mongo_instance, nil
}
