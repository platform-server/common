package launch

import (
	"gopkg.in/redis.v3"
)

type FrameworkRedis struct {
	redis_client *redis.Client
}

func (this *FrameworkRedis) init(context Context) (bool, error) {

	if !context.PFRedisSet() {
		GetLogger().ILog("not need to init statsd")
		return true, nil
	}

	host, pwd, db := context.RedisAddress()

	if db <= 0 || host == "" {
		GetLogger().WLog("redis is not require")
		return true, nil
	}

	redis_client := redis.NewClient(&redis.Options{
		Addr:     host,
		Password: pwd,
		DB:       int64(db),
	})

	if _, err := redis_client.Ping().Result(); err != nil {
		GetLogger().WLog("framework init redis failed , err(%+v)", err)
		return false, err
	}

	this.redis_client = redis_client
	g_redis_instance = this
	return true, nil
}

var g_redis_instance *FrameworkRedis

func GetRedisClient() *redis.Client {
	if g_redis_instance != nil {
		return g_redis_instance.redis_client
	}
	return nil
}
