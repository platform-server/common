package launch

const API_ROUTER_SERVICE = `
{
	"name":"{{.ProjectName}}",
	"addr": "{{.Addr}}",
	"checkPath": "info",
	"checkDuration": 10,
	"weight": 1,
	"maxQPS":10000
}
`
const API_ROUTER_API = `
{
	"name":"{{.ProjectName}}",
	"status" : "up"
}
`

const API_ROUTER_CLUSTER = `
{
	"name":"{{.ProjectName}}"
}
`

//api meta元素的配置路径
const API_PATH = "api_route/apis"
const API_META_PREFIX_PATH = "api_route/apis/meta"

//cluster的配置路径
const CLUSTER_PATH = "api_route/cluster"
const CLUSTER_META_PREFIX_PATH = "api_route/cluster/meta"

//serveres的配置路径
const SERVICE_PATH = "api_route/nodes"
const SERVICE_META_PREFIX_PATH = "api_route/nodes/meta"
