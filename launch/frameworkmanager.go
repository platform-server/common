package launch

import (
	"fmt"
	"git.tap4fun.com/platform-server/common/logext"
	framework "git.tap4fun.com/platform-server/rpcframework/lib/rpcframework"
	"time"
)

type FrameworkManager struct {
	config FrameworkConfig
	statsd *FrameworkStatsd
	zk     *FrameworkZKeeper
	redis  *FrameworkRedis
	db     *FrameworkGormDB
	mongo  *FrameworkMongo
	pfsdk  *FrameworkPfsdk
	etcd   *FrameworkEtcdStore
	tContext
}

func (this *FrameworkManager) OnFrameworkManagerLaunchInit(context Context) error {

	fmt.Printf("start init framework manager\n")

	if success, err := this.config.init(context); !success {
		return err
	}

	this.initLogger(context)

	if GetGlobalConfig().HasSection("framework-statsd") {

		GetLogger().ILog("start init statsd")
		this.statsd = &FrameworkStatsd{}
		if success, err := this.statsd.init(context); !success {
			GetLogger().ELog("init statsd failed ,err(%+v)", err)
			return err
		}
	}

	if GetGlobalConfig().HasSection("framework-zookeeper") {

		GetLogger().ILog("start init zookeeper")
		this.zk = &FrameworkZKeeper{}
		if success, err := this.zk.init(context); !success {
			GetLogger().ELog("init zk failed ,err(%+v)", err)
			return err
		}
	}

	if GetGlobalConfig().HasSection("framework-redis") {

		GetLogger().ILog("start init redis")
		this.redis = &FrameworkRedis{}
		if success, err := this.redis.init(context); !success {
			GetLogger().ELog("init redis failed ,err(%+v)", err)
			return err
		}
	}

	if GetGlobalConfig().HasSection("framework-db") {

		GetLogger().ILog("start init db")
		this.db = &FrameworkGormDB{}
		if success, err := this.db.init(context); !success {
			GetLogger().ELog("init db failed ,err(%+v)", err)
			return err
		}
	}

	if GetGlobalConfig().HasSection("framework-mongo") {

		GetLogger().ILog("start init mongo")
		this.mongo = &FrameworkMongo{}
		if success, err := this.mongo.init(context); !success {
			GetLogger().ELog("init mongo failed ,err(%+v)", err)
			return err
		}

	}

	if GetGlobalConfig().HasSection("framework-etcd") {

		GetLogger().ILog("start init etcd store")
		this.etcd = &FrameworkEtcdStore{}
		if success, err := this.etcd.init(context); !success {
			GetLogger().ELog("init etcd failed ,err(%+v)", err)
			return err
		}

	}
	if GetGlobalConfig().HasSection("framework-pfsdk") {
		GetLogger().ILog("start init pfsdk ")
		this.pfsdk = &FrameworkPfsdk{}
		if success, err := this.pfsdk.init(context); !success {
			GetLogger().ILog("start init pfsdk")
			return err
		}
	}

	//there init key
	if GetGlobalConfig().HasSection("framework-accessKey") {

		if !context.PFAccessKey() {
			GetLogger().ILog("not need init access key")
			return nil
		}

		GetLogger().ILog("start init accessKey")
		localAddr := GetGlobalConfig().GetString("framework-local", "local_address")
		ival := GetGlobalConfig().GetInt("framework-accessKey", "interval")
		framework.InitAccessKeyMgr(time.Duration(ival)*time.Millisecond, localAddr)
	}

	return nil
}

func (this *FrameworkManager) registerService(context Context) error {

	if this.etcd != nil {
		if success, err := this.etcd.register(context); !success {
			GetLogger().WLog("register server etcd failed ,err(%+v)", err)
		}
	}

	if this.zk != nil {
		return this.zk.RegisterService(context)
	} else {
		GetLogger().ILog("zookeeper client is null ")
	}

	return nil
}

func (this *FrameworkManager) OnFrameworkManagerLaunchAfterInit(context Context) error {

	if err := this.registerService(context); err != nil {
		return err
	}

	return nil
}

func (this *FrameworkManager) initLogger(context Context) {
	loggerLevel := context.LogLeverl()
	switch loggerLevel {
	case "":
		InitStdLogger(logext.LogLevelInfo, 0)
	case "Error":
		InitStdLogger(logext.LogLevelError, 0)
	case "Warnin":
		InitStdLogger(logext.LogLevelWarning, 0)
	case "Notice":
		InitStdLogger(logext.LogLevelNotice, 0)
	case "Info":
		InitStdLogger(logext.LogLevelInfo, 0)
	case "Data":
		InitStdLogger(logext.LogLevelData, 0)
	case "Debug":
		InitStdLogger(logext.LogLevelDebug, 0)
	case "Fatal":
		InitStdLogger(logext.LogLevelFatal, 0)
	}
}
