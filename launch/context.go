package launch

import (
	"strconv"
	"strings"
)

type Context interface {
	ProjectName() string
	Version() string
	LogLeverl() string
	DbAddress() (string, string, string, string, int)
	RedisAddress() (string, string, int)
	//DeployEnv() string
	ListenPort() int
	Schema() string
	MongoUrl() string
	EtcdStoreAddress() ([]string, string, string)
	PFStatstSet() bool
	PFZKSet() bool
	PFRedisSet() bool
	PFDBSet() bool
	PFMongoSet() bool
	PFEtcdSet() bool
	PFPFSDKSet() bool
	PFConfigSet() bool
	PFAccessKey() bool
}

type tContext struct {
}

func (this *tContext) ProjectName() string {
	return ""
}

func (this *tContext) Version() string {
	return ""
}

func (this *tContext) LogLeverl() string {
	return "Info"
}

func (this *tContext) DbAddress() (string, string, string, string, int) {
	if g_config != nil {
		if g_config.HasSection("framework-db") {
			user := g_config.GetString("framework-db", "user")
			password := g_config.GetString("framework-db", "password")
			host := g_config.GetString("framework-db", "host")
			db := g_config.GetString("framework-db", "db")
			port := g_config.GetInt("framework-db", "port")
			return user, password, host, db, port
		}
	}
	return "", "", "", "", 0
}

func (this *tContext) RedisAddress() (string, string, int) {
	if g_config != nil {
		if g_config.HasSection("pf-common-redis") {
			addr := g_config.GetString("pf-common-redis", "addr")
			pwd := g_config.GetString("pf-common-redis", "password")
			db := g_config.GetInt("pf-common-redis", "db")
			return addr, pwd, db
		}
	}
	return "", "", 0

}

func (this *tContext) ListenPort() int {
	if GetGlobalConfig().HasSection("framework-server") {
		sport := GetGlobalConfig().GetString("framework-server", "port")
		port, _ := strconv.Atoi(sport)
		GetLogger().ILog("server listen port at(%d)", port)
		return port
	}
	GetLogger().ILog("server listen port at default(%d)", 0)
	return 0
}

func (this *tContext) Schema() string {
	if GetGlobalConfig().HasSection("framework-server") {
		schema := GetGlobalConfig().GetString("framework-server", "schema")
		GetLogger().ILog("server listen schema at(%s)", schema)
		return schema
	}
	GetLogger().ILog("server listen port at default(%d)", "http")
	return "http"
}

func (this *tContext) MongoUrl() string {

	var mongo_addr string = ""

	if GetGlobalConfig().HasSection("framework-mongo") {
		mongo_addr = GetGlobalConfig().GetString("framework-mongo", "mongo-addr")
	}

	return mongo_addr
}

func (this *tContext) EtcdStoreAddress() ([]string, string, string) {
	if GetGlobalConfig().HasSection("framework-etcd") {
		addrs := GetGlobalConfig().GetString("framework-etcd", "addrs")
		user := GetGlobalConfig().GetString("framework-etcd", "user")
		pwd := GetGlobalConfig().GetString("framework-etcd", "pwd")
		return strings.Split(addrs, ","), user, pwd
	}
	return nil, "", ""
}

func (this *tContext) PFStatstSet() bool {
	return true
}
func (this *tContext) PFZKSet() bool {
	return true
}
func (this *tContext) PFRedisSet() bool {
	return true
}
func (this *tContext) PFDBSet() bool {
	return true
}
func (this *tContext) PFMongoSet() bool {
	return true
}
func (this *tContext) PFEtcdSet() bool {
	return true
}

func (this *tContext) PFPFSDKSet() bool {
	return true
}

func (this *tContext) PFConfigSet() bool {
	return true
}

func (this *tContext) PFAccessKey() bool {
	return true
}

// func (this *tContext) DeployEnv() string {
// 	return ENV_NODE_APP
// }
