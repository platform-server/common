package launch

import (
	"errors"
	"fmt"
	// "git.tap4fun.com/platform-server/common/config"
	"git.tap4fun.com/platform-server/pfsdk"
	"git.tap4fun.com/platform-server/pfsdk/log"
	"os"
	"strings"
	"time"
)

type FrameworkPfsdk struct {
	sdk_client *pfsdk.SDKClient
}

func (this *FrameworkPfsdk) init(context Context) (bool, error) {

	if !context.PFPFSDKSet() {
		GetLogger().ILog("not need to init pfsdk")
		return true, nil
	}

	seciont := "framework-pfsdk"
	if g_config == nil {
		GetLogger().WLog("first need init config")
		return false, errors.New("first need init config")
	}
	if !g_config.HasSection(seciont) {
		GetLogger().Warningf("section [framework-pfsdk] not found")
		return true, nil
	}
	var err error
	client_id := g_config.GetString(seciont, "client_id")
	eve_url := g_config.GetString(seciont, "eve_url")
	dc := g_config.GetString(seciont, "dc")
	env := g_config.GetString(seciont, "env")
	servers := g_config.GetString(seciont, "servers")
	// err = framework.GetFirstError(err1, err2, err3, err4, err5)
	if len(client_id)*len(eve_url)*len(dc)*len(env)*len(servers) == 0 {
		err := fmt.Errorf("PFSDK config is empty client_id:%s, eve_url: %s,dc: %s,env:% s,servers: %s",
			client_id, eve_url, dc, env, servers)
		GetLogger().WLog(err.Error())
		return false, err
	}
	var sdk_client *pfsdk.SDKClient

	servers_a := strings.Split(servers, ",")
	logger := log.New(os.Stdout, log.LOG_INFO)

	user := g_config.GetString(seciont, "user")
	password := g_config.GetString(seciont, "password")
	scopes := g_config.GetString(seciont, "scopes")
	if len(user)*len(password)*len(scopes) != 0 {
		scopes_a := strings.Split(scopes, ",")
		sdk_client, err = pfsdk.New(logger, user, password, "", scopes_a,
			client_id, eve_url, dc, env, servers_a, time.Second*5, time.Second*5)
		if err != nil {
			GetLogger().ELog("PFSDK init err:%s", err.Error())
			return false, err
		}
	} else {
		sdk_client, err = pfsdk.NewWithoutLogin(logger,
			client_id, eve_url, dc, env, servers_a, time.Second*5)
		if err != nil {
			GetLogger().ELog("PFSDK init err:%s", err.Error())
			return false, err
		}
	}
	this.sdk_client = sdk_client
	g_pfsdk = this
	return true, nil
}

var g_pfsdk *FrameworkPfsdk

func GetPfsdk() *pfsdk.SDKClient {
	if g_pfsdk != nil {
		return g_pfsdk.sdk_client
	}
	return nil
}
