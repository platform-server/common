#### Logext: Extensions of Package `Log`

logext大致分为两个个部分：

1. logext.go/Logger 描述基本的API；
2. records.go/writes.go/Recorder 描述常用的输出格式，方法；

目前支持3种Logger：

1. Std Logger:
    * 将所有日志默认输出至Stdout，可结合Supervisor进行日志管理、Rotate以及Redirect。
2. Kids Logger:
    * 将所有日志默认输出到Stdout，同时发送一份副本至Kids Agent。
3. File Logger:
    * 将所有日志输出至指定的文件中，可选择带Buffered进行日志缓冲。
    * [TODO] Rotate处理。