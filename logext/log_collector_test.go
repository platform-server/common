package logext

import (
	"database/sql"
	"fmt"
	_ "git.tap4fun.com/platform-server/common/database/mysqldriver"
	"testing"
)

const (
	USER = "root"
	PASS = ""
	HOST = "localhost"
	PORT = 3306
	DB   = "test"
)

func TestLogCollector(t *testing.T) {

	ds := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", USER, PASS, HOST, PORT, DB)
	db, err := sql.Open("mysql", ds)
	if err != nil {
		t.Log("database connection failed\n")
		return
	}

	lost := 0

	collector := NewLogCollector("test", db)
	for i := 0; i < 200000; i++ {
		if !collector.AddDebug("debug") {
			lost++
		}
		if !collector.AddInfo("info") {
			lost++
		}
		if !collector.AddNotice("notice") {
			lost++
		}
		if !collector.AddWarning("warning") {
			lost++
		}
		if !collector.AddError("error") {
			lost++
		}
	}
	fmt.Printf("lost: %d\n", lost)
}
