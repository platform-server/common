/*
* @Author: Jingkai Mao (oammix@gmail.com)
* @Date:   2015-01-19 15:05:23
 */

package logext

import (
	"database/sql"
	"fmt"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"io"
	"runtime"
	"strings"
	"time"
)

var LogLevelTag = map[LogLevel]string{
	LogLevelError:   "Error",
	LogLevelWarning: "Warn",
	LogLevelNotice:  "Notice",
	LogLevelInfo:    "Info",
	LogLevelDebug:   "Debug",
	LogLevelData:    "Data",
	LogLevelFatal:   "Fatal",
}

func CastLogLevel2Tag(level LogLevel) string {
	if item, found := LogLevelTag[level]; found {
		return item
	}

	return "Unknown"
}

//
type Recorder interface {
	Record(LogLevel, string, int) string
	SetErrorCollector(producer string, db *sql.DB)
	SetErrorMailer(producer, host string, port int, user, password, to string)
	SetKinesisLogProducer(producer, stream, region, accessKey, secretKey string)
	SetKinesisLogProducerWithSession(producer, stream string, sess *kinesis.Kinesis)
}

// StdRecorder
type tStdRecorder struct {
	writer          io.Writer
	stack           int
	collector       *LogCollector
	mailer          *LogMailer
	kinesisProducer *KinesisLogProducer
}

func NewStdRecorder(out io.Writer, stack int) Recorder {
	return &tStdRecorder{writer: out, stack: stack}
}

func (self *tStdRecorder) Record(level LogLevel, message string, stack int) string {
	if stack < 0 {
		stack = self.stack
	}

	_, file, line, _ := runtime.Caller(3 + stack)
	names := strings.Split(file, "/")
	basename := names[len(names)-1]
	if len(basename) > kFileWidth {
		basename = ".." + basename[len(basename)-kFileWidth+2:]
	}

	out := fmt.Sprintf(
		"%s %14s:%-4d[%-6s] %s\n",
		time.Now().Format("2006-01-02 15:04:05.000"), basename, line, CastLogLevel2Tag(level), message,
	)
	self.writer.Write([]byte(out))

	// write to kinesis
	if self.kinesisProducer != nil {
		switch level {
		case LogLevelError:
			self.kinesisProducer.AddError(out)
		case LogLevelWarning:
			self.kinesisProducer.AddWarning(out)
		case LogLevelNotice:
			self.kinesisProducer.AddNotice(out)
		case LogLevelInfo:
			self.kinesisProducer.AddInfo(out)
		case LogLevelDebug:
			self.kinesisProducer.AddDebug(out)
		case LogLevelData:
			self.kinesisProducer.AddData(out)
		case LogLevelFatal:
			self.kinesisProducer.AddFatal(out)
		}
	}

	// add to database
	if level == LogLevelError && self.collector != nil {
		self.collector.AddError(out)
	}

	// send mail
	if level == LogLevelError && self.mailer != nil {
		self.mailer.Add(out)
	}

	return strings.Trim(out, "\n")
}

func (self *tStdRecorder) SetKinesisLogProducerWithSession(producer, stream string, sess *kinesis.Kinesis) {
	if self.kinesisProducer == nil {
		self.kinesisProducer = NewKinesisLogProducerWithSession(producer, stream, sess)
	}
}

// only tStdRecorder send logs to kinesis
func (self *tStdRecorder) SetKinesisLogProducer(producer, stream, region, accessKey, secretKey string) {
	if self.kinesisProducer == nil {
		self.kinesisProducer = NewKinesisLogProducer(producer, stream, region, accessKey, secretKey)
	}
}

// only tStdRecorder do the error log collection
func (self *tStdRecorder) SetErrorCollector(producer string, db *sql.DB) {
	if self.collector == nil {
		self.collector = NewLogCollector(producer, db)
	}
}

// only tStdRecorder do the error log mailing
func (self *tStdRecorder) SetErrorMailer(producer, host string, port int, user, password, to string) {
	if self.mailer == nil {
		self.mailer = NewLogMailer(producer, host, port, user, password, to)
	}
}

// KidsRecorder
type tKidsRecorder struct {
	topic string
	kids  KidsClient
}

func NewKidsRecorder(ntype, address, topic string) (Recorder, error) {

	kids, err := NewKidsClient(ntype, address, nil)
	if err != nil {
		return nil, err
	}

	return &tKidsRecorder{topic: topic, kids: kids}, nil
}

func (self *tKidsRecorder) Record(level LogLevel, message string, stack int) string {

	self.kids.Log(self.topic, message)
	return message
}

func (self *tKidsRecorder) SetErrorCollector(producer string, db *sql.DB) {}
func (self *tKidsRecorder) SetErrorMailer(producer, host string, port int, user, password, to string) {
}
func (self *tKidsRecorder) SetKinesisLogProducer(producer, stream, region, accessKey, secretKey string) {
}
func (self *tKidsRecorder) SetKinesisLogProducerWithSession(producer, stream string, sess *kinesis.Kinesis) {
}

// SeqRecorder
type tSeqRecorder struct {
	rList []Recorder
}

func NewSeqRecorder(rList ...Recorder) Recorder {

	seq := &tSeqRecorder{}
	for _, r := range rList {
		if r != nil {
			seq.rList = append(seq.rList, r)
		}
	}

	return seq
}

func (self *tSeqRecorder) Record(level LogLevel, message string, stack int) string {

	for _, r := range self.rList {
		message = r.Record(level, message, stack)
	}

	return message
}

func (self *tSeqRecorder) SetErrorCollector(producer string, db *sql.DB) {
	for _, r := range self.rList {
		r.SetErrorCollector(producer, db)
	}
}

func (self *tSeqRecorder) SetErrorMailer(producer, host string, port int, user, password, to string) {
	for _, r := range self.rList {
		r.SetErrorMailer(producer, host, port, user, password, to)
	}
}

func (self *tSeqRecorder) SetKinesisLogProducer(producer, stream, region, accessKey, secretKey string) {
	for _, r := range self.rList {
		r.SetKinesisLogProducer(producer, stream, region, accessKey, secretKey)
	}
}
func (self *tSeqRecorder) SetKinesisLogProducerWithSession(producer, stream string, sess *kinesis.Kinesis) {
	for _, r := range self.rList {
		r.SetKinesisLogProducerWithSession(producer, stream, sess)
	}
}
