// Author: fengguangpu@nibirutech.om
// Date: 2015-05-08

package logext

import (
	"encoding/json"
	"fmt"
	"os"
	"sync/atomic"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kinesis"
)

const (
	LOG_LEVEL_DEBUG   = "debug"
	LOG_LEVEL_INFO    = "info"
	LOG_LEVEL_NOTICE  = "notice"
	LOG_LEVEL_WARNING = "warning"
	LOG_LEVEL_ERROR   = "error"
	LOG_LEVEL_DATA    = "data"
	LOG_LEVEL_FATAL   = "fatal"

	KINESIS_LOG_BUFFER_SIZE      = 20480
	KINESIS_LOG_BYTE_BUFFER_SIZE = 1024
	KINESIS_LOG_BYTE_BUFFER_TIME = time.Second
	KINESIS_LOG_GET_TIMEOUT      = 2 * time.Millisecond
	KINESIS_LOG_PUT_TIMEOUT      = 10 * time.Millisecond
	KINESIS_LOG_BATCH_SIZE       = 64
	KINESIS_ROUTINE_STOP_COUNT   = 1000
	KINESIS_MAX_WORKER           = 100
)

type KinesisRecord struct {
	Timestamp int    `json:"timestamp"`
	Level     string `json:"level"`
	Data      string `json:"data"`
}

func (kr *KinesisRecord) Bytes() []byte {
	bytes, _ := json.Marshal(kr)
	return bytes
}

type KinesisLogProducer struct {
	stream       string
	region       string
	accessKey    string
	secretKey    string
	producer     string // used as partitionkey
	kinesis      *kinesis.Kinesis
	currntWorker uint32
	logBuffer    chan *KinesisRecord
}

func NewKinesisLogProducerWithSession(producer, stream string, sess *kinesis.Kinesis) *KinesisLogProducer {
	c := &KinesisLogProducer{
		stream:    stream,
		producer:  producer,
		kinesis:   sess,
		logBuffer: make(chan *KinesisRecord, KINESIS_LOG_BUFFER_SIZE),
	}

	go c.writeLogRoutine(false)

	return c
}

func NewKinesisLogProducer(producer, stream, region, accessKey, secretKey string) *KinesisLogProducer {
	c := &KinesisLogProducer{
		stream:    stream,
		region:    region,
		accessKey: accessKey,
		secretKey: secretKey,
		producer:  producer,
		logBuffer: make(chan *KinesisRecord, KINESIS_LOG_BUFFER_SIZE),
	}
	// set env variables AWS_ACCESS_KEY and AWS_SECRET_KEY AWS_REGION_NAME
	os.Setenv("AWS_ACCESS_KEY_ID", accessKey)
	os.Setenv("AWS_SECRET_ACCESS_KEY", secretKey)

	//httpClient := http.DefaultClient
	sen := session.New(&aws.Config{Region: aws.String(region)})
	c.kinesis = kinesis.New(sen, &aws.Config{}) //HTTPClient: httpClient

	go c.writeLogRoutine(false)

	return c
}

func (klp *KinesisLogProducer) AddDebug(msg string) bool {
	return klp.Add(&KinesisRecord{Level: LOG_LEVEL_DEBUG, Data: msg})
}

func (klp *KinesisLogProducer) AddInfo(msg string) bool {
	return klp.Add(&KinesisRecord{Level: LOG_LEVEL_INFO, Data: msg})
}

func (klp *KinesisLogProducer) AddNotice(msg string) bool {
	return klp.Add(&KinesisRecord{Level: LOG_LEVEL_NOTICE, Data: msg})
}

func (klp *KinesisLogProducer) AddWarning(msg string) bool {
	return klp.Add(&KinesisRecord{Level: LOG_LEVEL_WARNING, Data: msg})
}

func (klp *KinesisLogProducer) AddError(msg string) bool {
	return klp.Add(&KinesisRecord{Level: LOG_LEVEL_ERROR, Data: msg})
}

func (klp *KinesisLogProducer) AddData(msg string) bool {
	return klp.Add(&KinesisRecord{Level: LOG_LEVEL_DATA, Data: msg})
}

func (klp *KinesisLogProducer) AddFatal(msg string) bool {
	return klp.Add(&KinesisRecord{Level: LOG_LEVEL_FATAL, Data: msg})
}

// never block the caller more than 10 milliseconds
func (klp *KinesisLogProducer) Add(rec *KinesisRecord) bool {
	if rec == nil {
		return true
	}
	rec.Timestamp = int(time.Now().Unix())
	select {
	case klp.logBuffer <- rec:
		return true
	case <-time.After(KINESIS_LOG_PUT_TIMEOUT):
		fmt.Fprintf(os.Stdout, "too many(%d) pending kinesis log records to send\n", KINESIS_LOG_BUFFER_SIZE)
		if atomic.LoadUint32(&klp.currntWorker) < KINESIS_MAX_WORKER {
			go klp.writeLogRoutine(true)
			atomic.AddUint32(&klp.currntWorker, 1)
		}
		return false
	}
}

// never block the caller more than 2 milliseconds
func (klp *KinesisLogProducer) getRecord() *KinesisRecord {
	select {
	case rec := <-klp.logBuffer:
		return rec
	case <-time.After(KINESIS_LOG_GET_TIMEOUT):
		return nil
	}
}

func (klp *KinesisLogProducer) getTask() []*KinesisRecord {
	records := make([]*KinesisRecord, 0)

	// block read
	rec := <-klp.logBuffer
	records = append(records, rec)

	for i := 0; i < KINESIS_LOG_BATCH_SIZE; i++ {
		rec = klp.getRecord()
		if rec == nil {
			break
		}
		records = append(records, rec)
	}
	if len(records) == 0 {
		return nil
	}
	return records

}

var LOG_BOUNDARY = []byte("##log_boundary##")

func (klp *KinesisLogProducer) writeLogRoutine(autoStop bool) {
	fmt.Println("start writeLogRoutine...")
	var buff []byte
	kinesisRecords := make([]*kinesis.PutRecordsRequestEntry, 0)
	i := 0
	buffSize := KINESIS_LOG_BYTE_BUFFER_SIZE
	flushTime := time.Now()
	for {
		records := klp.getTask()
		if records == nil {
			// wait for 1 milisecond
			if i >= KINESIS_ROUTINE_STOP_COUNT && autoStop {
				fmt.Fprintf(os.Stdout, "kinesis writeLogRoutine empty count:%d  ,and stop \n", i)
				break
			}
			fmt.Fprintf(os.Stdout, "kinesis writeLogRoutine empty count:%d ,and wait 1ms \n", i)
			time.Sleep(time.Millisecond)
			i++
			if i%10 == 0 &&
				time.Now().Sub(flushTime) < KINESIS_LOG_BYTE_BUFFER_TIME &&
				buffSize > KINESIS_LOG_BYTE_BUFFER_SIZE {
				buffSize = KINESIS_LOG_BYTE_BUFFER_SIZE
			}
			if len(kinesisRecords) > 0 {
				go klp.putRecords(kinesisRecords)
				kinesisRecords = make([]*kinesis.PutRecordsRequestEntry, 0)
			}
			continue
		}

		// Put records in batch
		for i, rec := range records {
			data := rec.Bytes()
			// buffer to 128k
			needFlush := false
			bsize := len(buff) + len(data) + len(LOG_BOUNDARY)
			td := time.Now().Sub(flushTime)
			if td > KINESIS_LOG_BYTE_BUFFER_TIME {
				needFlush = true
			}
			if (needFlush == false) && bsize >= buffSize {
				if td < KINESIS_LOG_BYTE_BUFFER_TIME*2 && buffSize < 512*KINESIS_LOG_BYTE_BUFFER_SIZE {
					buffSize = buffSize * 2
				}
				needFlush = true
			}
			if needFlush {
				if len(buff) > 0 {
					partitionKey := fmt.Sprintf("%s#%d", klp.producer, i)
					kinesisRecords = append(kinesisRecords, &kinesis.PutRecordsRequestEntry{
						Data:         []byte(buff),
						PartitionKey: aws.String(partitionKey)})
				}

				flushTime = time.Now()
				if len(kinesisRecords) >= 10 || td > KINESIS_LOG_BYTE_BUFFER_TIME*5 {
					go klp.putRecords(kinesisRecords)
					kinesisRecords = make([]*kinesis.PutRecordsRequestEntry, 0)
				}
				buff = data
			} else {
				buff = append(buff, LOG_BOUNDARY...)
				buff = append(buff, data...)
			}
		}
	}
}

func updateFailedRecords(records []*kinesis.PutRecordsRequestEntry, failedNums []int) []*kinesis.PutRecordsRequestEntry {

	resp := make([]*kinesis.PutRecordsRequestEntry, 0)
	for i := 0; i < len(failedNums); i++ {
		resp = append(resp, records[failedNums[i]])
	}

	return resp
}
func (klp *KinesisLogProducer) putRecords(records []*kinesis.PutRecordsRequestEntry) {
	params := &kinesis.PutRecordsInput{}
	params.SetStreamName(klp.stream)

	var maxTryCount = 10
	var tryCount = 0
	for {
		params.SetRecords(records)
		if tryCount >= maxTryCount {
			fmt.Fprintf(os.Stdout, "kinesis puts record retry count:%d ,failed will drop data \n", tryCount)
			return
		}
		if klp.accessKey != "" && klp.secretKey != "" {
			os.Setenv("AWS_ACCESS_KEY_ID", klp.accessKey)
			os.Setenv("AWS_SECRET_ACCESS_KEY", klp.secretKey)
		}

		resp, err := klp.kinesis.PutRecords(params)
		if err != nil || (resp != nil && resp.FailedRecordCount != nil && *resp.FailedRecordCount != 0) {

			if err != nil {
				fmt.Fprintf(os.Stdout, "kinesis PutRecords failed: %v info:%+v\n", err)
			} else if resp != nil {
				fmt.Fprintf(os.Stdout, "kinesis PutRecords failed: %v failed count:%+v, info:%+v\n", err, resp.FailedRecordCount, resp.Records)
				failedNums := make([]int, 0)

				for i := 0; i < len(resp.Records); i++ {
					if resp.Records[i].ErrorCode != nil {
						fmt.Println("get failed num %d", i)
						failedNums = append(failedNums, i)
					}
				}

				fmt.Println("failed records:", failedNums)
				records = updateFailedRecords(records, failedNums)
				time.Sleep(time.Second)
			}

			tryCount++
			continue
		}
		break
	}
}
