/*
* @Author: Jingkai Mao (oammix@gmail.com)
* @Date:   2015-01-19 15:05:23
 */

// package forward is used to avoid cycle imports of logext.
package forward

import (
	"git.tap4fun.com/platform-server/common/logext"
	"sync"
)

// forward declaration of GLOBAL VARIABLE g_logger.
var (
	g_logger logext.Logger = logext.Noop() // keep silence as default
	g_rwlock *sync.RWMutex = &sync.RWMutex{}
)

// Getter & Setter, [WARNING] should NEVER be used outside codebase.
func GetGlobalLogger() logext.Logger {
	g_rwlock.RLock()
	defer g_rwlock.RUnlock()

	return g_logger
}

func SetGlobalLogger(ins logext.Logger) {
	g_rwlock.Lock()
	defer g_rwlock.Unlock()

	g_logger = ins
}
