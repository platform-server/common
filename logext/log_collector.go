// Error log collector

// Author: fengguangpu@nibirutech.om
// Date: 2015-02-03

/*
CREATE TABLE `errors` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `level` varchar(16) NOT NULL DEFAULT '',
    `producer` varchar(64) NOT NULL DEFAULT '',
    `content` varchar(2048) NOT NULL DEFAULT '',
    `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`,`create_at`),
    FULLTEXT KEY `content` (`content`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
PARTITION BY RANGE(create_at)(...)
*/

package logext

import (
	"database/sql"
	"time"
)

const (
	// log level definition
	LOG_LEVEL_D = "DEBUG"
	LOG_LEVEL_I = "INFO"
	LOG_LEVEL_N = "NOTICE"
	LOG_LEVEL_W = "WARNING"
	LOG_LEVEL_E = "ERROR"

	// max length of single log record: 2048 bytes
	// content will be truncated if too long, performed by database
	CONTENT_MAX_SIZE = 2048
	// max size of the buffer of log records
	BUFFER_MAX_SIZE = 10240
	// number of log writer
	LOG_WRITER_NUMBER = 2
	// number of records in a batch insert
	BATCH_INSERT_SIZE = 10

	INSERT_LOG_SQL      = "INSERT INTO errors(producer, level, content) values(?, ?, ?)"
	INSERT_LOG_SQL_MORE = " ,(?, ?, ?)"
)

type LogRecord struct {
	level   string
	content string
}

type LogCollector struct {
	Producer string
	DB       *sql.DB
	Buffer   chan *LogRecord
}

func NewLogCollector(producer string, db *sql.DB) *LogCollector {
	c := &LogCollector{Producer: producer, DB: db}
	c.Buffer = make(chan *LogRecord, BUFFER_MAX_SIZE)
	c.createWorkers()
	return c
}

func (lc *LogCollector) AddDebug(msg string) bool {
	return lc.Add(&LogRecord{level: LOG_LEVEL_D, content: msg})
}

func (lc *LogCollector) AddInfo(msg string) bool {
	return lc.Add(&LogRecord{level: LOG_LEVEL_I, content: msg})
}

func (lc *LogCollector) AddNotice(msg string) bool {
	return lc.Add(&LogRecord{level: LOG_LEVEL_N, content: msg})
}

func (lc *LogCollector) AddWarning(msg string) bool {
	return lc.Add(&LogRecord{level: LOG_LEVEL_W, content: msg})
}

func (lc *LogCollector) AddError(msg string) bool {
	return lc.Add(&LogRecord{level: LOG_LEVEL_E, content: msg})
}

// never block the caller more than 10 milliseconds
func (lc *LogCollector) Add(rec *LogRecord) bool {
	select {
	case lc.Buffer <- rec:
		return true
	case <-time.After(time.Millisecond * 10):
		return false
	}
}

func (lc *LogCollector) createWorkers() {
	for i := 0; i < LOG_WRITER_NUMBER; i++ {
		go lc.write()
	}
}

// never block the caller more than 1 microsecond
func (lc *LogCollector) getRecord() *LogRecord {
	select {
	case rec := <-lc.Buffer:
		return rec
	case <-time.After(time.Microsecond):
		return nil
	}
}

func (lc *LogCollector) getTask() (string, []interface{}) {
	// block read
	rec := <-lc.Buffer
	sql := INSERT_LOG_SQL

	params := make([]interface{}, 0)
	params = append(params, lc.Producer, rec.level, rec.content)

	for i := 0; i < BATCH_INSERT_SIZE-1; i++ {
		rec = lc.getRecord()
		if rec == nil {
			break
		}
		sql += INSERT_LOG_SQL_MORE
		params = append(params, lc.Producer, rec.level, rec.content)
	}

	return sql, params
}

func (lc *LogCollector) write() {
	for {
		sql, params := lc.getTask()
		_, err := lc.DB.Exec(sql, params...)
		if err != nil {
			continue
		}
	}
}
