/*
* @Author: Jingkai Mao (oammix@gmail.com)
* @Date:   2015-01-19 15:05:23
 */

package logext

import (
	"bufio"
	"io"
	"os"
	"path"
)

// NewStdLogger
func NewStdLogger(level LogLevel, stack int) (Logger, error) {

	return New(NewStdRecorder(os.Stdout, stack), level)
}

// NewKidsLogger
func NewKidsLogger(ntype, address, topic string, level LogLevel, stack int) (Logger, error) {

	std := NewStdRecorder(os.Stdout, stack+1)
	kids, err := NewKidsRecorder(ntype, address, topic)
	if err != nil {
		return nil, err
	}

	return New(NewSeqRecorder(std, kids), level)
}

// NewFileLogger
func NewFileLogger(dir, name string, level LogLevel, stack int) (Logger, error) {

	out, err := address(dir, name)
	if err != nil {
		return nil, err
	}

	return New(NewStdRecorder(out, stack), level)
}

type tBufferedLogger struct {
	tLogger // anonymous filed
	buffer  *bufio.Writer
}

// NewBufferedFileLogger
func NewBufferedLogger(dir, name string, size int, level LogLevel, stack int) (Logger, error) {

	out, err := address(dir, name)
	if err != nil {
		return nil, err
	}

	buffer := bufio.NewWriterSize(out, size)
	return &tBufferedLogger{
		tLogger{
			level:  level,
			writer: NewStdRecorder(buffer, stack),
		},
		buffer,
	}, nil
}

func (self *tBufferedLogger) Flush() {
	self.buffer.Flush()
}

func address(dir, name string) (io.Writer, error) {
	// create the log directory if not exists
	if err := os.MkdirAll(dir, 0750); err != nil {
		return nil, err
	}

	// use program name as log filename
	if name == "" {
		name = path.Base(os.Args[0])
	}

	fullpath := path.Join(dir, name)
	return os.OpenFile(fullpath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0640)
}
