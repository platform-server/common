/*
* @Author: Jingkai Mao (oammix@gmail.com)
* @Date:   2015-01-19 15:05:23
 */

package logext

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"net"
	"strconv"
	"strings"
	"time"
)

var (
	ErrKidsFailedReadResponse = errors.New("failed-read-response.")
	ErrKidsFailedReadArgument = errors.New("failed-read-argument.")
	kKidsReconnectInterval    = time.Duration(1) * time.Second
	kKidsMaxPendingTasks      = 256
)

const (
	kSginCr        byte = 13 // \r
	kSignLf        byte = 10 // \n
	kSignBulk      byte = 36 // $
	kSignInt       byte = 58 // :
	kSignErr       byte = 45 // -
	kSignOk        byte = 43 // +
	kSignMultiBulk byte = 42 // *
)

type KidsResponseHandler interface {
	OnRecvMessage(string, string)
	OnRecvPatternMessage(string, string, string)
	OnRecvUnexpectedMessage([]string)
	OnError(error)
}

type SimpleKidsHandler struct{}

func (self *SimpleKidsHandler) OnRecvMessage(string, string)                {}
func (self *SimpleKidsHandler) OnRecvPatternMessage(string, string, string) {}
func (self *SimpleKidsHandler) OnRecvUnexpectedMessage([]string)            {}
func (self *SimpleKidsHandler) OnError(error)                               {}

type KidsClient interface {
	Log(string, string)
	Subscribe(string)
	Unsubscribe(string)
	Psubscribe(string)
	Punsubscribe(string)
}

type tKidsClient struct {
	handler KidsResponseHandler
	conn    net.Conn
	read    *bufio.Reader
	write   *bufio.Writer
	send    chan [][]byte
	errors  chan error
	recv    chan bool
	tasks   [][][]byte
	ntype   string
	address string
}

func NewKidsClient(ntype, addr string, handler KidsResponseHandler) (KidsClient, error) {

	if handler == nil {
		handler = &SimpleKidsHandler{}
	}

	kids := &tKidsClient{
		handler: handler,
		send:    make(chan [][]byte, 128),
		errors:  make(chan error, 16),
		ntype:   ntype,
		address: addr,
	}

	if err := kids.Connect(); err != nil {
		return nil, err
	}

	go kids.Routine()
	return kids, nil
}

func (self *tKidsClient) Log(topic, message string) {

	self.send <- [][]byte{[]byte("LOG"), []byte(topic), []byte(message)}
}

func (self *tKidsClient) Subscribe(topic string) {

	self.send <- [][]byte{[]byte("SUBSCRIBE"), []byte(topic)}
}

func (self *tKidsClient) Psubscribe(pattern string) {

	self.send <- [][]byte{[]byte("PSUBSCRIBE"), []byte(pattern)}
}

func (self *tKidsClient) Unsubscribe(topic string) {

	self.send <- [][]byte{[]byte("UNSUBSCRIBE"), []byte(topic)}
}

func (self *tKidsClient) Punsubscribe(pattern string) {

	self.send <- [][]byte{[]byte("PUNSUBSCRIBE"), []byte(pattern)}
}

func (self *tKidsClient) Routine() {

	reConn := false
	self.tasks = [][][]byte{}
	self.LaunchRecv()

Loop:
	for {

		select {
		case argv := <-self.send:
			if err := SendRequest(self.write, argv); err != nil {
				reConn = true
				// store failed send tasks to pending list.
				self.tasks = append(self.tasks, argv)
				if len(self.tasks) >= kKidsMaxPendingTasks {
					self.tasks = [][][]byte{}
					fmt.Println("kids drop pending tasks.") //
				}
			}

		case _ = <-self.errors:
			reConn = true

		case <-time.After(kKidsReconnectInterval):
			break
		}

		if reConn {
			if err := self.Connect(); err != nil {
				fmt.Printf("failed to make connection with kids(%s), %v\n", self.address, err)
				continue
			}

			for ind, argv := range self.tasks {
				if err := SendRequest(self.write, argv); err != nil {
					self.tasks = self.tasks[ind:]
					reConn = true
					goto Loop // retry
				}
			}

			reConn = false
			self.tasks = [][][]byte{}
			self.LaunchRecv()
		}
	}
}

func (self *tKidsClient) Connect() error {

	if self.conn != nil {
		self.conn.Close()
		self.conn = nil
	}

	conn, err := net.Dial(self.ntype, self.address)
	if err != nil {
		return err
	}

	self.conn = conn
	self.write = bufio.NewWriter(conn)
	self.read = bufio.NewReader(conn)
	return nil
}

func SendRequest(write *bufio.Writer, argv [][]byte) error {

	buf := &bytes.Buffer{}
	fmt.Fprintf(buf, "*%d\r\n", len(argv))

	for _, arg := range argv {
		fmt.Fprintf(buf, "$%d\r\n", len(arg))
		buf.Write(arg)
		buf.Write([]byte("\r\n"))
	}

	if _, err := write.Write(buf.Bytes()); err != nil {
		return err
	}

	return write.Flush()
}

func (self *tKidsClient) LaunchRecv() {
	if self.recv != nil {
		close(self.recv)
		self.recv = nil
	}

	self.recv = make(chan bool)
	go self.Recv(self.recv)
}

func (self *tKidsClient) Recv(ctrl chan bool) {

	for {
		select {
		case _, _ = <-ctrl:
			return

		default:
			resp, err := ReadResponse(self.read)
			if err != nil {
				if err != ErrKidsFailedReadArgument && err != ErrKidsFailedReadResponse {
					self.errors <- err
					return
				}
			}

			switch strings.ToUpper(resp[0]) {
			case "MESSAGE":
				self.handler.OnRecvMessage(resp[1], resp[2])
			case "PMESSAGE":
				self.handler.OnRecvPatternMessage(resp[1], resp[2], resp[3])
			default:
				self.handler.OnRecvUnexpectedMessage(resp)
			}
		}
	}
}

func ReadResponse(read *bufio.Reader) ([]string, error) {

	raw, err := read.ReadBytes(kSignLf)
	if err != nil {
		return nil, err
	}

	//
	response := []string{}
	switch raw[0] {
	case kSignInt:
		response = append(response, string(raw[:len(raw)-2]))
	case kSignOk, kSignErr:
		response = append(response, string(raw[:len(raw)-2]))
	case kSignMultiBulk:
		argc, err := strconv.Atoi(string(raw[1 : len(raw)-2]))
		if err != nil {
			return nil, err
		}

		for i := 0; i < argc; i++ {
			arg, err := ReadArgument(read)
			if err != nil {
				return nil, err
			}

			response = append(response, string(arg))
		}
	default:
		return nil, ErrKidsFailedReadResponse
	}

	return response, nil
}

func ReadArgument(read *bufio.Reader) (string, error) {

	raw, err := read.ReadBytes(kSignLf)
	if err != nil {
		return "", err
	}

	if raw[0] == kSignBulk {
		arglen, err := strconv.Atoi(string(raw[1 : len(raw)-2]))
		if err != nil {
			return "", err
		}

		arg := make([]byte, arglen+2) // make sure to read \r\n
		if _, err := io.ReadFull(read, arg[0:]); err != nil {
			return "", err
		}
		return string(arg[:len(arg)-2]), nil
	} else if raw[0] == kSignInt {
		return string(raw[1 : len(raw)-2]), nil
	}

	return "", ErrKidsFailedReadArgument
}
