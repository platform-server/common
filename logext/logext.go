/*
* @Author: Jingkai Mao (oammix@gmail.com)
* @Date:   2015-01-19 15:05:23
 */

package logext

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/aws/aws-sdk-go/service/kinesis"
)

const (
	LogBufferedSize = 4096
)

var ErrInvalidLogLevel = errors.New("invalid-log-level.")

type LogLevel uint32

//
const (
	LogLevelError = LogLevel(iota)
	LogLevelWarning
	LogLevelNotice
	LogLevelInfo
	LogLevelData
	LogLevelDebug
	LogLevelFatal
	kFileWidth = 14
)

// Logger defines all the methods used in logging.
type Logger interface {
	Debug(args ...interface{})
	Info(args ...interface{})
	Notice(args ...interface{})
	Warning(args ...interface{})
	Error(args ...interface{})
	Fatal(args ...interface{})
	Debugf(format string, args ...interface{})
	Infof(format string, args ...interface{})
	Noticef(format string, args ...interface{})
	Warningf(format string, args ...interface{})
	Errorf(format string, args ...interface{})
	Fatalf(format string, args ...interface{})
	Dataf(format string, args ...interface{})

	Flush() // flush is a reserved for buffer

	// deprecated interfaces
	DLog(format string, args ...interface{})
	ILog(format string, args ...interface{})
	NLog(format string, args ...interface{})
	WLog(format string, args ...interface{})
	ELog(format string, args ...interface{})
	FLog(format string, args ...interface{})
	//
	DSLog(format string, stack int, args ...interface{})
	ISLog(format string, stack int, args ...interface{})
	NSLog(format string, stack int, args ...interface{})
	WSLog(format string, stack int, args ...interface{})
	ESLog(format string, stack int, args ...interface{})

	// kinesis log producer
	SetKinesisLogProducerWithSession(producer, stream string, sess *kinesis.Kinesis)
	SetKinesisLogProducer(producer, stream, region, access, secret string)

	// error log collector
	SetErrorCollector(producer string, db *sql.DB)

	// error log mailer
	SetErrorMailer(producer, host string, port int, user, password, to string)
}

// tLogger provides a concurrent-friendly and buffered(?) logging,
// it also automatic filters per-output log based on levels.
type tLogger struct {
	level  LogLevel
	writer Recorder
}

// NewLogger returns a tLogger structure. The out variable sets the
// destination to which log data will be written.
func New(out Recorder, level LogLevel) (Logger, error) {

	if level > LogLevelDebug || level < LogLevelError {
		return nil, ErrInvalidLogLevel
	}

	return &tLogger{
		level:  level,
		writer: out,
	}, nil
}

// record filters log based on LogLevel, and writes it into buffer.
func (self *tLogger) record(level LogLevel, message string, stack int) {
	//  因为要加入其他扩展标签，所以不过滤
	// if level > self.level {
	// 	return
	// }

	self.writer.Record(level, message, stack)
}

func (self tLogger) SetKinesisLogProducerWithSession(producer, stream string, sess *kinesis.Kinesis) {
	self.writer.SetKinesisLogProducerWithSession(producer, stream, sess)
}

func (self *tLogger) SetKinesisLogProducer(producer, stream, region, access, secret string) {
	self.writer.SetKinesisLogProducer(producer, stream, region, access, secret)
}

func (self *tLogger) SetErrorCollector(producer string, db *sql.DB) {
	self.writer.SetErrorCollector(producer, db)
}

func (self *tLogger) SetErrorMailer(producer, host string, port int, user, password, to string) {
	self.writer.SetErrorMailer(producer, host, port, user, password, to)
}

// Log methods in convenience uses.
func (self *tLogger) Debug(args ...interface{}) {
	self.record(LogLevelDebug, fmt.Sprint(args...), -1)
}

func (self *tLogger) Info(args ...interface{}) {
	self.record(LogLevelInfo, fmt.Sprint(args...), -1)
}

func (self *tLogger) Notice(args ...interface{}) {
	self.record(LogLevelNotice, fmt.Sprint(args...), -1)
}

func (self *tLogger) Warning(args ...interface{}) {
	self.record(LogLevelWarning, fmt.Sprint(args...), -1)
}

func (self *tLogger) Error(args ...interface{}) {
	self.record(LogLevelError, fmt.Sprint(args...), -1)
}

func (self *tLogger) Fatal(args ...interface{}) {
	self.record(LogLevelFatal, fmt.Sprint(args...), -1)
}

func (self *tLogger) Debugf(format string, args ...interface{}) {
	self.record(LogLevelDebug, fmt.Sprintf(format, args...), -1)
}

func (self *tLogger) Infof(format string, args ...interface{}) {
	self.record(LogLevelInfo, fmt.Sprintf(format, args...), -1)
}

func (self *tLogger) Noticef(format string, args ...interface{}) {
	self.record(LogLevelNotice, fmt.Sprintf(format, args...), -1)
}

func (self *tLogger) Warningf(format string, args ...interface{}) {
	self.record(LogLevelWarning, fmt.Sprintf(format, args...), -1)
}

func (self *tLogger) Errorf(format string, args ...interface{}) {
	self.record(LogLevelError, fmt.Sprintf(format, args...), -1)
}

func (self *tLogger) Fatalf(format string, args ...interface{}) {
	self.record(LogLevelFatal, fmt.Sprintf(format, args...), -1)
}

func (self *tLogger) Dataf(format string, args ...interface{}) {
	self.record(LogLevelData, fmt.Sprintf(format, args...), -1)
}

func (self *tLogger) Flush() {}

func (self *tLogger) DLog(format string, args ...interface{}) {
	self.record(LogLevelDebug, fmt.Sprintf(format, args...), -1)
}

func (self *tLogger) ILog(format string, args ...interface{}) {
	self.record(LogLevelInfo, fmt.Sprintf(format, args...), -1)
}

func (self *tLogger) NLog(format string, args ...interface{}) {
	self.record(LogLevelNotice, fmt.Sprintf(format, args...), -1)
}

func (self *tLogger) WLog(format string, args ...interface{}) {
	self.record(LogLevelWarning, fmt.Sprintf(format, args...), -1)
}

func (self *tLogger) ELog(format string, args ...interface{}) {
	self.record(LogLevelError, fmt.Sprintf(format, args...), -1)
}

func (self *tLogger) FLog(format string, args ...interface{}) {
	self.record(LogLevelFatal, fmt.Sprintf(format, args...), -1)
}

func (self *tLogger) DSLog(format string, stack int, args ...interface{}) {
	self.record(LogLevelDebug, fmt.Sprintf(format, args...), stack)
}

func (self *tLogger) ISLog(format string, stack int, args ...interface{}) {
	self.record(LogLevelInfo, fmt.Sprintf(format, args...), stack)
}

func (self *tLogger) NSLog(format string, stack int, args ...interface{}) {
	self.record(LogLevelNotice, fmt.Sprintf(format, args...), stack)
}

func (self *tLogger) WSLog(format string, stack int, args ...interface{}) {
	self.record(LogLevelWarning, fmt.Sprintf(format, args...), stack)
}

func (self *tLogger) ESLog(format string, stack int, args ...interface{}) {
	self.record(LogLevelError, fmt.Sprintf(format, args...), stack)
}

// ->
type noLogger struct{}

func (self noLogger) Debug(...interface{})            {}
func (self noLogger) Info(...interface{})             {}
func (self noLogger) Notice(...interface{})           {}
func (self noLogger) Warning(...interface{})          {}
func (self noLogger) Error(...interface{})            {}
func (self noLogger) Fatal(...interface{})            {}
func (self noLogger) Debugf(string, ...interface{})   {}
func (self noLogger) Infof(string, ...interface{})    {}
func (self noLogger) Noticef(string, ...interface{})  {}
func (self noLogger) Warningf(string, ...interface{}) {}
func (self noLogger) Errorf(string, ...interface{})   {}
func (self noLogger) Fatalf(string, ...interface{})   {}
func (self noLogger) Dataf(string, ...interface{})    {}
func (self noLogger) Flush()                          {}

func (self noLogger) DLog(format string, args ...interface{})                                   {}
func (self noLogger) ILog(format string, args ...interface{})                                   {}
func (self noLogger) NLog(format string, args ...interface{})                                   {}
func (self noLogger) WLog(format string, args ...interface{})                                   {}
func (self noLogger) ELog(format string, args ...interface{})                                   {}
func (self noLogger) FLog(format string, args ...interface{})                                   {}
func (self noLogger) DSLog(format string, stack int, args ...interface{})                       {}
func (self noLogger) ISLog(format string, stack int, args ...interface{})                       {}
func (self noLogger) NSLog(format string, stack int, args ...interface{})                       {}
func (self noLogger) WSLog(format string, stack int, args ...interface{})                       {}
func (self noLogger) ESLog(format string, stack int, args ...interface{})                       {}
func (self noLogger) SetErrorCollector(producer string, db *sql.DB)                             {}
func (self noLogger) SetErrorMailer(producer, host string, port int, user, password, to string) {}
func (self noLogger) SetKinesisLogProducer(producer, stream, region, access, secret string)     {}
func (self noLogger) SetKinesisLogProducerWithSession(producer, stream string, sess *kinesis.Kinesis) {
}

// NoopLogger returns a struct that satisfies the Logger interface
// but silently ignores all Logger method invocations.
func Noop() Logger {
	return noLogger{}
}
