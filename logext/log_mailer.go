package logext

import (
	"encoding/base64"
	"errors"
	"fmt"
	"net/smtp"
	"os"
	"strings"
	"time"
)

const MAX_PENDING_MAILS = 20480
const LOG_MAILER_NUM = 2

type LogMailer struct {
	to       string
	producer string
	sender   *EmailSender
	buffer   chan string
}

func NewLogMailer(producer, host string, port int, user, password, to string) *LogMailer {
	lm := &LogMailer{
		to:       to,
		producer: producer,
		sender:   NewEmailSender(host, port, user, password),
		buffer:   make(chan string, MAX_PENDING_MAILS),
	}
	lm.createWorkers()
	return lm
}

func (lm *LogMailer) createWorkers() {
	for i := 0; i < LOG_MAILER_NUM; i++ {
		go lm.send()
	}
}

func (lm *LogMailer) send() {
	for {
		msg := <-lm.buffer
		err := lm.sender.Send(lm.to, fmt.Sprintf("Error from %s", lm.producer), msg)
		if err != nil {
			fmt.Fprintf(os.Stdout, "%s |-> send email for error failed: %s", msg, err.Error())
		}
	}
}

func (lm *LogMailer) Add(msg string) bool {
	select {
	case lm.buffer <- msg:
		return true
	case <-time.After(time.Millisecond * 10):
		return false
	}
}

type EmailSender struct {
	// priviate fields
	smtpHost string
	smtpPort int
	smtpAddr string
	from     string
	auth     smtp.Auth
}

// init auth
func NewEmailSender(host string, port int, user, password string) *EmailSender {
	return &EmailSender{
		smtpHost: host,
		smtpPort: port,
		smtpAddr: fmt.Sprintf("%s:%d", host, port),
		from:     user,
		auth:     smtp.PlainAuth("", user, password, host),
	}
}

// receivers are seperated by ',' in to
func (es *EmailSender) Send(to, subject, body string) error {
	if es.smtpHost == "" || es.smtpPort == 0 || es.from == "" || es.auth == nil {
		return errors.New("EmailSender has not been initialized")
	}

	if strings.TrimSpace(to) == "" {
		return errors.New("Receivers are empty")
	}

	receivers := strings.Split(to, ",")
	for i := 0; i < len(receivers); i++ {
		receivers[i] = strings.TrimSpace(receivers[i])
	}

	header := make(map[string]string)
	header["From"] = es.from
	header["To"] = "You"
	header["Subject"] = subject
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = "text/plain; charset=\"utf-8\""
	header["Content-Transfer-Encoding"] = "base64"

	message := ""
	for k, v := range header {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + base64.StdEncoding.EncodeToString([]byte(body))
	//fmt.Printf("message:\n%s\n", message)

	return smtp.SendMail(es.smtpAddr, es.auth, es.from, receivers, []byte(message))
}

func test() {
	// Set up authentication information.
	smtpHost := "smtp.126.com"
	smtpPort := 25
	user := "pf_tap4fun@126.com"
	password := "pftap4fun1234"
	receiver := "fengguangpu@nibirutech.com,pf_errors@163.com"

	sender := NewEmailSender(smtpHost, smtpPort, user, password)

	err := sender.Send(receiver, "error occured!", "something is wrong, attention!")
	if err != nil {
		fmt.Printf("Send failed: %s\n", err.Error())
	}
}
