/*
* @Author: Jingkai Mao (oammix@gmail.com)
* @Date:   2015-01-21 13:27:10
 */

package framework

import (
	"path"
	"time"

	"git.tap4fun.com/platform-server/common/config"
	"git.tap4fun.com/platform-server/common/deamon"
	"git.tap4fun.com/platform-server/common/logext"
	"git.tap4fun.com/platform-server/common/logext/forward"
	"git.tap4fun.com/platform-server/common/zookeeper"
)

// ALL the GLOBAL VARIABLES are declared here except `g_logger`.
// is Singleton suitable to this senario?
var (
	g_logger        logext.Logger             = logext.Noop()
	g_statsd        deamon.Statsd             = deamon.NoopStats()
	g_status        deamon.PlatformStatus     = deamon.NoopPlatformStatus()
	g_zookeeper     zookeeper.ZookeeperClient = zookeeper.Noop()
	g_access        AccessKeyMgr              = NoopAccessKeyMgr()
	g_configs       *goconfig.ConfigFile      = nil
	g_remoteConfigs *goconfig.RemoteConfig    = nil

	kDefaultStatsdTimeout  = time.Duration(5) * time.Second
	kDefaultStatsdInterval = time.Duration(30) * time.Second
)

// Getter, Nothing Else
func GetConfigs() *goconfig.ConfigFile {
	return g_configs
}

func GetLogger() logext.Logger {
	return forward.GetGlobalLogger()
}

func GetStatsd() deamon.Statsd {
	return g_statsd
}

func GetStatus() deamon.PlatformStatus {
	return g_status
}

func GetZookeeper() zookeeper.ZookeeperClient {
	return g_zookeeper
}

func GetAccessMgr() AccessKeyMgr {
	return g_access
}

func GetRemoteConfigs() *goconfig.RemoteConfig {
	return g_remoteConfigs
}

//
func InitConfigFromFile(dir string, name string) error {

	conf, err := goconfig.ReadConfigFile(path.Join(dir, name))
	if err != nil {
		return err
	}

	g_configs = conf
	return nil
}

func InitRemoteConfig(conf *goconfig.ConfigFile) error {

	g_remoteConfigs = goconfig.GetRemoteConfig(conf)
	return nil
}

func InitStdLogger(level logext.LogLevel, stack int) error {

	logger, err := logext.NewStdLogger(level, stack)
	if err != nil {
		return err
	}

	g_logger = logger
	forward.SetGlobalLogger(logger)
	return nil
}

func InitKidsLogger(address, topic string, level logext.LogLevel, stack int) error {

	logger, err := logext.NewKidsLogger("tcp", address, topic, level, stack)
	if err != nil {
		return err
	}

	g_logger = logger
	forward.SetGlobalLogger(logger)
	return nil
}

func InitStatsDeamon(service, address string) error {

	return InitStatsDeamon2(service, address, kDefaultStatsdTimeout, kDefaultStatsdInterval)
}

func InitStatsDeamon2(service, address string, tout, interval time.Duration) error {

	statsd, err := deamon.NewUDPStats(service, address, tout, interval)
	if err != nil {
		return err
	}

	g_statsd = statsd
	return nil
}

func InitPlatformStatus(name, version, hash string) error {

	status, err := deamon.NewPlatformStatus(name, version, hash)
	if err != nil {
		return err
	}

	g_status = status
	return nil
}

func InitZookeeper(addrs []string, user, pswd string, tout time.Duration, localAddr string) error {

	client, err := zookeeper.NewClient(addrs, user, pswd, tout, localAddr, GetRemoteConfigs())
	if err != nil {
		return err
	}

	g_zookeeper = client
	return nil
}

func InitAccessKeyMgr(interval time.Duration, localAddr string) error {
	mgr, err := NewAccessKeyMgr(interval, localAddr)
	if err != nil {
		return err
	}

	g_access = mgr
	return nil
}
