/*
* @Author: Jingkai Mao (oammix@gmail.com)
* @Date:   2015-01-19 18:17:17
 */

package framework

import (
	"flag"
	"fmt"
	"log"

	"git.tap4fun.com/platform-server/mthird/thrift/lib/go/thrift"
)

var g_framework Framework

// success or failed
func InitServer(server FrameworkServer, name, date, ver, hash string) {

	if g_framework != nil {
		log.Fatalln(ErrDoubleInit)
	}

	sup, err := NewFramework(server, name, date, ver, hash)
	if err != nil {
		log.Fatalln(err)
	}

	g_framework = sup
}

func Serve() {

	if g_framework == nil {
		log.Fatalln(ErrFrameworkNotReady)
	}

	if err := g_framework.Launch(); err != nil {
		log.Fatalln(err)
	}

	g_framework.Serve()
}

func CreateService(name, port string, processor thrift.TProcessor) error {

	if g_framework == nil {
		log.Fatalln(ErrFrameworkNotReady)
	}

	return g_framework.StartService(name, port, processor)
}

func CreateSecureService(
	name, fkey, fcert, port string, processor thrift.TProcessor) error {

	if g_framework == nil {
		log.Fatalln(ErrFrameworkNotReady)
	}

	return g_framework.StartSecureService(name, fkey, fcert, port, processor)
}

func RegisterService(name, servType string) error {

	if g_framework == nil {
		log.Fatalln(ErrFrameworkNotReady)
	}

	return g_framework.RegisterService(name, servType)
}

func ParseCommandLine(strings []string) (map[string]string, error) {

	values := make([]string, len(strings)+1)
	for ind, key := range strings {
		flag.StringVar(&values[ind], key, "", "")
	}
	flag.Parse()

	rsp := map[string]string{}
	for ind, key := range strings {
		rsp[key] = values[ind]
		if rsp[key] == "" {
			flag.PrintDefaults()
			return nil, fmt.Errorf("%s-need-as-command.", key)
		}
	}

	return rsp, nil
}

func GetFirstError(errs ...error) error {

	for _, err := range errs {
		if err != nil {
			return err
		}
	}

	return nil
}
