package framework

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"git.tap4fun.com/platform-server/common/logext"
	"git.tap4fun.com/platform-server/common/netext"
)

type FrameworkSimpleServer struct{}

func (self *FrameworkSimpleServer) OnInit(env map[string]string) error {

	cmd, err := ParseCommandLine([]string{"d", "f"})
	AssertInit(err)

	// init conf first
	AssertInit(InitConfigFromFile(cmd["d"], cmd["f"]))

	AssertInit(InitRemoteConfig(GetConfigs()))

	localAddr := "127.0.0.1:6000"
	if env["name"] != "local" {
		localAddr = AssertGetConfig("framework-local", "local_address")
		GetLogger().Infof("%s start, get local addr:%s", env["name"], localAddr)
	} else {
		GetLogger().Infof("local server start")
	}
	// get physical server tag
	url, err := netext.GetRedirectLocalUrl(localAddr)
	if err != nil {
		url, err = netext.GetExternalIpv4Address()
		if err != nil {
			url = "localhost"
		}
	}
	env["tag"] = url

	GetRemoteConfigs().InitConfig(env["name"], env["version"], localAddr)

	if HasSection("framework-log") {

		// we use GetLogger().Outputf as default, so set stack to 0
		level := logext.LogLevel(AssertGetRemoteConfigInt64("framework-log", "level"))
		AssertInit(InitStdLogger(level, 0))

		if HasSection("framework-kids") {
			addr := AssertGetRemoteConfig("framework-kids", "address")
			AssertInit(InitKidsLogger(addr, env["name"]+"@"+env["tag"], level, 0))
			GetLogger().Info("Kids logger initiation OK")
		}
	} else {
		GetLogger().Warningf("[1/2] section [framework-log] not found,")
		GetLogger().Warningf("[2/2] use Stdout and LogLevelInfo as default logger.")
	}

	if HasSection("framework-statsd") {

		addr := AssertGetRemoteConfig("framework-statsd", "address")
		itrv := AssertGetRemoteConfigInt64("framework-statsd", "interval")
		tval := AssertGetRemoteConfigInt64("framework-statsd", "timeout")

		tout := time.Duration(tval) * time.Millisecond
		ival := time.Duration(itrv) * time.Millisecond
		AssertInit(InitStatsDeamon2(env["name"], addr, tout, ival))
	} else {

		GetLogger().Warningf("[1/2] section [framework-statsd] not found,")
		GetLogger().Warningf("[2/2] failed to init statistics deamon.")
	}

	if HasSection("framework-zookeeper") {

		addr := AssertGetRemoteConfig("framework-zookeeper", "zkList")
		user := AssertGetRemoteConfig("framework-zookeeper", "username")
		pswd := AssertGetRemoteConfig("framework-zookeeper", "password")
		tval := AssertGetRemoteConfigInt64("framework-zookeeper", "timeout")

		addrList := strings.Split(addr, ",")
		tout := time.Duration(tval) * time.Millisecond
		AssertInit(InitZookeeper(addrList, user, pswd, tout, localAddr))
	} else {

		GetLogger().Warningf("[1/2] section [framework-zookeeper] not found,")
		GetLogger().Warningf("[2/2] failed to init zookeeper.")
	}

	if HasSection("framework-status") {

		AssertInit(InitPlatformStatus(env["name"], env["version"], env["hash"]))
	} else {

		GetLogger().Warningf("[1/2] section [framework-status] not found,")
		GetLogger().Warningf("[2/2] failed to init platform status.")
	}

	if HasSection("framework-accessKey") {
		ival := AssertGetRemoteConfigInt64("framework-accessKey", "interval")
		tval := time.Duration(ival) * time.Millisecond
		//addr := AssertGetRemoteConfig("framework-local", "local_address")

		AssertInit(InitAccessKeyMgr(tval, localAddr))
		GetLogger().ILog("Get/update accesskey from local service")
	}

	if HasSection("framework-logerror") {
		InitErrorCollector("framework-logerror")
	} else {
		GetLogger().Warningf("Configuration file do not contain section 'framework-logerror'")
	}

	if HasSection("framework-mailerror") {
		InitErrorMailer("framework-mailerror")
	} else {
		GetLogger().Warningf("Configuration file do not contain section 'framework-mailerror'")
	}

	if HasSection("framework-logkinesis") {
		InitLogKinesis("framework-logkinesis")
	} else {
		GetLogger().Warningf("Configuration file do not contain section 'framework-logkinesis'")
	}

	return nil
}

func (self *FrameworkSimpleServer) OnInitZookeeper() error { return nil }
func (self *FrameworkSimpleServer) OnCreateService() error { return nil }
func (self *FrameworkSimpleServer) OnStart() error         { return nil }
func (self *FrameworkSimpleServer) OnStop() error          { return nil }

type FrameworkNormalServer struct{}

func (self *FrameworkNormalServer) OnInit(env map[string]string) error {

	cmd, err := ParseCommandLine([]string{"d", "f"})
	AssertInit(err)

	// init conf first
	AssertInit(InitConfigFromFile(cmd["d"], cmd["f"]))

	AssertInit(InitRemoteConfig(GetConfigs()))

	localAddr := AssertGetConfig("framework-local", "local_address")
	// get physical server tag
	url, err := netext.GetRedirectLocalUrl(localAddr)
	if err != nil {
		url, err = netext.GetExternalIpv4Address()
		if err != nil {
			url = "localhost"
		}
	}
	env["tag"] = url

	GetRemoteConfigs().InitConfig(env["name"], env["version"], localAddr)

	// init statsd
	addr := AssertGetRemoteConfig("framework-statsd", "address")
	AssertInit(InitStatsDeamon(env["name"], addr))

	// init std logger
	level := logext.LogLevel(AssertGetRemoteConfigInt64("framework-log", "level"))
	if kids, err := GetRemoteConfigs().GetString("framework-log", "kids"); err == nil {
		AssertInit(InitKidsLogger(kids, env["name"]+"@"+env["tag"], level, 0))
	} else {
		AssertInit(InitStdLogger(level, 0))
	}

	// init platform status
	AssertInit(InitPlatformStatus(env["name"], env["version"], env["hash"]))

	// init access keys clients
	if HasSection("framework-accessKey") {
		ival := AssertGetRemoteConfigInt64("framework-accessKey", "interval")
		tval := time.Duration(ival) * time.Millisecond

		AssertInit(InitAccessKeyMgr(tval, localAddr))
	}

	if HasSection("framework-logerror") {
		InitErrorCollector("framework-logerror")
	} else {
		GetLogger().Warningf("Configuration file do not contain section 'framework-logerror'")
	}

	if HasSection("framework-mailerror") {
		InitErrorMailer("framework-mailerror")
	} else {
		GetLogger().Warningf("Configuration file do not contain section 'framework-mailerror'")
	}

	if HasSection("framework-logkinesis") {
		InitLogKinesis("framework-logkinesis")
	} else {
		GetLogger().Warningf("Configuration file do not contain section 'framework-logkinesis'")
	}

	return nil
}

func (self *FrameworkNormalServer) OnInitZookeeper() error {
	addr := AssertGetRemoteConfig("framework-zookeeper", "zklist")
	user := AssertGetRemoteConfig("framework-zookeeper", "username")
	pswd := AssertGetRemoteConfig("framework-zookeeper", "password")
	tval := AssertGetRemoteConfigInt64("framework-zookeeper", "timeout")
	localAddr := AssertGetConfig("framework-local", "local_address")

	addrList := strings.Split(addr, ",")
	tout := time.Duration(tval) * time.Millisecond

	AssertInit(InitZookeeper(addrList, user, pswd, tout, localAddr))
	return nil
}

func (self *FrameworkNormalServer) OnCreateService() error { return nil }
func (self *FrameworkNormalServer) OnStart() error         { return nil }
func (self *FrameworkNormalServer) OnStop() error          { return nil }

func AssertInit(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func AssertGetConfig(section, option string) string {

	raw, err := GetConfigs().GetString(section, option)
	if err != nil {
		log.Fatalln(err)
	}

	return raw
}

func AssertGetConfigInt64(section, option string) int64 {

	raw, err := GetConfigs().GetInt64(section, option)
	if err != nil {
		log.Fatalln(err)
	}

	return raw
}

func AssertGetRemoteConfig(section, option string) string {

	var raw string
	var err error
	if GetRemoteConfigs().GetServiceType() == "local" || GetRemoteConfigs().GetConfigLen() == 0 {
		raw, err = GetConfigs().GetString(section, option)
	} else {
		raw, err = GetRemoteConfigs().GetString(section, option)
	}

	if err != nil {
		log.Fatalln(err)
	}

	return raw
}

func AssertGetRemoteConfigInt64(section, option string) int64 {

	var raw int64
	var err error
	if GetRemoteConfigs().GetServiceType() == "local" || GetRemoteConfigs().GetConfigLen() == 0 {
		raw, err = GetConfigs().GetInt64(section, option)
	} else {
		raw, err = GetRemoteConfigs().GetInt64(section, option)
	}
	if err != nil {
		log.Fatalln(err)
	}

	return raw
}

func HasSection(section string) bool {

	if GetRemoteConfigs().GetServiceType() == "local" || GetRemoteConfigs().GetConfigLen() == 0 {
		return GetConfigs().HasSection(section)
	} else {
		return GetRemoteConfigs().HasSection(section)
	}
}

func InitErrorCollector(section string) {
	producer := AssertGetRemoteConfig(section, "producer")
	hn, _ := os.Hostname()
	producer = producer + "@" + hn
	user := AssertGetRemoteConfig(section, "user")
	pass := AssertGetRemoteConfig(section, "pass")
	host := AssertGetRemoteConfig(section, "host")
	port := AssertGetRemoteConfig(section, "port")
	db := AssertGetRemoteConfig(section, "db")

	ds := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", user, pass, host, port, db)
	dbConn, err := sql.Open("mysql", ds)
	if err != nil {
		log.Printf("InitErrorCollector connect to error db failed: %s\n", err.Error())
		return
	}
	GetLogger().SetErrorCollector(producer, dbConn)
}

func InitErrorMailer(section string) {
	producer := AssertGetRemoteConfig(section, "producer")
	hn, _ := os.Hostname()
	producer = producer + "@" + hn
	host := AssertGetRemoteConfig(section, "host")
	port := AssertGetRemoteConfigInt64(section, "port")
	user := AssertGetRemoteConfig(section, "user")
	pass := AssertGetRemoteConfig(section, "pass")
	receiver := AssertGetRemoteConfig(section, "receiver")

	GetLogger().SetErrorMailer(producer, host, int(port), user, pass, receiver)
}

func InitLogKinesis(section string) {
	producer := AssertGetRemoteConfig(section, "aws_kinesis_producer")
	hn, _ := os.Hostname()
	producer = producer + "@" + hn
	stream := AssertGetRemoteConfig(section, "aws_kinesis_stream")
	region := AssertGetRemoteConfig(section, "aws_kinesis_region")
	access := AssertGetRemoteConfig(section, "aws_access_key")
	secret := AssertGetRemoteConfig(section, "aws_secret_key")

	GetLogger().SetKinesisLogProducer(producer, stream, region, access, secret)
}
