/*
* @Author: Jingkai Mao (oammix@gmail.com)
* @Date:   2015-01-21 13:26:24
 */

package framework

import (
	"errors"
	"fmt"

	"git.tap4fun.com/platform-server/common/logext"
	//"git.tap4fun.com/platform-server/common/netext"
	"math/rand"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"strconv"
	"syscall"
	"time"

	. "git.tap4fun.com/platform-server/common/protocol"
	"git.tap4fun.com/platform-server/mthird/thrift/lib/go/thrift"
)

var (
	// framework errors
	ErrFrameworkNotReady       = errors.New("framework-not-ready.")
	ErrDoubleInit              = errors.New("duplicated-initialization.")
	ErrUndefinedScheme         = errors.New("undefined-scheme.")
	ErrFailedToStartService    = errors.New("failed-start-service.")
	ErrFailedToRegisterService = errors.New("no-running-service-to-register.")

	// consts
	kDefaultStopTimeout   = time.Duration(32) * time.Second
	kDefaultServerTimeout = time.Duration(36000) * time.Millisecond
	kDefaultQuitMessage   = []byte((Error{ErrServiceUnavailable, "service-unavailable"}).Marshal())
)

type FrameworkServer interface {
	OnInit(map[string]string) error
	OnInitZookeeper() error
	OnCreateService() error
	OnStart() error
	OnStop() error
}

type Framework interface {
	StartService(string, string, thrift.TProcessor) error
	StartSecureService(string, string, string, string, thrift.TProcessor) error
	RegisterService(string, string) error

	Launch() error
	Serve()
}

type tFramework struct {
	Behavior FrameworkServer
	Services map[string]thrift.THTTPService

	ProjectName string
	BuildDate   string
	Version     string
	SourcesHash string

	localAddr string
}

func NewFramework(
	server FrameworkServer, name, date, ver, hash string) (Framework, error) {

	// launch default logger first
	if err := InitStdLogger(logext.LogLevelInfo, 0); err != nil {
		return nil, err
	}

	//
	rand.Seed(time.Now().UnixNano())
	runtime.GOMAXPROCS(runtime.NumCPU()*2 + 2)

	//
	GetLogger().Infof("-------------------------------------------------------------")
	GetLogger().Infof("| Module     : %s(%s)", name, ver)
	GetLogger().Infof("| BuildDate  : %s", date)
	GetLogger().Infof("| Version    : %s", hash)
	GetLogger().Infof("| Copyright (c) 2015 tap4fun.com, Inc. All Rights Reserved")
	GetLogger().Infof("-------------------------------------------------------------")

	//
	var limit syscall.Rlimit
	GetLogger().Infof("| RLimit     -")
	syscall.Getrlimit(syscall.RLIMIT_NOFILE, &limit)
	GetLogger().Infof("|    Nofile  : (curr -> %d, max -> %d)", limit.Cur, limit.Max)
	syscall.Getrlimit(syscall.RLIMIT_CORE, &limit)
	GetLogger().Infof("|    Core    : (curr -> %d, max -> %d)", limit.Cur, limit.Max)
	GetLogger().Infof("-------------------------------------------------------------")

	//

	return &tFramework{
		Behavior:    server,
		Services:    map[string]thrift.THTTPService{},
		ProjectName: name,
		BuildDate:   date,
		Version:     ver,
		SourcesHash: hash}, nil
}

// StartService will launch a thrift listener in http protocol.
func (self *tFramework) StartService(name, port string, processor thrift.TProcessor) error {

	return self.LaunchService(name, port, thrift.NewTHTTPServer(
		fmt.Sprintf(":%s", port),
		kDefaultServerTimeout,
		processor,
		GetLogger(),
		kDefaultQuitMessage))
}

// StartSecureService will launch thrift listener to specified port,
// if port == 0, a random & free port is used.
func (self *tFramework) StartSecureService(
	name, fkey, fcert, port string, processor thrift.TProcessor) error {

	return self.LaunchService(name, port, thrift.NewTHTTPSServer(
		fmt.Sprintf(":%s", port),
		kDefaultServerTimeout,
		processor,
		GetLogger(),
		fkey, fcert,
		kDefaultQuitMessage))
}

// LaunchService is a helper to StartService and StartSecureService
func (self *tFramework) LaunchService(name, port string, server thrift.THTTPService) error {

	if _, found := self.Services[name]; found {
		return ErrDoubleInit
	}

	failed := make(chan bool)
	client := make(chan string)
	go func() {
		if err := server.Serve(client); err != nil {
			GetLogger().Errorf("[1/2] failed to start %s service at [:%s]", name, port)
			GetLogger().Errorf("[2/2] %s...", err.Error())
			failed <- true
		}
	}()

	select {
	case <-failed:
		return ErrFailedToStartService
	case <-client:
		self.Services[name] = server
		GetLogger().Infof("started %s service at [:%d]", name, server.GetListenedPort())
	}

	return nil
}

// RegisterService register service's name as znode to zookeeper in empheral mode,
// send heartbeat every second to keep node from expired.
func (self *tFramework) RegisterService(name, servType string) error {
	if servType != "http" && servType != "https" {
		GetLogger().Errorf("invalid service type %s.only support https or http", servType)
		return ErrFailedToRegisterService
	}
	item, found := self.Services[name]
	if !found {
		return ErrFailedToRegisterService
	}

	port := strconv.Itoa(int(item.GetListenedPort()))
	return GetZookeeper().RegisterService(name, self.Version, servType, port, true)
}

// try to launch server, history reasons cause some duplicated codes.
func (self *tFramework) Launch() error {

	// register some environments variables
	envs := map[string]string{
		"name":    self.ProjectName,
		"version": self.Version,
		"hash":    self.SourcesHash,
	}

	// init system features
	if err := self.Behavior.OnInit(envs); err != nil {
		return err
	}

	// deprecated, init zookeeper, should be done at OnInit(map[string]string)
	if err := self.Behavior.OnInitZookeeper(); err != nil {
		return err
	}

	// init services specified features
	if err := self.Behavior.OnStart(); err != nil {
		return err
	}

	// create thrift service and register to zookeeper
	if err := self.Behavior.OnCreateService(); err != nil {
		return err
	}

	return nil
}

// Serve is used to block our main thread, until we receives system exit signal.
// before shut down server, we should handle old requests properly.
func (self *tFramework) Serve() {
	// wait to stop
	ch := make(chan os.Signal)
	signal.Notify(ch,
		os.Interrupt, syscall.SIGINT, syscall.SIGTERM,
		syscall.SIGQUIT, syscall.SIGHUP)

	sig := <-ch
	module := filepath.Base(os.Args[0])

	GetLogger().Infof("module %s received signal: %v...", module, sig)

	done := make(chan bool, 1)
	go func() { //
		// remove all the znodes of zookeepers
		GetZookeeper().Close()

		// gracefully stop thrift services
		for _, service := range self.Services {
			service.Stop()
		}

		// touch user defined onStop handler
		if err := self.Behavior.OnStop(); err != nil {
			GetLogger().Error("failed to call OnStop()")
		}

		//
		close(done)
	}()

	select {
	case <-done:
		GetLogger().Infof("module %s stoped gracefully...", module)
		GetLogger().Flush()
	case <-time.After(kDefaultStopTimeout):
		GetLogger().Infof("module %s stoped timeout...", module)
		GetLogger().Flush()
	}
}
