package framework

import (
	"encoding/json"
	"fmt"
	"strings"
	"sync"
	"time"

	"git.tap4fun.com/platform-server/common/netext"
	"git.tap4fun.com/platform-server/common/protocol"
)

type tAccessKeyElement struct {
	Key  string `json:"key"`
	Time int64  `json:"time"`
}

type tAccessToken struct {
	GameId   string `json:"game_id"`
	Token    string `json:"token"`
	Scopes   string `json:"scopes"`
	Key      string `json:"key"`
	ExpireAt uint64 `json:"expire_at"`
}

func (self *tAccessToken) ParseAccessToken() (protocol.AccessToken, error) {
	return nil, nil
}

type tAccessKeys struct {
	Keys   [3]tAccessKeyElement `json:"keys"`
	Tokens []tAccessToken       `json:"tokens"`
}

type tAccessKeyMgr struct {
	keys      tAccessKeys
	mutex     sync.RWMutex
	interval  time.Duration
	localAddr string
}

type AccessKeyMgr interface {
	ParseAccessToken(string) (protocol.AccessToken, error)
	GetKeys() [3]tAccessKeyElement
	ExpireAt() int64
}

func NewAccessKeyMgr(interval time.Duration, localAddr string) (AccessKeyMgr, error) {

	manager := &tAccessKeyMgr{
		interval: interval, localAddr: localAddr}

	if keys, tokens, err := manager.Update(); err != nil {
		return nil, err
	} else {
		manager.mutex.Lock()
		manager.keys.Keys = keys
		manager.keys.Tokens = tokens
		manager.mutex.Unlock()
	}

	go manager.Routine()
	return manager, nil
}

func (self *tAccessKeyMgr) ParseStaicScopes(raw string) (protocol.AccessToken, error) {
	self.mutex.RLock()
	defer self.mutex.RUnlock()
	for _, sToken := range self.keys.Tokens {
		if strings.TrimSpace(sToken.Token) == strings.TrimSpace(raw) {
			token, err := protocol.ParseAccessTokenEx(raw, sToken.Key)
			if err != nil {
				return nil, err
			}
			if strings.TrimSpace(sToken.GameId) == strings.TrimSpace(token.GetGameId()) {
				return token, nil
			}
		}
	}
	return nil, protocol.Error{protocol.ErrInvalidAccessToken, "invalide-static-token."}
}

func (self *tAccessKeyMgr) ParseAccessToken(raw string) (protocol.AccessToken, error) {
	// 验证静态token权限
	if token, err := self.ParseStaicScopes(raw); err == nil {
		return token, nil
	}
	// 1. verify access token by fixed salt in protocal
	if token, err := protocol.ParseAccessToken(raw); err == nil {
		return token, nil
	}

	// 2. verify access token by serial keys
	serial, err := protocol.GetSerialFromAccessTokenEx(raw)
	if err != nil {
		return nil, err
	}

	key, err := self.GetKey(serial)
	if err != nil {
		return nil, err
	}

	token, err := protocol.ParseAccessTokenEx(raw, key)
	if err != nil {
		return nil, err
	}

	return token, nil
}

func (self *tAccessKeyMgr) Routine() {

	for {
		<-time.After(self.interval)

		keys, tokens, err := self.Update()
		if err != nil {
			GetLogger().Errorf("failed to update access key from local service, %s", err.Error())
			continue
		}

		needUpdate := false
		self.mutex.RLock()
		if keys[0].Time != self.keys.Keys[0].Time {
			needUpdate = true
		}
		self.mutex.RUnlock()

		self.mutex.Lock()
		self.keys.Tokens = tokens
		if needUpdate {
			self.keys.Keys = keys
		}
		self.mutex.Unlock()

	}
}

func (self *tAccessKeyMgr) GetKey(serial int64) (string, error) {
	self.mutex.RLock()
	defer self.mutex.RUnlock()
	for _, key := range self.keys.Keys {
		if key.Time == serial {
			return key.Key, nil
		}
	}
	return "", protocol.Error{
		protocol.ErrInvalidAccessToken,
		"invalid-access-token(expired-serial)."}
}

func (self *tAccessKeyMgr) GetKeys() [3]tAccessKeyElement {
	var keys [3]tAccessKeyElement
	self.mutex.RLock()
	keys = self.keys.Keys
	self.mutex.RUnlock()
	return keys
}

func (self *tAccessKeyMgr) ExpireAt() int64 {
	ak := self.GetKeys()
	return time.Now().Unix() + (ak[2].Time-ak[1].Time)/2
}

func (self *tAccessKeyMgr) Update() ([3]tAccessKeyElement, []tAccessToken, error) {

	httpAddr := fmt.Sprintf("http://%s", self.localAddr)
	client := netext.NewHttpClient(httpAddr)

	rawRsp, err := client.Get("/accesskey", map[string]string{})

	var defkeys [3]tAccessKeyElement
	var defTokens []tAccessToken
	if err != nil {
		return defkeys, defTokens, protocol.Error{
			protocol.ErrFailedNetworkRequest,
			fmt.Sprintf("failed-get-access-keys(%s).", err.Error())}
	}

	keys := &tAccessKeys{}
	if err := json.Unmarshal([]byte(rawRsp), keys); err != nil {
		return defkeys, defTokens, protocol.Error{
			protocol.ErrJsonUnmarshalFailed,
			fmt.Sprintf("failed-unmarshal-access-keys(%s).", err.Error())}
	}
	return keys.Keys, keys.Tokens, nil
}

//
type noAccessKeyMgr struct{}

func (self noAccessKeyMgr) ParseAccessToken(raw string) (protocol.AccessToken, error) {
	return protocol.ParseAccessToken(raw)
}

func (self noAccessKeyMgr) GetKeys() [3]tAccessKeyElement {
	return [3]tAccessKeyElement{}
}

func (self noAccessKeyMgr) ExpireAt() int64 {
	return 0
}
func NoopAccessKeyMgr() AccessKeyMgr {
	return noAccessKeyMgr{}
}
