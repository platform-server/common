/*
* @Author: Jingkai Mao (oammix@gmail.com)
* @Date:   2015-01-19 16:12:31
 */

package zookeeper

import (
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
	"sync/atomic"
	"time"

	"git.tap4fun.com/platform-server/common/config"
	. "git.tap4fun.com/platform-server/common/logext/forward"
	"git.tap4fun.com/platform-server/common/netext"
	"git.tap4fun.com/platform-server/common/zookeeper/zklib/zk"
)

var (
	ErrInvalidParams           = errors.New("invalid-params.")
	ErrFailedGetChildren       = errors.New("failed-get-children.")
	ErrServiceAlreadyRegisterd = errors.New("service-already-registered.")
	ErrRootNOTExists           = errors.New("zookeeper-root-not-exists.")
)

const (
	kZookeeperRootPath         = "/platform"
	kZookeeperHeatbeatInterval = 1 * time.Second
)

//
type ZookeeperClient interface {
	GetRunningServices(string, bool) ([]*Node, error)
	GetRandService(string) (string, int, error)
	GetServiceByMem(string) (string, int, error)
	GetServiceByCpu(string) (string, int, error)
	GetServiceByCM(service string, weightC, weightM float64) (host string, port int, err error)

	RegisterService(string, string, string, string, bool) error
	CreateZNode(string, []byte, int32) (string, error)
	EraseZNode(string, int32) error
	SetZNode(string, []byte, int32) (*zk.Stat, error)
	GetZNode(string) ([]byte, *zk.Stat, error)

	GetLocalService(string) *tZookeeperService
	Close()
}

//
type Heartbeat struct {
	Workload   uint32 `json:"workload"`
	Version    string `json:"version"`
	Rps        uint32 `json:"rps"`
	Pid        uint32 `json:"pid"`
	Address    string `json:"address"`
	InternalIP string `json:"internal_ip"`
	Scheme     string `json:"scheme"`
}

type Node struct {
	Host     string
	Port     int
	UserData []byte
}

type tZookeeperService struct {
	npath      string
	version    string
	address    string // ip:port
	pid        uint32
	workload   uint32 // rps, workload is reserved
	rps        uint32
	ctrl       chan bool // stop control channel
	internalIP string
	scheme     string
}

type tZookeeperClient struct {
	conn      *zk.Conn
	auth      []byte
	acl       []zk.ACL
	services  map[string]*tZookeeperService
	config    *goconfig.RemoteConfig
	localAddr string
}

// make sure tZookeeperClient implements all methods
var _ ZookeeperClient = (*tZookeeperClient)(nil)

// NewZookeeperClient returns a ZookeeperClient, which connects zkservers.
func NewClient(addr_list []string, user, pswd string, tout time.Duration, localAddr string, config *goconfig.RemoteConfig) (ZookeeperClient, error) {
	conn, _, err := zk.Connect(addr_list, tout)
	if err != nil {
		return nil, err
	}

	if len(user) <= 0 || len(pswd) <= 0 {
		return nil, ErrInvalidParams
	}

	auth := []byte(fmt.Sprintf("%s:%s", user, pswd))
	if err = conn.AddAuth("digest", auth); err != nil {
		return nil, err
	}

	// ACL permission pre-defines, Read-Only for others
	acl := zk.DigestACL(zk.PermAll, user, pswd)
	acl = append(acl, zk.WorldACL(zk.PermRead)...)

	return &tZookeeperClient{
		conn:      conn,
		auth:      auth,
		acl:       acl,
		services:  make(map[string]*tZookeeperService),
		config:    config,
		localAddr: localAddr,
	}, nil
}

// GetRunningServer receives service(string) as sub-directory of /platform/ to get data slice.
// refactor change a name
func (self *tZookeeperClient) GetRunningServices(service string, udata bool) ([]*Node, error) {

	path := fmt.Sprintf("/platform/%s", service)
	data, _, err := self.conn.Children(path) // sub paths
	if err != nil {
		return nil, ErrFailedGetChildren
	}

	response := []*Node{}
	for _, leaf := range data {
		parts := strings.SplitN(leaf, ":", 2)
		port, err := strconv.ParseInt(parts[1], 10, 32)
		if err != nil {
			GetGlobalLogger().Errorf("bad zookeeper leaf format, %s", leaf)
			continue
		}

		userdata := []byte{}
		if udata {
			fullpath := fmt.Sprintf("%s/%s", path, leaf)
			userdata, _, err = self.GetZNode(fullpath)
			if err != nil {
				GetGlobalLogger().Warningf("[1/2] failed to get userdata from %s,", fullpath)
				GetGlobalLogger().Warningf("[2/2] %v.", err)
			}
		}

		response = append(response,
			&Node{Host: parts[0], Port: int(port), UserData: userdata})
	}

	return response, nil
}

// GetRandService picks a service which has been registered randomly.
func (self *tZookeeperClient) GetRandService(service string) (host string, port int, err error) {

	nodes, err := self.GetRunningServices(service, false)
	if err != nil {
		return "", 0, err
	}

	if len(nodes) == 0 {
		return "", 0, fmt.Errorf("no-service-%s-found.", service)
	}

	i := time.Now().Unix() % int64(len(nodes))
	return nodes[i].Host, nodes[i].Port, nil
}

type tNodeUData struct {
	Status struct {
		Cpu struct {
			Min1  float64 `json:"min1"`
			Min5  float64 `json:"min5"`
			Min15 float64 `json:"min15"`
			avg   float64
		} `json:"cpu"`
		Memory struct {
			Free  int `json:"free"`
			Total int `json:"total"`
		} `json:"memory"`
	} `json:"status"`
}

// get service by free memory
func (self *tZookeeperClient) GetServiceByMem(service string) (host string, port int, err error) {
	nodes, err := self.GetRunningServices(service, true)
	if err != nil {
		return "", 0, err
	}

	if len(nodes) == 0 {
		return "", 0, fmt.Errorf("no-service-%s-found.", service)
	}

	i := 0
	f := 0
	for key, node := range nodes {
		nodeData := &tNodeUData{}
		if err := json.Unmarshal(node.UserData, nodeData); err != nil {
			i = int(time.Now().Unix() % int64(len(nodes)))
			break
		}
		if nodeData.Status.Memory.Free > f {
			i = key
			f = nodeData.Status.Memory.Free
		}

	}

	return nodes[i].Host, nodes[i].Port, nil

}

// get service by cpu load
func (self *tZookeeperClient) GetServiceByCpu(service string) (host string, port int, err error) {
	nodes, err := self.GetRunningServices(service, true)
	if err != nil {
		return "", 0, err
	}

	if len(nodes) == 0 {
		return "", 0, fmt.Errorf("no-service-%s-found.", service)
	}

	i := 0
	load := 10000.0
	for key, node := range nodes {
		nodeData := &tNodeUData{}
		if err := json.Unmarshal(node.UserData, nodeData); err != nil {
			i = int(time.Now().Unix() % int64(len(nodes)))
			break
		}
		nload := nodeData.Status.Cpu.Min1*0.3 + nodeData.Status.Cpu.Min5*0.3 + nodeData.Status.Cpu.Min15*0.4
		if nload < load {
			i = key
			load = nload
		}

	}

	return nodes[i].Host, nodes[i].Port, nil

}

// get service by cpu load
func (self *tZookeeperClient) GetServiceByCM(service string, weightC, weightM float64) (host string, port int, err error) {
	nodes, err := self.GetRunningServices(service, true)
	if err != nil {
		return "", 0, err
	}

	if len(nodes) == 0 {
		return "", 0, fmt.Errorf("no-service-%s-found.", service)
	}

	maxC := 0.001 // cpu load percentage
	maxM := 1     // free memory in MB unit
	stats := make([]*tNodeUData, len(nodes))
	for key, node := range nodes {
		nodeData := &tNodeUData{}
		if err := json.Unmarshal(node.UserData, nodeData); err != nil {
			i := int(time.Now().Unix() % int64(len(nodes)))
			return nodes[i].Host, nodes[i].Port, nil
		}
		c := nodeData.Status.Cpu.Min1*0.3 + nodeData.Status.Cpu.Min5*0.3 + nodeData.Status.Cpu.Min15*0.4
		nodeData.Status.Cpu.avg = c
		if maxC < c {
			maxC = c
		}
		m := nodeData.Status.Memory.Free
		if maxM < m {
			maxM = m
		}
		stats[key] = nodeData
	}

	i := 0
	weight := 0.0
	for key, stat := range stats {
		w := weightC*(float64(1.0)-stat.Status.Cpu.avg/maxC) +
			weightM*(float64(stat.Status.Memory.Free)/float64(maxM))
		if w > weight {
			i = key
			weight = w
		}

	}
	// fmt.Println(i, weight)

	return nodes[i].Host, nodes[i].Port, nil

}

// StopService closes a running zookeeper service by erasing node and stoping heatbeat.
func (self *tZookeeperClient) StopService(name string) error {

	if service, found := self.services[name]; found {
		close(service.ctrl)
		delete(self.services, name)
	}
	return nil
}

// RegisterService appends a znode about service on the node of `/platform`,
// for naming service.
func (self *tZookeeperClient) RegisterService(name, version, servType, port string, debug bool) error {

	if _, found := self.services[name]; found {
		return ErrServiceAlreadyRegisterd
	}

	// make sure root node exists. (/root)
	exists, _, err := self.conn.Exists(kZookeeperRootPath)
	if err != nil {
		return err
	}

	if !exists {
		_, err := self.CreateZNode(kZookeeperRootPath, []byte{}, 0)
		if err != nil {
			return err
		}
	}

	// make sure parent node exists. (/root/[name])
	parent := fmt.Sprintf("%s/%s", kZookeeperRootPath, name)
	exists, _, err = self.conn.Exists(parent)
	if err != nil {
		return err
	}

	if !exists {
		GetGlobalLogger().Infof("can't find zookeeper parent node[%s].", parent)
		_, err := self.CreateZNode(parent, []byte{}, 0) // trying to create parent node
		if err != nil {
			return err
		}
	}

	// create Ephemeral leaf node. (/root/[name]/[id]:[port])
	url := self.localAddr
	address := self.localAddr
	if name != "local" {

		url, err = netext.GetRedirectLocalUrl(self.localAddr)
		if err != nil { // a little bit of ugly code to address
			if debug {
				url, err = netext.GetExternalIpv4Address()
				if err != nil {
					return err
				}
			} else {
				return err
			}
		}
		address = fmt.Sprintf("%s:%s", strings.Trim(url, "/"), port)
	}

	leaf := fmt.Sprintf("%s/%s", parent, address)

	self.services[name] = &tZookeeperService{
		npath:      leaf,
		version:    version,
		address:    address,
		pid:        uint32(os.Getpid()),
		ctrl:       make(chan bool),
		internalIP: url,
		scheme:     servType,
	}
	go self.serve(name, self.services[name])
	return nil
}

// the second network adapter(eth1) is used for internal ip
func (self *tZookeeperClient) getInternalIPv4() string {
	var ifi *net.Interface
	var err error
	var adapters []string

	if self.config == nil {
		GetGlobalLogger().ELog("framework config is nil")
	} else {
		ia, err := self.config.GetString("Internal_Adapter", "addr")
		GetGlobalLogger().ILog("ia:%v, err:%v", ia, err)
		if ia != "" {
			GetGlobalLogger().ILog("internal adapter is %s", ia)
			adapters = []string{ia}
		}
	}

	if adapters == nil {
		adapters = []string{"eth1", "en1"}
	}

	for _, adapter := range adapters {
		ifi, err = net.InterfaceByName(adapter)
		if err == nil {
			break
		} else {
			GetGlobalLogger().WLog("net.InterfaceByName(%s) err:%s", adapter, err.Error())
		}
	}
	if ifi == nil {
		GetGlobalLogger().ELog("get internal ip failed")
		return ""
	}

	addrs, err := ifi.Addrs()
	if err != nil {
		GetGlobalLogger().ELog("get internal ip failed err:%s", err.Error())
		return ""
	}

	for _, addr := range addrs {
		if ipnet, ok := addr.(*net.IPNet); ok {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}

	GetGlobalLogger().ELog("get internal ip failed")
	return ""
}

func (self *tZookeeperClient) serve(name string, service *tZookeeperService) {

	_, err := self.bindServiceAsZnode(service)
	if err != nil && err != zk.ErrNodeExists {
		GetGlobalLogger().Warning("failed to register service [%s]...err:%s  %s", name, err.Error(), service.npath)
	} else {
		GetGlobalLogger().Infof("register service [%s] at [%s]", name, service.npath)
	}

	for {
		// sending heartbeat every second
		_, err := self.SetZNode(service.npath, service.GetHeartbeatPackage(), -1)
		switch err {
		case zk.ErrConnectionClosed: // failed to connect to zkServer
		case zk.ErrNoNode: // remote session down
			self.conn.AddAuth("digest", self.auth)
			if rsp, e := self.bindServiceAsZnode(service); e != nil { // re-register service
				GetGlobalLogger().Warningf("[1/2] remote zk session down, trying to register [%s] again...", name)
				GetGlobalLogger().Warningf("[2/2] failed to register service, %s", e.Error())
			} else {
				GetGlobalLogger().Infof("[1/2] remote zk session down, trying to register [%s] again...", name)
				GetGlobalLogger().Infof("[2/2] register service [%s] at [%s]", name, rsp)
			}
		case zk.ErrNoAuth: // lost auth informations
			if e := self.conn.AddAuth("digest", self.auth); e != nil { // add-auth of service
				GetGlobalLogger().Warningf("[1/2] remote zk auth lost, trying to bind [%s] again...", name)
				GetGlobalLogger().Warningf("[2/2] failed to add-auth of service, %s.", e.Error())
			} else {
				GetGlobalLogger().Infof("[1/2] remote zk auth lost, trying to bind [%s] again...", name)
				GetGlobalLogger().Infof("[2/2] add-auth of service [%s] successfully.", name)
			}
		default:
			if err != nil { // [NOTICE] we drop unknown error, dangerouse.
				GetGlobalLogger().Warningf("[1/2] unknown error happened when push heartbeat of service %s", name)
				GetGlobalLogger().Warningf("[2/2] %s...\n", err.Error())
			}
		}

		select {
		case _, opened := <-service.ctrl:
			if !opened {
				err := self.eraseServiceFromZNode(service)
				if err != nil {
					GetGlobalLogger().Warningf("failed to erase service from znode, %s", err)
				}
				return // stops this infinite loop
			}
		case <-time.After(kZookeeperHeatbeatInterval):
			break
		}
	}
}

func (self *tZookeeperClient) bindServiceAsZnode(service *tZookeeperService) (string, error) {
	rsp, err := self.CreateZNode(service.npath, service.GetHeartbeatPackage(), zk.FlagEphemeral)
	if err != nil {
		return "", err
	}
	return rsp, nil
}

func (self *tZookeeperClient) eraseServiceFromZNode(service *tZookeeperService) error {
	return self.EraseZNode(service.npath, -1)
}

// CreateZNode makes a znode with *data* in named *path*, *flags* should
// be one of 0, zk.FlagEphemeral, zk.FlagSequence
func (self *tZookeeperClient) CreateZNode(path string, data []byte, flags int32) (string, error) {
	return self.conn.Create(path, data, flags, self.acl)
}

// SetZNode modefies an znode when we got the previous *version,
// let *version* = -1 means we're willing to clobber whatever value might be there.
func (self *tZookeeperClient) SetZNode(path string, data []byte, version int32) (*zk.Stat, error) {
	return self.conn.Set(path, data, version)
}

// GetZNode returns the contents of a znode.
func (self *tZookeeperClient) GetZNode(path string) ([]byte, *zk.Stat, error) {
	return self.conn.Get(path)
}

// EraseZNode remove znode from zookeeper.
func (self *tZookeeperClient) EraseZNode(path string, version int32) error {
	return self.conn.Delete(path, version)
}

// GetLocalService returns the content of service.
func (self *tZookeeperClient) GetLocalService(name string) *tZookeeperService {
	if ser, found := self.services[name]; found {
		return ser
	}
	return nil
}

// Close should gracefully shutdown zookeeper client.
func (self *tZookeeperClient) Close() {
	for name, service := range self.services {
		close(service.ctrl)
		delete(self.services, name)
	}
}

// GetHeartbeatPackage returns a heartbeat string in json.
func (self *tZookeeperService) GetHeartbeatPackage() []byte {
	pkg := Heartbeat{
		Address:    self.address,
		Workload:   self.workload,
		Version:    self.version,
		Rps:        self.rps,
		Pid:        self.pid,
		InternalIP: self.internalIP,
		Scheme:     self.scheme,
	}

	jsonPkg, _ := json.Marshal(pkg) // no chance
	return jsonPkg
}

// Setters & Getters
func (self *tZookeeperService) GetWorkLoad(name string) uint32 {
	return self.workload
}

func (self *tZookeeperService) SetWorkLoad(name string, workload uint32) {
	atomic.StoreUint32(&self.workload, workload)
}

func (self *tZookeeperService) GetRps(name string) uint32 {
	return self.rps
}

func (self *tZookeeperService) SetRps(name string, rps uint32) {
	atomic.StoreUint32(&self.rps, rps)
}

//
type noZookeeper struct{}

func (self noZookeeper) GetRunningServices(string, bool) ([]*Node, error)           { return []*Node{}, nil }
func (self noZookeeper) RegisterService(string, string, string, string, bool) error { return nil }
func (self noZookeeper) CreateZNode(string, []byte, int32) (string, error)          { return "", nil }
func (self noZookeeper) EraseZNode(string, int32) error                             { return nil }
func (self noZookeeper) SetZNode(string, []byte, int32) (*zk.Stat, error)           { return nil, nil }
func (self noZookeeper) GetZNode(string) ([]byte, *zk.Stat, error)                  { return []byte{}, nil, nil }
func (self noZookeeper) GetRandService(string) (string, int, error)                 { return "", 0, nil }
func (self noZookeeper) GetServiceByMem(string) (string, int, error)                { return "", 0, nil }
func (self noZookeeper) GetServiceByCpu(string) (string, int, error)                { return "", 0, nil }
func (self noZookeeper) GetServiceByCM(string, float64, float64) (string, int, error) {
	return "", 0, nil
}
func (self noZookeeper) GetLocalService(string) *tZookeeperService { return nil }
func (self noZookeeper) Close()                                    {}

// NoopZookeeper returns a struct that satisfies the ZookeeperClient interface
// but silently ignores all ZookeeperClient method invocations.
func Noop() ZookeeperClient {
	return noZookeeper{}
}
