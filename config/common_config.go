package goconfig

import (
	"fmt"
)

type CommonConfig struct {
	LocalCfg  *ConfigFile
	RemoteCfg *RemoteConfig
}

func NewCommonConfig(localCfg *ConfigFile, remoteCfg *RemoteConfig) *CommonConfig {
	return &CommonConfig{
		LocalCfg:  localCfg,
		RemoteCfg: remoteCfg,
	}
}

var ErrCfgNotInit = fmt.Errorf("config data not init!")

func (cfg *CommonConfig) GetSections() (sections []string) {
	s_map := make(map[string]bool)
	if cfg.LocalCfg != nil {
		for _, s := range cfg.LocalCfg.GetSections() {
			s_map[s] = true
		}
	}
	if cfg.RemoteCfg != nil {
		for _, s := range cfg.RemoteCfg.GetSections() {
			s_map[s] = true
		}
	}
	s_arr := make([]string, 0)
	for s, _ := range s_map {
		s_arr = append(s_arr, s)
	}
	return s_arr
}

func (cfg *CommonConfig) HasSection(section string) bool {
	if cfg.LocalCfg != nil && cfg.LocalCfg.HasSection(section) {
		return true
	}
	if cfg.RemoteCfg != nil && cfg.RemoteCfg.HasSection(section) {
		return true
	}
	return false
}

func (cfg *CommonConfig) GetOptions(section string) ([]string, error) {
	o_map := make(map[string]bool)
	o_arr := make([]string, 0)
	if cfg.LocalCfg != nil && cfg.LocalCfg.HasSection(section) {
		options, err := cfg.LocalCfg.GetOptions(section)
		if err != nil {
			return o_arr, err
		}
		for _, o := range options {
			o_map[o] = true
		}
	}
	if cfg.RemoteCfg != nil && cfg.RemoteCfg.HasSection(section) {
		options, err := cfg.RemoteCfg.GetOptions(section)
		if err != nil {
			return o_arr, err
		}
		for _, o := range options {
			o_map[o] = true
		}
	}

	for o, _ := range o_map {
		o_arr = append(o_arr, o)
	}
	return o_arr, nil
}

func (cfg *CommonConfig) HasOption(section string, option string) bool {
	if cfg.LocalCfg != nil && cfg.LocalCfg.HasSection(section) && cfg.LocalCfg.HasOption(section, option) {
		return true
	}
	if cfg.RemoteCfg != nil && cfg.RemoteCfg.HasSection(section) && cfg.LocalCfg.HasOption(section, option) {
		return true
	}
	return false
}

func (cfg *CommonConfig) GetRawString(section string, option string) (string, error) {
	if cfg.LocalCfg != nil && cfg.LocalCfg.HasSection(section) {
		if s, err := cfg.LocalCfg.GetRawString(section, option); err == nil {
			return s, nil
		}
	}
	if cfg.RemoteCfg != nil && cfg.RemoteCfg.HasSection(section) {
		return cfg.RemoteCfg.GetRawString(section, option)
	}
	return "", ErrCfgNotInit
}

func (cfg *CommonConfig) GetString(section string, option string) (string, error) {
	if cfg.LocalCfg != nil && cfg.LocalCfg.HasSection(section) {
		if s, err := cfg.LocalCfg.GetString(section, option); err == nil {
			return s, nil
		}
	}
	if cfg.RemoteCfg != nil && cfg.RemoteCfg.HasSection(section) {
		return cfg.RemoteCfg.GetString(section, option)
	}
	return "", ErrCfgNotInit
}

func (cfg *CommonConfig) GetInt(section string, option string) (int, error) {
	if cfg.LocalCfg != nil && cfg.LocalCfg.HasSection(section) {
		if s, err := cfg.LocalCfg.GetInt(section, option); err == nil {
			return s, nil
		}
	}
	if cfg.RemoteCfg != nil && cfg.RemoteCfg.HasSection(section) {
		return cfg.RemoteCfg.GetInt(section, option)
	}
	return 0, ErrCfgNotInit
}
func (cfg *CommonConfig) GetFloat(section string, option string) (float64, error) {
	if cfg.LocalCfg != nil && cfg.LocalCfg.HasSection(section) {
		if s, err := cfg.LocalCfg.GetFloat(section, option); err == nil {
			return s, nil
		}
	}
	if cfg.RemoteCfg != nil && cfg.RemoteCfg.HasSection(section) {
		return cfg.RemoteCfg.GetFloat(section, option)
	}
	return 0, ErrCfgNotInit
}

func (cfg *CommonConfig) GetBool(section string, option string) (bool, error) {

	if cfg.LocalCfg != nil && cfg.LocalCfg.HasSection(section) {
		if s, err := cfg.LocalCfg.GetBool(section, option); err == nil {
			return s, nil
		}
	}
	if cfg.RemoteCfg != nil && cfg.RemoteCfg.HasSection(section) {
		return cfg.RemoteCfg.GetBool(section, option)
	}
	return false, ErrCfgNotInit
}

func (cfg *CommonConfig) GetInt64(section string, option string) (int64, error) {
	if cfg.LocalCfg != nil && cfg.LocalCfg.HasSection(section) {
		if s, err := cfg.LocalCfg.GetInt64(section, option); err == nil {
			return s, nil
		}
	}
	if cfg.RemoteCfg != nil && cfg.RemoteCfg.HasSection(section) {
		return cfg.RemoteCfg.GetInt64(section, option)
	}
	return 0, ErrCfgNotInit
}
