package goconfig

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

type RemoteConfig struct {
	ServerType   string
	ServerConfig map[string](map[string]string)
	Config       *ConfigFile
}

func GetRemoteConfig(conf *ConfigFile) *RemoteConfig {
	return &RemoteConfig{Config: conf}
}

func (self *RemoteConfig) GetServiceType() string {
	return self.ServerType
}

func (self *RemoteConfig) UseLocal() bool {
	for _, s := range []string{"local", "config_server"} {
		if self.ServerType == s {
			return true
		}
	}
	return false
}

func (self *RemoteConfig) Empty() bool {
	return self.GetConfigLen() == 0
}

func (self *RemoteConfig) GetConfigLen() int32 {
	return int32(len(self.ServerConfig))
}

func (self *RemoteConfig) InitConfig(service, version, tag, localAddr string) error {

	self.ServerType = service
	fmt.Println("start get remote config", service)
	if service == "local" || service == "config_server" {
		//local server, no need to get config from remote local
		return nil
	}

	m := url.Values{}
	m.Set("server_name", service)
	m.Set("version", version)
	m.Set("tag", tag)
	url := fmt.Sprintf("http://%s/configuration?%s", localAddr, m.Encode())
	resp, err := http.Get(url)

	if err != nil {
		return err
	}
	defer resp.Body.Close()

	var body []byte
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	self.ServerConfig = make(map[string](map[string]string), 0)
	config := make(map[string](map[string]string), 0)
	//Item := make(map[string]string, 0)

	json.Unmarshal(body, &config)
	for key, sectionItem := range config {

		var Item map[string]string
		section := strings.ToLower(key)

		if len(self.ServerConfig[section]) == 0 {
			Item = make(map[string]string, 0)
		} else {
			Item = self.ServerConfig[section]
		}

		for subKey, value := range sectionItem {
			Item[strings.ToLower(subKey)] = value
		}

		self.ServerConfig[section] = Item
	}
	fmt.Println(self.ServerConfig)
	return nil
}

func (self *RemoteConfig) HasSection(section string) bool {

	if self.ServerType == "local" || self.GetConfigLen() == 0 {
		return self.Config.HasSection(section)
	} else {

		_, ok := self.ServerConfig[strings.ToLower(section)]
		return ok
	}
}

func (self *RemoteConfig) HasOption(section string, option string) bool {

	section = strings.ToLower(section)
	option = strings.ToLower(option)
	if self.ServerType == "local" || self.GetConfigLen() == 0 {
		return self.Config.HasOption(section, option)
	}
	if s, ok := self.ServerConfig[section]; ok {
		if _, ok1 := s[option]; ok1 {
			return true
		}
	}

	return false
}

func (self *RemoteConfig) GetSections() (sections []string) {
	if self.ServerType == "local" || self.GetConfigLen() == 0 {
		return self.Config.GetSections()
	}
	sections = make([]string, len(self.ServerConfig))

	for section, _ := range self.ServerConfig {
		sections = append(sections, section)
	}
	return sections
}

func (self *RemoteConfig) GetOptions(section string) ([]string, error) {

	section = strings.ToLower(section)
	if self.ServerType == "local" || self.GetConfigLen() == 0 {
		return self.Config.GetOptions(section)
	}
	if _, ok := self.ServerConfig[section]; !ok {
		return nil, fmt.Errorf("Section not found: %s", section)

	}

	options := make([]string, len(self.ServerConfig[DefaultSection]))

	for k, _ := range self.ServerConfig[DefaultSection] {
		options = append(options, k)
	}

	for k, _ := range self.ServerConfig[section] {
		options = append(options, k)
	}

	return options, nil
}

func (self *RemoteConfig) GetRawString(section string, option string) (string, error) {

	section = strings.ToLower(section)
	option = strings.ToLower(option)

	if self.ServerType == "local" || self.GetConfigLen() == 0 {
		return self.Config.GetRawString(section, option)
	}

	if _, ok := self.ServerConfig[section]; ok {

		if value, ok := self.ServerConfig[section][option]; ok {
			return value, nil
		}

		return "", errors.New(fmt.Sprintf("Option(%s) not found at %s", option, section))
	}
	// fmt.Printf("server_config %v", self.ServerConfig)
	return "", errors.New(fmt.Sprintf("Section not found: %s!", section))
}

func (self *RemoteConfig) GetString(section string, option string) (string, error) {

	if self.ServerType == "local" || self.GetConfigLen() == 0 {
		return self.Config.GetString(section, option)
	}

	str, err := self.GetRawString(section, option)
	return str, err
}

func (self *RemoteConfig) GetInt64(section string, option string) (int64, error) {

	if self.ServerType == "local" || self.GetConfigLen() == 0 {
		return self.Config.GetInt64(section, option)
	}

	sv, err := self.GetString(section, option)
	if err != nil {
		return 0, err
	}

	value, err := strconv.ParseInt(sv, 10, 64)
	if err != nil {
		return 0, err
	}

	return value, nil
}

func (self *RemoteConfig) GetInt(section string, option string) (int, error) {

	if self.ServerType == "local" || self.GetConfigLen() == 0 {
		return self.Config.GetInt(section, option)
	}

	i64, err := self.GetInt64(section, option)
	return int(i64), err
}

func (self *RemoteConfig) GetFloat(section string, option string) (float64, error) {
	if self.ServerType == "local" || self.GetConfigLen() == 0 {
		return self.Config.GetFloat(section, option)
	}

	sv, err := self.GetString(section, option)
	if err != nil {
		return float64(0), err
	}

	value, err := strconv.ParseFloat(sv, 64)
	if err != nil {
		return float64(0), err
	}

	return value, nil
}

func (self *RemoteConfig) GetBool(section string, option string) (bool, error) {

	if self.ServerType == "local" || self.GetConfigLen() == 0 {
		return self.Config.GetBool(section, option)
	}

	sv, err := self.GetString(section, option)
	if err != nil {
		return false, err
	}

	value, ok := BoolStrings[strings.ToLower(sv)]
	if ok == false {
		return false, errors.New(
			fmt.Sprintf("Could not parse bool value: %s", sv),
		)
	}

	return value, nil
}

func (self *RemoteConfig) DisplayConfigs() {

	if self.ServerType == "local" || self.GetConfigLen() == 0 {
		self.Config.DisplayConfigs()
		return
	}
	fmt.Println("Configuration is from remote local server.")

	for k, records := range self.ServerConfig {
		fmt.Println("\n[", k, "]")
		for k1, value := range records {
			fmt.Println(k1, "=", value)
		}
	}
	fmt.Println("")
}
