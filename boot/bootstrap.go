package boot

import (
	goconfig "git.tap4fun.com/platform-server/common/config"
)

func (manager *BootManager) UpdateLogKinesis(config *goconfig.ConfigFile) error {
	if config == nil {
		manager.Logger.Infof("config is null,need't update framework kinesis !")
		return nil
	}
	if !manager.SetKinesis {
		manager.Logger.Infof("SetKinesis is false,need't update framework kinesis !")
		return nil
	}
	if !config.HasOption("Framework-LogKinesis", "aws_kinesis_producer") {
		manager.Logger.Infof("framework-logkinesis.aws_kinesis_producer is null,need't update logkinesis !")
		return nil
	}
	if !config.HasOption("Framework-LogKinesis", "aws_kinesis_stream") {
		manager.Logger.Infof("framework-logkinesis.aws_kinesis_stream is null,need't update logkinesis !")
		return nil
	}
	aws_kinesis_producer, _ := config.GetString("Framework-LogKinesis", "aws_kinesis_producer")
	aws_kinesis_stream, _ := config.GetString("Framework-LogKinesis", "aws_kinesis_stream")
	if aws_kinesis_producer == "" || aws_kinesis_stream == "" {
		manager.Logger.Infof(" aws_kinesis_producer or aws_kinesis_stream is empty ,need't update logkinesis ! %s,%s", aws_kinesis_producer, aws_kinesis_stream)
		return nil
	}
	if !config.HasOption("Framework-LogKinesis", "aws_scope") {
		manager.Logger.Infof("framework-logkinesis.aws_scope is null,need't update logkinesis !")
		return nil
	}
	aws_scope, _ := config.GetString("Framework-LogKinesis", "aws_scope")
	if aws_scope == "" {
		aws_scope = DEDAULT
	}
	client := manager.Aws.GetAwsClient(aws_scope)
	if client == nil {
		manager.Logger.Errorf("framework-logkinesis.aws_scope %s can not find aws client", aws_scope)
		return nil
	}

	kins, _ := client.SetKienesis(aws_kinesis_producer, aws_kinesis_stream)
	manager.Logger.SetKinesisLogProducerWithSession(kins.Producer, kins.Stream, kins.Kinesis)
	manager.Logger.Infof("init kienesis success")
	return nil
}

// func (manager *BootManager) UpdateKinesisPro
