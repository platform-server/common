package boot

import (
	"encoding/json"
	"fmt"
	goconfig "git.tap4fun.com/platform-server/common/config"
	"git.tap4fun.com/platform-server/common/logext"
	"git.tap4fun.com/platform-server/common/netext"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"os/user"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	CcpModePrograme = "programe"
	CcpModeCMD      = "cmd"
)

const (
	SvcAWS      = "aws"
	SvcPrograme = "programe"
)

var (
	httpClient = &http.Client{
		Timeout: 3 * time.Second,
	}
)

func IsFrameWorkService(s, t string) bool {
	return fmt.Sprintf("framework-%s", t) == s
}

type CcpMode struct {
	Mode string
	Name string
}

func NewCcpMode(mode, name string) *CcpMode {
	return &CcpMode{
		Mode: mode,
		Name: name,
	}
}

func ParseCcpMode(str string) (*CcpMode, error) {
	arr := strings.Split(str, ":")
	if arr[0] != CcpModeCMD && arr[0] != CcpModePrograme {
		return nil, ErrCcpUnsupportMode
	}
	if arr[0] == CcpModePrograme {
		if len(arr) < 2 {
			return nil, ErrCcpUnsupportMode
		}
		return &CcpMode{Mode: arr[0], Name: arr[1]}, nil
	}
	return &CcpMode{Mode: arr[0]}, nil
}

func (m *CcpMode) ToString() string {
	if m.Mode == CcpModeCMD {
		return m.Mode
	}
	return fmt.Sprintf("%s:%s", m.Mode, m.Name)
}

//匹配逻辑需要再细化一下
//aws 匹配 aws:所有的
func IsMatchNode(node_matcher string, node_name string) bool {
	if node_matcher == node_name {
		return true
	}
	a0 := make([]string, 3)
	a1 := make([]string, 3)
	for i, s := range strings.Split(node_matcher, ":") {
		if i < 3 {
			a0[i] = s
		}
	}
	for i, s := range strings.Split(node_name, ":") {
		if i < 3 {
			a1[i] = s
		}
	}
	for i := 0; i < 3; i++ {
		if !_match_string(a0[i], a1[i]) {
			return false
		}
	}
	return true
}

func _match_string(s1, s2 string) bool {
	if s1 == s2 || s1 == "*" || s1 == "" {
		return true
	}
	return false
}

func (m *CcpMode) IsSame(node string) bool {
	arr := strings.Split(node, ":")
	if m.Mode == CcpModePrograme && arr[0] == SvcPrograme {
		if len(arr) < 2 {
			return false
		}
		return m.Name == strings.Join(arr[1:], ":")
	}
	if m.Mode == CcpModeCMD {
		for _, s := range []string{SvcAWS} {
			if arr[0] == s {
				return true
			}
		}
	}
	return false

}

type CcpManager struct {
	NodeMatcher          string
	Env                  string
	ProgrameConfig       *goconfig.ConfigFile
	ProgrameConfigLocker sync.RWMutex
	RefreshTicker        *time.Ticker
	PauseLocker          sync.RWMutex
	IsPause              bool
	IsWriteFile          bool
	LocaltionInfo        *netext.LocationInfo
	CfgServerAdd         string
	IopRoles             string
	IopProject           string
	ClientIp             string
	Logger               logext.Logger
	CcpProcessors        []CcpProcessor
}

var gConfigServerMap = map[string]map[string]string{
	"us-west-2": map[string]string{
		"beta": "http://internal-pf-beta-dev-http-6000-2044261051.us-west-2.elb.amazonaws.com:6003",
		"gold": "http://internal-pf-glod-app-http-6000-1898069530.us-west-2.elb.amazonaws.com:6003",
	},
	"cn-hangzhou": map[string]string{
		"dev": "http://localhost:6003",
	},
	"cn-north-1": map[string]string{
		"gold": "http://internal-pf-inner-616059354.cn-north-1.elb.amazonaws.com.cn:6003",
	},
	"localhost": map[string]string{
		"dev": "http://localhost:6003",
	},
}

var (
	ErrCcpUnsupportRegin = fmt.Errorf("unsupport region to config_server")
	ErrCcpUnsupportMode  = fmt.Errorf("unsupport running mode")
)

func NewCcpManager(node_matcher, env string, logger logext.Logger) (*CcpManager, error) {
	// if mode.Mode != CcpModeCMD && mode.Mode != CcpModePrograme {
	// 	return nil, ErrCcpUnsupportMode
	// }
	if node_matcher == "" {
		node_matcher = "*"
	}
	manager := &CcpManager{
		Env:           env,
		NodeMatcher:   node_matcher,
		Logger:        logger,
		IsWriteFile:   false,
		CcpProcessors: make([]CcpProcessor, 0),
		RefreshTicker: time.NewTicker(time.Minute),
	}
	manager.AddCfgProccessor(manager)
	return manager, nil
}

func (manager *CcpManager) AddCfgProccessor(p CcpProcessor) {
	manager.CcpProcessors = append(manager.CcpProcessors, p)
}

func (manager *CcpManager) Init() error {
	manager.Logger.Infof("begin init ccp, mode %s...", manager.NodeMatcher)
	// var err error
	// manager.LocaltionInfo, err = netext.GetLocationInfoFromEve()
	// if err != nil {
	// 	manager.Logger.Errorf("get location info error %s", err.Error())
	// 	return err
	// }
	// public_ip := manager.LocaltionInfo.PublicIp
	if os.Getenv("CCP_CLIENT_IP") != "" {
		manager.ClientIp = os.Getenv("CCP_CLIENT_IP")
	}
	if os.Getenv("CCP_CFG_ADDR") != "" {
		manager.CfgServerAdd = os.Getenv("CCP_CFG_ADDR")
	}
	if os.Getenv("CCP_IOP_ROLES") != "" {
		manager.IopRoles = os.Getenv("CCP_IOP_ROLES")
	}
	if os.Getenv("CCP_IOP_PROJECT") != "" {
		manager.IopProject = os.Getenv("CCP_IOP_PROJECT")
	}
	if os.Getenv("CPP_ENV") != "" {
		manager.Env = os.Getenv("CPP_ENV")
	}
	if manager.CfgServerAdd == "" {
		err := manager.GetConfigServer3()
		if err != nil {
			manager.Logger.Errorf("get config server error %s", err.Error())
			return err
		}
	}

	if err := manager.RefreshConfig(); err != nil {
		manager.Logger.Errorf("RefreshConfig error %s,cf_server:%s ,ip: %s", err.Error(), manager.CfgServerAdd, manager.ClientIp)
		return err
	}
	go manager.RefreshConfigRountine()
	return nil

}

func GetConfigServer(region, env string) (string, error) {
	m, ok := gConfigServerMap[region]
	if !ok {
		return "", ErrCcpUnsupportRegin
	}
	u, ok := m[env]
	if !ok || len(u) == 0 {
		return "", ErrCcpUnsupportRegin
	}
	return u, nil
}

//直接从现有的列表中查找，只要找到有对应的配置就算找到了配置服
func (manager *CcpManager) GetConfigServer2() error {
	var err error
	var d map[string]string
	for _, m := range gConfigServerMap {
		for _, u := range m {
			url_str := fmt.Sprintf("%s/node_configs_by_ip?node_matcher=%s", u, manager.NodeMatcher)
			if manager.ClientIp != "" {
				url_str = fmt.Sprintf("%s/node_configs_by_ip?client_ip=%s&node_matcher=%s", u, manager.ClientIp, manager.NodeMatcher)
			}
			if d, err = GetConfig(url_str); err == nil && len(d) > 0 {
				manager.CfgServerAdd = u
				return nil
			}
		}
	}
	return err
}

var (
	g_eve_url = "https://gateway.pf.tap4fun.com:10443"
)

type EveServerConfig struct {
	ClietnIp      string `json:"client_ip"`
	IopRoles      string `json:"iop_roles"`
	IopRegionCode string `json:"iop_region_code"`
	IopEnv        string `json:"iop_env"`
	IopProject    string `json:"iop_project"`
	CfgServer     string `json:"cfg_server"`
}

//从eve获取配置服地址
func (manager *CcpManager) GetConfigServer3() error {
	if os.Getenv("EVE_URL") != "" {
		g_eve_url = os.Getenv("EVE_URL")
	}
	url_str := fmt.Sprintf("%s/server_config", g_eve_url)
	if manager.ClientIp != "" {
		url_str = fmt.Sprintf("%s/server_config?client_ip=%s", g_eve_url, manager.ClientIp)
	}
	resp, err := httpClient.Get(url_str)
	if err != nil {
		manager.Logger.Infof("call url error %s,%s", err.Error(), url_str)
		return err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		manager.Logger.Errorf("read data error %s,%s", err.Error(), url_str)
		return err
	}

	info := EveServerConfig{}
	if err := json.Unmarshal(b, &info); err != nil {
		manager.Logger.Errorf("Unmarshal data error %s,%s", err.Error(), url_str)
		return err
	}
	if info.ClietnIp != "" {
		manager.ClientIp = info.ClietnIp
	}
	if info.CfgServer == "" {
		err := fmt.Errorf("cfg server not found %s", url_str)
		manager.Logger.Errorf(err.Error())
		return err
	}
	manager.CfgServerAdd = info.CfgServer
	if manager.IopRoles == "" {
		manager.IopRoles = info.IopRoles
	}
	if manager.IopProject == "" {
		manager.IopProject = info.IopProject
	}
	return nil
}

func GetConfig(config_url string) (map[string]string, error) {
	resp, err := httpClient.Get(config_url)
	if err != nil {
		// logger.Errorf("http request error %s ", err.Error())
		return nil, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// logger.Errorf("read http body data error %s", err.Error())
		return nil, err
	}
	cfg := make(map[string]string, 0)
	if err := json.Unmarshal(b, &cfg); err != nil {
		// logger.Errorf("unmarshal json data error %s", err.Error())
		return nil, err
	}
	return cfg, nil
}

const (
	CCP          = "ccp"
	CcpConfigure = "configure"
)

const (
	CfgUsers    = "users"
	CfgFilepath = "filepath"
	CfgFileperm = "fileperm"
	CfgFormat   = "format"
)

type CCPService struct {
	Name      string
	Scope     string
	Version   string
	Configure CCPServierConfigure
	AppCfgs   map[string]map[string]string
}

// func (c *CcpConfigure) ToIni() ()
func NewGoConfig(filename string) (*goconfig.ConfigFile, error) {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return goconfig.NewConfigFile(), nil
	}
	return goconfig.ReadConfigFile(filename)
}

//把所有programe的配置都写到一个config中。如果出现key冲突，则会覆盖
//这样只考虑了booter只有一个programe:xxxx node。适用于单一服务的情况
func (c *CCPService) UpdateMemConfig(manager *CcpManager) error {
	manager.ProgrameConfigLocker.Lock()
	defer manager.ProgrameConfigLocker.Unlock()
	if manager.ProgrameConfig == nil {
		manager.ProgrameConfig = goconfig.NewConfigFile()
	}
	for section, sub_kv := range c.AppCfgs {
		for sub_key, v := range sub_kv {
			manager.ProgrameConfig.AddOption(section, sub_key, v)
		}
	}
	// return UpdateAwsCache(GetMemeConfig())
	return nil
}

func (manager *CcpManager) UpdateMemConfig(c *CCPService) error {
	manager.ProgrameConfigLocker.Lock()
	defer manager.ProgrameConfigLocker.Unlock()
	if manager.ProgrameConfig == nil {
		manager.ProgrameConfig = goconfig.NewConfigFile()
	}
	for section, sub_kv := range c.AppCfgs {
		for sub_key, v := range sub_kv {
			manager.ProgrameConfig.AddOption(section, sub_key, v)
		}
	}
	// return UpdateAwsCache(GetMemeConfig())
	return nil
}

func (manager *CcpManager) GetMemeConfig() *goconfig.ConfigFile {
	manager.ProgrameConfigLocker.RLock()
	defer manager.ProgrameConfigLocker.RUnlock()
	if manager.ProgrameConfig == nil {
		return nil
	}
	return manager.ProgrameConfig.Clone()
}

const (
	DEDAULT = "default"
	EMPTY   = ""
)

func IsDefaultScope(scope string) bool {
	return scope == DEDAULT || scope == EMPTY
}

type CCPServierConfigure struct {
	OsUsers  []*user.User
	Filepath string
	Fileperm uint32
	Format   string
	Others   map[string]string
}

func (c *CCPServierConfigure) InitUsers(users string) error {
	os_users := make([]*user.User, 0)
	for _, u := range strings.Split(users, ",") {
		if u == EMPTY {
			continue
		}

		os_user, err := user.Lookup(u)
		if err != nil {
			//logger.Errorf("invailed user: %s, error: %s", u, err.Error())
			return err
		}
		os_users = append(os_users, os_user)
	}
	if len(os_users) == 0 {
		current_user, err := user.Current()
		if err != nil {
			//logger.Errorf("get current user error %s", err.Error())
			return err
		}
		os_users = append(os_users, current_user)
	}
	c.OsUsers = os_users
	return nil
}

func (manager *CcpManager) ParseConfig(cfg map[string]string) (map[string]*CCPService, error) {
	app_services := make(map[string]*CCPService, 0)
	for k, v := range cfg {
		k = strings.ToLower(k)
		k_arr := strings.Split(k, ".")
		//key format format ccp.#{type}:#{scope}:#{version}.#{key_section}.#{key_name}
		if len(k_arr) < 4 || k_arr[0] != CCP {
			manager.Logger.Infof("not invailed ccp config key: %s ,value: %v", k, v)
			continue
		}
		//k_arr[1]=#{type}:#{scope}:#{version}. node_name
		h_arr := strings.Split(k_arr[1], ":")
		service, ok := app_services[k_arr[1]]
		if !ok {
			service = &CCPService{
				Name:    h_arr[0],
				AppCfgs: make(map[string]map[string]string, 0),
			}
			service.Configure.OsUsers = make([]*user.User, 0)
			service.Configure.Others = make(map[string]string, 0)
			if len(h_arr) >= 2 {
				service.Scope = h_arr[1]
			}
			if len(h_arr) >= 3 {
				service.Version = h_arr[2]
			}
			// manager.Logger.Infof("new ccp config for %s", k_arr[1])
			app_services[k_arr[1]] = service
		}

		// switch service.Name
		section := k_arr[2]
		sub_key := k_arr[3]
		if section == CcpConfigure {
			switch sub_key {
			case CfgUsers:
				if err := service.Configure.InitUsers(v); err != nil {
					return nil, err
				}
			case CfgFilepath:
				service.Configure.Filepath = v
			case CfgFileperm:
				_p, err := strconv.Atoi(v)
				if err != nil {
					manager.Logger.Errorf("invalied perm value %s,%s", v, err.Error())
				} else {
					service.Configure.Fileperm = uint32(_p)
				}
			case CfgFormat:
				service.Configure.Format = v
			default:
				service.Configure.Others[sub_key] = v
			}
			continue
		}
		if len(service.Configure.OsUsers) == 0 {
			if err := service.Configure.InitUsers(""); err != nil {
				return nil, err
			}
		}
		section_cfg, ok := service.AppCfgs[section]
		if !ok {
			section_cfg = make(map[string]string, 0)
		}
		section_cfg[sub_key] = v
		service.AppCfgs[section] = section_cfg
	}
	return app_services, nil
}

func (manager *CcpManager) RefreshConfig() error {
	v := url.Values{}
	v.Add("node_matcher", manager.NodeMatcher)
	if manager.ClientIp != "" {
		v.Add("client_ip", manager.ClientIp)
	}
	if manager.IopProject != "" {
		v.Add("iop_project", manager.IopProject)
	}
	if manager.IopRoles != "" {
		v.Add("iop_roles", manager.IopRoles)
	}
	if manager.Env != "" {
		v.Add("env", manager.Env)
	}
	// url_str := fmt.Sprintf("%s/node_configs_by_ip?client_ip=%s&node_matcher=%s", server_host, client_ip, manager.NodeMatcher)
	url_str := fmt.Sprintf("%s/node_configs_by_ip?%s", manager.CfgServerAdd, v.Encode())
	manager.Logger.Infof("get remote config from url %s", url_str)
	cfg, err := GetConfig(url_str)
	if err != nil {
		manager.Logger.Errorf("get config error %s,%s", err.Error(), url_str)
		return err
	}
	ccp_map, err := manager.ParseConfig(cfg)
	if err != nil {
		manager.Logger.Errorf("ParseConfig error %s", err.Error())
		return err
	}
	if len(ccp_map) == 0 {
		manager.Logger.Info("Remote config is empty")
		return nil
	}
	for _, p := range manager.CcpProcessors {
		if err := p.HandleCfg(cfg, ccp_map); err != nil {
			return err
		}
	}

	return nil
}

func (manager *CcpManager) PauseRefresh() {
	manager.PauseLocker.Lock()
	defer manager.PauseLocker.Unlock()
	manager.IsPause = true
}

func (manager *CcpManager) UnPauseRefresh() {
	manager.PauseLocker.Lock()
	defer manager.PauseLocker.Unlock()
	manager.IsPause = false
}

func (manager *CcpManager) StopRefresh() {
	manager.RefreshTicker.Stop()
}

func (manager *CcpManager) RefreshConfigRountine() {
	for {
		manager.PauseLocker.RLock()
		if !manager.IsPause {
			manager.RefreshConfig()
		}
		manager.PauseLocker.RUnlock()

		<-manager.RefreshTicker.C
	}
}
