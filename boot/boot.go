package boot

import (
	goconfig "git.tap4fun.com/platform-server/common/config"
	"git.tap4fun.com/platform-server/common/logext"
	"strings"
	"time"
)

type BootManager struct {
	Name         string
	Tag          string
	Env          string
	LocalConfig  *goconfig.ConfigFile
	Ccp          *CcpManager
	Aws          *AwsClientManager
	SetKinesis   bool
	Logger       logext.Logger
	UpdateTicker *time.Ticker
}

//programe:*:*
//*:*:*
func NewBootManager(node_match, name, tag, env string) (*BootManager, error) {
	var err error
	logger, err := logext.NewStdLogger(logext.LogLevelInfo, 0)
	if err != nil {
		return nil, err
	}
	if node_match == "" {
		node_match = "*"
	} else {
		node_match = strings.Replace(node_match, ".", "-", -1)
	}
	if name == "" {
		name = "*"
	} else {
		name = strings.Replace(name, ".", "-", -1)
	}
	if tag == "" {
		tag = "*"
	} else {
		tag = strings.Replace(tag, ".", "-", -1)
	}

	// ns = name + ":" + tag
	m := node_match + ":" + name + ":" + tag
	ccp, err := NewCcpManager(m, env, logger)
	if err != nil {
		return nil, err
	}
	return &BootManager{
		Name:         name,
		Tag:          tag,
		Env:          env,
		Aws:          NewAwsClientManager(logger),
		Ccp:          ccp,
		Logger:       logger,
		SetKinesis:   false,
		UpdateTicker: time.NewTicker(time.Minute),
	}, nil
}

func (manager *BootManager) Init() error {
	if err := manager.Ccp.Init(); err != nil {
		return err
	}
	if err := manager.ResetService(); err != nil {
		return err
	}
	go manager.UpdateRoutine()
	return nil
}

func (manager *BootManager) Bootrstap() error {
	if err := manager.SetOnce(); err != nil {
		return err
	}
	if err := manager.ResetService(); err != nil {
		return err
	}
	return nil
}

func (manager *BootManager) SetOnce() error {

	return nil
}

func (manager *BootManager) ResetService() error {
	cfg := manager.Ccp.GetMemeConfig()
	if err := manager.Aws.UpdateAwsCache(cfg); err != nil {
		return err
	}
	if err := manager.UpdateLogKinesis(cfg); err != nil {
		return err
	}
	return nil
}

func (manager *BootManager) UpdateRoutine() {
	for range manager.UpdateTicker.C {
		manager.ResetService()
	}
}

func (manager *BootManager) Shutdown() {
	manager.UpdateTicker.Stop()
}
