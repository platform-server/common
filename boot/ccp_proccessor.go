package boot

type CcpProcessor interface {
	HandleCfg(ori_cfg map[string]string, ccp_map map[string]*CCPService) error
}

func (manager *CcpManager) HandleCfg(ori_cfg map[string]string, ccp_map map[string]*CCPService) error {
	for _, ccp := range ccp_map {
		switch ccp.Name {
		case SvcAWS:
			//	Do nothing
		case SvcPrograme:
			if err := manager.UpdateMemConfig(ccp); err != nil {
				manager.Logger.Errorf("UpdateMemConfig error %s", err.Error())
				return err
			}
		}
	}

	return nil
}
