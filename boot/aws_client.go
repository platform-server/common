package boot

import (
	goconfig "git.tap4fun.com/platform-server/common/config"
	"git.tap4fun.com/platform-server/common/logext"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/aws/aws-sdk-go/service/s3"
	"os"
	"strings"
	"sync"
)

type AwsClientManager struct {
	AwsCache       map[string]*AwsClient
	AwsCacheLocker sync.RWMutex
	Logger         logext.Logger
}

func NewAwsClientManager(logger logext.Logger) *AwsClientManager {
	return &AwsClientManager{
		Logger:   logger,
		AwsCache: make(map[string]*AwsClient, 0),
	}
}

func (manager *AwsClientManager) GetAwsClient(scope string) *AwsClient {
	manager.AwsCacheLocker.RLock()
	defer manager.AwsCacheLocker.RUnlock()
	if scope == "" {
		scope = DEDAULT
	}
	client, _ := manager.AwsCache[scope]
	return client
}

func (manager *AwsClientManager) UpdateAwsCache(config *goconfig.ConfigFile) error {
	if config == nil {
		manager.Logger.Infof("config is null,need't update framework aws !")
		return nil
	}

	manager.AwsCacheLocker.Lock()
	defer manager.AwsCacheLocker.Unlock()
	for _, section := range config.GetSections() {
		arr := strings.Split(section, ":")
		//match frame-aws:#{scope}
		if !IsFrameWorkService(arr[0], "aws") {
			// manager.Logger.Infof("config is null,need't update aws !，section %s ", section)
			continue
		}
		scope := DEDAULT
		if len(arr) == 2 && arr[1] != "" {
			scope = arr[1]
		}
		access_key_id, err := config.GetString(section, "aws_access_key_id")
		if err != nil {
			manager.Logger.Errorf("get option error %s,aws_access_key_id, error %s", section, err.Error())
			return err
		}
		secret_access_key, err := config.GetString(section, "aws_secret_access_key")
		if err != nil {
			return err
		}
		region, err := config.GetString(section, "aws_region")
		if err != nil {
			return err
		}
		client, ok := manager.AwsCache[scope]
		if !ok || client.AccessKeyId != access_key_id || client.SecretAccessKey != secret_access_key {
			manager.Logger.Infof("new aws client scope %s,key_id %s,access_key %s,region %s",
				scope, access_key_id, secret_access_key, region)
			new_client, err := NewAwsClient(scope, access_key_id, secret_access_key, region)
			if err != nil {
				manager.Logger.Errorf("new aws client error %s,key_id %s", err.Error(), access_key_id)
				return err
			}
			if config.HasOption(section, "aws_kinesis_producer") && config.HasOption(section, "aws_kinesis_stream") {
				producer, _ := config.GetString(section, "aws_kinesis_producer")
				stream, _ := config.GetString(section, "aws_kinesis_stream")
				hn, _ := os.Hostname()
				producer = producer + "@" + hn
				manager.Logger.Infof("init kinesis producer: %s,stream: %s", producer, stream)
				kins := &AwsKinesis{
					Producer: producer,
					Stream:   stream,
					Kinesis:  kinesis.New(new_client.GetSession(), &aws.Config{}),
				}
				new_client.kinesis = kins
			}
			manager.Logger.Infof("add aws cache scope %s,client_id %s ", scope, new_client.AccessKeyId)
			manager.AwsCache[scope] = new_client
		}
	}
	return nil
}

type AwsClient struct {
	Scope           string
	AccessKeyId     string
	SecretAccessKey string
	Region          string
	session         *session.Session
	sessionLocker   sync.RWMutex
	kinesis         *AwsKinesis
	s3              *s3.S3
}

type AwsKinesis struct {
	Producer string
	Stream   string
	Kinesis  *kinesis.Kinesis
}

func NewAwsClient(scope, access_key_id, secret_access_key, region string) (*AwsClient, error) {

	cred := credentials.NewStaticCredentials(access_key_id, secret_access_key, "")
	new_session, err := session.NewSession(&aws.Config{Credentials: cred, Region: aws.String(region)})
	if err != nil {
		return nil, err
	}
	client := &AwsClient{
		Scope:           scope,
		AccessKeyId:     access_key_id,
		SecretAccessKey: secret_access_key,
		Region:          region,
		session:         new_session,
		s3:              s3.New(new_session, &aws.Config{}),
		// kinesis:         kinesis.New(new_session, &aws.Config{}),
	}
	return client, nil
}

func (client *AwsClient) SetKienesis(producer, stream string) (*AwsKinesis, error) {
	client.sessionLocker.Lock()
	defer client.sessionLocker.Unlock()
	if client.kinesis != nil &&
		client.kinesis.Producer == producer &&
		client.kinesis.Stream == stream {
		return client.kinesis, nil
	}
	hn, _ := os.Hostname()
	producer = producer + "@" + hn
	// manager.Logger.Infof("init kinesis producer: %s,stream: %s", producer, stream)
	k := &AwsKinesis{
		Producer: producer,
		Stream:   stream,
		Kinesis:  kinesis.New(client.session, &aws.Config{}),
	}
	client.kinesis = k
	return k, nil
}

func (client *AwsClient) GetSession() *session.Session {
	client.sessionLocker.RLock()
	defer client.sessionLocker.RUnlock()
	return client.session
}

func (client *AwsClient) GetS3() *s3.S3 {
	client.sessionLocker.RLock()
	defer client.sessionLocker.RUnlock()
	return client.s3
}

func (client *AwsClient) GetKinesis() *AwsKinesis {
	client.sessionLocker.RLock()
	defer client.sessionLocker.RUnlock()
	return client.kinesis
}
