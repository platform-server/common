package kinesis

import (
	"fmt"
	"os"
	"time"
	"encoding/json"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kinesis"
)

const (
		DefaultResendPeriod = 1
		DefaultResendTimes  = 3
		DefaultRecordCount  = 20
		DefaultMaxKinesisBuffer = 500000
		DefaultSendPeriod = 30

		DefaultChanBuffer = 10000
	)

type Data struct{
	Data            string `json:"data"`
	PartitionKey    string `json:"partitionKey"`
	ExplicitHashKey string `json:"explicitHashKey"`
	Length          int    `json:"-"`
}

type Kinesis struct {
	client       *kinesis.Kinesis

	Stream          string
	Region          string
	AccessKey       string
	SecretKey       string

	SendPeriod       int
	ResendPeriod     int
	ResendTimes      int
	RecordCount      int
	MaxKinesisBuffer int

	Boundary          string
	FailedRecordFile  string

	msgChan           chan *Data
	fileHandler          map[string]*os.File

}

func (this *Kinesis) Test() {
	fmt.Println("kinesis package test")
}

func (this *Kinesis) Init() {
	this.checkAndUpdateConfig()
	this.msgChan = make(chan *Data, DefaultChanBuffer)

	os.Setenv("AWS_ACCESS_KEY_ID", this.AccessKey)
	os.Setenv("AWS_SECRET_ACCESS_KEY", this.SecretKey)

	sen := session.New(&aws.Config{Region: aws.String(this.Region)})
	this.client = kinesis.New(sen, &aws.Config{})

	this.fileHandler = make(map[string]*os.File, 0)
	go this.startWork()
}

func (this *Kinesis) checkAndUpdateConfig(){

	if this.SendPeriod <= 0{
		this.SendPeriod = DefaultSendPeriod
	}

	if this.ResendPeriod <= 0 {
		this.ResendPeriod = DefaultResendPeriod
	}

	if this.ResendTimes <= 0 {
		this.ResendTimes = DefaultResendTimes
	}

	if this.MaxKinesisBuffer <= 0 {
		this.MaxKinesisBuffer = DefaultMaxKinesisBuffer
	}

	if this.RecordCount <= 0 {
		this.RecordCount = DefaultRecordCount
	}

}

func (this *Kinesis) AddData(data, partitionKey, explicitHashKey string){

	msg := &Data{Data:data, PartitionKey:partitionKey, ExplicitHashKey:explicitHashKey}
	this.msgChan <- msg
}

func (this *Kinesis)startTimer(ch chan bool ){

	select{
		case <- time.After(time.Second * time.Duration(this.SendPeriod)):
	}
	ch <- true
}


func (this *Kinesis) startWork(){

	recordsLength := 0
	ch := make(chan bool, 1)
	records := make([]*Data, 0)

	go this.startTimer(ch)
	for {
		select{
		case msg := <- this.msgChan:
			 data, err := json.Marshal(msg)
			 if err !=nil{
			 	continue
			 }
			 length := len(data)
			 if recordsLength + length > this.MaxKinesisBuffer && recordsLength > 0{
			 	go this.preSendDataToKinesis(records)
			 	records = make([]*Data, 0)
			 	recordsLength = 0
			 }

			 msg.Length = length
			 recordsLength += length
			 records = append(records, msg)

			 fmt.Println(msg, string(data), length)
			
		case <- ch:
			fmt.Println("time out, send data")
			if len(records) > 0 {
				go this.preSendDataToKinesis(records)
				records = make([]*Data, 0)
			 	recordsLength = 0
			}

			go this.startTimer(ch)
		}

	}
}

func (this *Kinesis) preSendDataToKinesis(msgs []*Data){
	fmt.Println("start forward message", len(msgs))

	length := len(msgs)
	count := 0
	data := ""
	records :=  make([]*kinesis.PutRecordsRequestEntry, 0)
	for i, msg := range msgs {
		if this.Boundary == ""{
			data = msg.Data
		} else {
				if data != ""{
					data = data + this.Boundary + msg.Data
				} else {
					data = msg.Data
				}
			count++
			if count < this.RecordCount && i < length - 1{
				continue
			}
		    count = 0
		}

		if msg.ExplicitHashKey != "" {
			records = append(records, 
							&kinesis.PutRecordsRequestEntry{
							Data: []byte(data),
							PartitionKey:    aws.String(msg.PartitionKey),
							ExplicitHashKey: aws.String(msg.ExplicitHashKey)})

		}else{
			records = append(records, 
							&kinesis.PutRecordsRequestEntry{
							Data: []byte(data),
							PartitionKey:  aws.String(msg.PartitionKey)})
		}

		data = ""

	}
	this.sendData(records)
}

func resetFailedRecords(records []*kinesis.PutRecordsRequestEntry, failedNum []int) []*kinesis.PutRecordsRequestEntry {
	newRecords := make([]*kinesis.PutRecordsRequestEntry, 0)
	for i := 0; i < len(failedNum); i++{
		newRecords = append(newRecords, records[failedNum[i]])
	}
	return newRecords
}


func (this *Kinesis) sendData(records []*kinesis.PutRecordsRequestEntry){
	//fmt.Println("send data", records)
	fmt.Println("length:", len(records))
	params := &kinesis.PutRecordsInput{}
	params.SetStreamName(this.Stream)
	count := 0

	for {
		params.SetRecords(records)
		resp, err := this.client.PutRecords(params)

		if resp != nil{
			if resp.FailedRecordCount != nil && *resp.FailedRecordCount != 0{
				resultRecords := resp.Records
				failedNum := make([]int, 0)
				for i := 0; i < len(resultRecords); i++ {
					if resultRecords[i].ErrorCode != nil{
						failedNum = append(failedNum, i)
					}
				}

				if *resp.FailedRecordCount == int64(len(failedNum)){
					records = resetFailedRecords(records, failedNum)

				}
			}
		}
		fmt.Println(err, resp)
		if err != nil || (resp != nil && resp.FailedRecordCount != nil && *resp.FailedRecordCount != 0){
			if count > this.ResendTimes {
				this.saveFailedData(records)
				break
			}
			select {
				case <-time.After(time.Second * time.Duration(this.ResendPeriod)):
					count++
			}
			continue
		}		

		break
	}
}

func (this *Kinesis) saveFailedData(records []*kinesis.PutRecordsRequestEntry){
	//fmt.Println("send data failed ,save", records)
	if this.FailedRecordFile == ""{
		return
	}

	ts := time.Now().Unix()
	fullName :=  this.FailedRecordFile + "_" + time.Unix(ts, 0).Format("20060102")

	file, ok := this.fileHandler[fullName]
	if !ok {
		newFile, err := os.OpenFile(fullName, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0666)
		if err != nil {
			fmt.Println(err)
			return //drop message
		} 
		this.fileHandler[fullName] =  newFile
		file = newFile
	}
	for _, record := range records{
		fmt.Println(string(record.Data))
		_, err := file.WriteString(string(record.Data) + "\n")
		if err != nil{
			fmt.Println(err)
		}
	}

	for key, file := range this.fileHandler{
		if key != fullName {
			file.Close()
			delete(this.fileHandler, key)
		}
	}

}







