#### Deamon Service

提供两种监控方式：
1. stats.go: 以udp形式，将统计信息发往特定服务器。由特定服务器对信息进行存储展示。
2. status.go: 在Service运行时，将统计信息保存在内存中，并提供接口进行访问。
