/*
* @Author: Jingkai Mao (oammix@gmail.com)
* @Date:   2015-01-19 15:59:34
 */

package deamon

import (
	"fmt"
	"io"
	"net"
	"os"
	"strings"
	"sync"
	"time"

	. "git.tap4fun.com/platform-server/common/logext/forward"
)

var (
	START = "start"
	END   = "end"
)

type Statsd interface {
	Stat(bucket string)
	StatNum(bucket string, num int)
	StatReq(service, api, gameID string)
	StatFail(service, api, gameID string)

	Stat2Start()
	Stat2End(api string, timestamp int64, state bool)

	Flush()
}

type apiStatsd struct {
	name       string
	allRequest int64
	failed     int64
	total      int64
}

type serviceRequest struct {
	total      int64
	newBuckets map[string]*apiStatsd
}

type message2 struct {
	phase    string
	apiName  string
	usedTime int64
	success  bool
}

type message struct {
	bucket string
	num    int
}

type tStatsd struct {
	service      string
	host         string
	conn         io.Writer
	interv       time.Duration
	buckets      map[string]int
	requestState *serviceRequest
	stats        chan message
	stats2       chan message2
	done         chan bool
	mutex        sync.Mutex
}

// NewStatsd returns a tStatsd structure which will write
// statsd-protocal messages to the given address in every *ticker* milliseconds.
func NewUDPStats(service, addr string, tout, interv time.Duration) (Statsd, error) {

	conn, err := net.DialTimeout("udp", addr, tout)
	if err != nil {
		return nil, err
	}

	return NewStats(service, conn, interv)
}

func NewStats(service string, out io.Writer, interv time.Duration) (Statsd, error) {

	host, err := os.Hostname()
	if err != nil {
		host = "unknownHost"
	}
	host = strings.Replace(host, ".", "-", -1)

	statsd := &tStatsd{
		service:      service,
		host:         host,
		conn:         out,
		interv:       interv,
		buckets:      make(map[string]int),
		requestState: &serviceRequest{total: 0, newBuckets: make(map[string]*apiStatsd)},
		stats:        make(chan message, 16),
		stats2:       make(chan message2, 16),
		done:         make(chan bool),
		mutex:        sync.Mutex{},
	}

	go statsd.serve()
	return statsd, nil
}

func (self *tStatsd) Stat(bucket string) {
	self.stats <- message{
		bucket: bucket,
		num:    1,
	}
}

func (self *tStatsd) StatNum(bucket string, num int) {
	self.stats <- message{
		bucket: bucket,
		num:    num,
	}
}

func (self *tStatsd) StatReq(service, api, gameID string) {
	nameall := fmt.Sprintf("%s.%s", service, api)
	namehost := fmt.Sprintf("%s.%s.%s", service, api, self.host)

	self.stats <- message{bucket: nameall + ".req.all", num: 1}
	self.stats <- message{bucket: namehost + ".req.all", num: 1}
	if gameID != "" {
		self.stats <- message{bucket: nameall + ".req." + gameID, num: 1}
		self.stats <- message{bucket: namehost + ".req." + gameID, num: 1}
	}
}

func (self *tStatsd) StatFail(service, api, gameID string) {

	nameall := fmt.Sprintf("%s.%s", service, api)
	namehost := fmt.Sprintf("%s.%s.%s", service, api, self.host)

	//self.stats <- message{bucket: buckeName(service, api, self.host) + "fail.all", num: 1}
	self.stats <- message{bucket: nameall + ".fail.all", num: 1}
	self.stats <- message{bucket: namehost + ".fail.all", num: 1}
	if gameID != "" {
		self.stats <- message{bucket: nameall + ".fail." + gameID, num: 1}
		self.stats <- message{bucket: namehost + ".fail." + gameID, num: 1}
	}
}

func (self *tStatsd) Stat2Start() {
	self.stats2 <- message2{phase: START}
}

func (self *tStatsd) Stat2End(api string, startAt int64, state bool) {
	endAt := time.Now().UnixNano()
	elapsed := (endAt - startAt) / int64(time.Millisecond)

	self.stats2 <- message2{phase: END, apiName: api, usedTime: elapsed, success: state}
}

/*
func buckeName(service, api, host string) string {
	return fmt.Sprintf("PF.%s.%s.%s", service, api, host)
}
*/

func (self *tStatsd) Flush() {

	for i := 0; i < len(self.stats); i++ {
		msg := <-self.stats
		self.mutex.Lock()
		self.buckets[msg.bucket] += msg.num
		self.mutex.Unlock()
	}

	self.output()
}

// serve handles all the modification of buckets, and guarantees
// the thread-safe. it also sends all the statstics in a named frequency.
func (self *tStatsd) serve() {

	ticker := time.NewTicker(self.interv)
	for {
		select {
		case msg := <-self.stats: // receive stats
			self.mutex.Lock()
			self.buckets[msg.bucket] += msg.num
			self.mutex.Unlock()
		case msg2 := <-self.stats2:
			if msg2.phase == START {
				self.requestState.total++
			} else if msg2.phase == END {
				if _, found := self.requestState.newBuckets[msg2.apiName]; !found {
					self.requestState.newBuckets[msg2.apiName] = &apiStatsd{allRequest: 0, total: 0, failed: 0}
				}
				self.requestState.newBuckets[msg2.apiName].name = msg2.apiName
				self.requestState.newBuckets[msg2.apiName].allRequest++
				self.requestState.newBuckets[msg2.apiName].total += msg2.usedTime

				if msg2.success == false {
					self.requestState.newBuckets[msg2.apiName].failed++
				}
			}
		case <-ticker.C: // every interv
			self.output()
		}
	}
}

func (self *tStatsd) output() {
	self.mutex.Lock()
	dataBuffers := make([][]byte, 0)
	for bucket, count := range self.buckets {
		if count > 0 {
			buf := format(bucket, count)
			dataBuffers = append(dataBuffers, buf)
			self.buckets[bucket] = 0
		}
	}

	for apiName, bucket2 := range self.requestState.newBuckets {
		buffers := getBuffers(self.service, self.host, bucket2)
		for _, bufer := range buffers {
			//GetGlobalLogger().Noticef("[Statsd test]: %s", bufer)
			dataBuffers = append(dataBuffers, []byte(bufer))
		}
		self.requestState.newBuckets[apiName].allRequest = 0
		self.requestState.newBuckets[apiName].failed = 0
		self.requestState.newBuckets[apiName].total = 0
	}
	self.mutex.Unlock()
	go func() {
		for _, buf := range dataBuffers {
			n, err := self.conn.Write(buf)
			if err != nil {
				GetGlobalLogger().Noticef("[Statsd] Error: %s\n", err)
			} else if n != len(buf) {
				GetGlobalLogger().Noticef("[Statsd] Error: short send, %d < %d.\n", n, len(buf))
			}
			time.Sleep(time.Millisecond * 20)
		}
	}()
}

func getBuffers(service, host string, bucket *apiStatsd) []string {
	strs := make([]string, 0)
	var str string
	if bucket.allRequest > 0 {
		str = format1(service, bucket.name, host, "request", bucket.allRequest)
		strs = append(strs, str)
		str = format1(service, bucket.name, "all", "request", bucket.allRequest)
		strs = append(strs, str)
	}
	if bucket.failed > 0 {
		str = format1(service, bucket.name, host, "failed", bucket.failed)
		strs = append(strs, str)
		str = format1(service, bucket.name, "all", "failed", bucket.failed)
		strs = append(strs, str)
	}

	var averageTime int64 = 0
	if bucket.allRequest != 0 {
		averageTime = bucket.total / bucket.allRequest
	}
	if averageTime > 0 {
		str = format2(service, bucket.name, host, "averageTime", averageTime)
		strs = append(strs, str)
		str = format2(service, bucket.name, "all", "averageTime", averageTime)
		strs = append(strs, str)
	}
	return strs
}

func format1(service, api, host, state string, num int64) string {
	return fmt.Sprintf("PF3.%s.%s.%s.%s:%d|c\n", service, api, host, state, num)
}

func format2(service, api, host, state string, num int64) string {
	return fmt.Sprintf("PF3.%s.%s.%s.%s:%d|ms\n", service, api, host, state, num)
}

func format(bucket string, count int) []byte {
	return []byte(fmt.Sprintf("PF3.%s:%d|c\n", bucket, count))
}

// ->
type noStatsd struct{}

func (self noStatsd) Stat(string)                     {}
func (self noStatsd) StatNum(string, int)             {}
func (self noStatsd) Flush()                          {}
func (self noStatsd) StatReq(string, string, string)  {}
func (self noStatsd) StatFail(string, string, string) {}
func (self noStatsd) Stat2Start()                     {}
func (self noStatsd) Stat2End(string, int64, bool)    {}

// NoopStatsd returns a struct that satisfies the Statsd interface
// but silently ignores all Statsd method invocations.
func NoopStats() Statsd {
	return noStatsd{}
}
