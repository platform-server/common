package deamon

import (
	//"io"
	//"net"
	//"os"
	"encoding/json"
	//"fmt"
	"strings"
	"sync"
	"time"

	"git.tap4fun.com/platform-server/common/logext/forward"
)

const (
	PUT    = "Put"
	Get    = "Get"
	Post   = "Post"
	Delete = "Delete"
)

type InterfaceStatReq interface {
	StatApiCall(gameId, apiName string, status bool, inputLen, outputLen int)
}

type statReqMessage struct {
	GameId    string
	ApiName   string
	Status    string
	InputLen  int
	OutputLen int
}

type ApiRequestRecord struct {
	GameId            string `json:"game_id"`
	Service           string `json:"service"`
	Interface         string `json:"interface"`
	Method            string `json:"method"`
	Status            bool   `json:"status"`
	Count             int    `json:"count"`
	Input             int    `json:"input_len"`
	Output            int    `json:"output_len"`
	RecordCreatedTime string `json:"record_created_at"`
}

type RequstInfo struct {
	Count     int
	InputLen  int
	OutputLen int
}

type StatReq struct {
	statReqChan    chan statReqMessage
	mutex          sync.Mutex
	apiCalledState map[string]RequstInfo
	interval       time.Duration
	service        string
}

func NewStatReq(service string, interval time.Duration) (InterfaceStatReq, error) {

	statReq := &StatReq{statReqChan: make(chan statReqMessage, 32),
		apiCalledState: make(map[string]RequstInfo),
		mutex:          sync.Mutex{},
		interval:       interval,
		service:        service}

	go statReq.server()
	return statReq, nil
}

func SplitApiName(apiName string) []string {

	slice := make([]string, 0)
	length := len(apiName)
	if length > len(PUT) && apiName[length-len(PUT):length] == PUT {
		slice = append(slice, apiName[0:length-len(PUT)])
		slice = append(slice, PUT)
		return slice
	} else if length > len(Get) && apiName[length-len(Get):length] == "Get" {
		slice = append(slice, apiName[0:length-len(Get)])
		slice = append(slice, Get)
		return slice
	} else if length > len(Post) && apiName[length-len(Post):length] == "Post" {
		slice = append(slice, apiName[0:length-len(Post)])
		slice = append(slice, Post)
		return slice
	} else if length > len(Delete) && apiName[length-len(Delete):length] == "Delete" {
		slice = append(slice, apiName[0:length-len(Delete)])
		slice = append(slice, Delete)
		return slice
	}
	slice = append(slice, apiName)
	slice = append(slice, "UnknownMethod")
	return slice
}

func (self *StatReq) server() {
	ticker := time.NewTicker(self.interval)
	for {
		select {
		case msg := <-self.statReqChan:
			self.mutex.Lock()
			api := SplitApiName(msg.ApiName)
			key := msg.GameId + "." + self.service + "." + api[0] + "." + api[1] + "." + msg.Status

			state, ok := self.apiCalledState[key]
			if ok {
				state.Count++
				state.InputLen += msg.InputLen
				state.OutputLen += msg.OutputLen
			} else {
				state = RequstInfo{Count: 1, InputLen: msg.InputLen, OutputLen: msg.OutputLen}
			}

			self.apiCalledState[key] = state
			self.mutex.Unlock()
		case <-ticker.C:
			self.output()
		}
	}

}
func (self *StatReq) StatApiCall(gameId, apiName string, status bool, inputLen, outputLen int) {

	statusStr := "success"
	if status == false {
		statusStr = "failed"
	}

	self.statReqChan <- statReqMessage{GameId: gameId, ApiName: apiName, Status: statusStr, InputLen: inputLen, OutputLen: outputLen}

}

func (self *StatReq) output() {
	recordTime := time.Now().Format("2006-01-02 15:04:05")
	self.mutex.Lock()
	for key, value := range self.apiCalledState {
		if value.Count == 0 {
			continue
		}
		slice := strings.Split(key, ".")

		state := true
		if slice[4] == "failed" {
			state = false
		}

		record := &ApiRequestRecord{
			GameId:            slice[0],
			Service:           slice[1],
			Interface:         slice[2],
			Method:            slice[3],
			Status:            state,
			Count:             value.Count,
			Input:             value.InputLen,
			Output:            value.OutputLen,
			RecordCreatedTime: recordTime}

		data, _ := json.Marshal(record)
		//fmt.Println(string(data))
		forward.GetGlobalLogger().Dataf("[DataName]:%s,[Data]:%s", "pf_api_called_count", string(data))
		value.Count = 0
		value.InputLen = 0
		value.OutputLen = 0
		self.apiCalledState[key] = value
	}
	self.mutex.Unlock()
}

type noStatReq struct {
}

func (self *noStatReq) StatApiCall(gameId, apiName string, status bool, inputLen, outputLen int) {}

func NoStatReq() InterfaceStatReq {
	return &noStatReq{}
}
