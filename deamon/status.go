package deamon

import (
	"encoding/json"
	"time"
)

// --------------------------
// server info
// --------------------------
type tServerInfo struct {
	Name        string `json:"name"`
	Version     string `json:"version"`
	CodeVersion string `json:"code_version"`
	Started     string `json:"started_at"`
	Uptime      int64  `json:"uptime"`

	// hidden filed, used to calculate uptime
	startedUnix int64
}

// --------------------------
// server status
// --------------------------
type tApiStatus struct {
	Name     string `json:"name"`
	Requests int64  `json:"requests"`
	Failed   int64  `json:"failed"`
	Rt       int64  `json:"rt"`
	Total    int64  `json:"total"`
}

type tServerStatus struct {
	Requests    int64         `json:"requests"`
	Concurrency int64         `json:"concurrency"`
	Failed      int64         `json:"failed"`
	ApisArray   []*tApiStatus `json:"api"`
	// hidden field, used to output ApisArray
	apis map[string]*tApiStatus
}

type tStepOutArgs struct {
	api     string
	elapsed int64
}

// --------------------------
// platform statistic
// --------------------------
type tPlatformStatus struct {
	status  *tServerStatus
	info    *tServerInfo
	failed  chan string
	stepin  chan bool
	stepout chan tStepOutArgs
}

type PlatformStatus interface {
	ApiStepIn() int64
	ApiStepOut(string, int64)
	ApiFailOne(string)

	GetStatusString(bool) string
	GetInfoString() string
}

//
func NewPlatformStatus(name, version, hash string) (PlatformStatus, error) {

	status := &tPlatformStatus{
		status: &tServerStatus{apis: map[string]*tApiStatus{}},
		info: &tServerInfo{
			Name:        name,
			Version:     version,
			CodeVersion: hash,
			Started:     time.Now().Format(time.RFC1123),
			startedUnix: time.Now().Unix(),
		},
		failed:  make(chan string, 32),
		stepin:  make(chan bool, 32),
		stepout: make(chan tStepOutArgs, 32)}
	go status.Routine()

	return status, nil
}

func (self *tPlatformStatus) Routine() {

	for {
		select {
		case <-self.stepin:
			self.status.Concurrency += 1
			self.status.Requests += 1

		case out := <-self.stepout:
			if _, found := self.status.apis[out.api]; !found {
				self.status.apis[out.api] = &tApiStatus{}
			}

			self.status.apis[out.api].Total += out.elapsed
			self.status.apis[out.api].Requests += 1
			self.status.Concurrency -= 1

		case failed := <-self.failed:
			if _, found := self.status.apis[failed]; !found {
				self.status.apis[failed] = &tApiStatus{}
			}

			self.status.apis[failed].Failed += 1
			self.status.Failed += 1
		}
	}
}

//
func (self *tPlatformStatus) ApiStepIn() int64 {

	startAt := time.Now().UnixNano()
	self.stepin <- true
	return startAt
}

func (self *tPlatformStatus) ApiStepOut(api string, startAt int64) {

	endAt := time.Now().UnixNano()
	elapsed := (endAt - startAt) / int64(time.Millisecond)
	self.stepout <- tStepOutArgs{api, elapsed}
}

func (self *tPlatformStatus) ApiFailOne(api string) {
	self.failed <- api
}

func (self *tPlatformStatus) GetStatusString(reboot bool) string {
	// no need to be 100% accurate, just for simple statistic
	status := &tServerStatus{
		Requests:    self.status.Requests,
		Concurrency: self.status.Concurrency,
		Failed:      self.status.Failed,
		ApisArray:   make([]*tApiStatus, 0, len(self.status.apis)),
	}

	// preparing
	for name, api := range self.status.apis {
		apiStatus := &tApiStatus{
			Name:     name,
			Requests: api.Requests,
			Failed:   api.Failed,
			Total:    api.Total,
		}
		if api.Requests > 0 {
			apiStatus.Rt = api.Total / api.Requests
		}

		if reboot {
			api.Total = 0
			api.Requests = 0
			api.Failed = 0
		}
		status.ApisArray = append(status.ApisArray, apiStatus)
	}

	bytes, _ := json.Marshal(status)
	return string(bytes)
}

func (self *tPlatformStatus) GetInfoString() string {

	self.info.Uptime = time.Now().Unix() - self.info.startedUnix

	bytes, _ := json.Marshal(self.info)
	return string(bytes)
}

// ->
type noPlatformStatus struct{}

func (self noPlatformStatus) ApiStepIn() int64            { return 0 }
func (self noPlatformStatus) ApiStepOut(string, int64)    {}
func (self noPlatformStatus) ApiFailOne(string)           {}
func (self noPlatformStatus) GetStatusString(bool) string { return "" }
func (self noPlatformStatus) GetInfoString() string       { return "" }

// Noop returns a struct that satisfies the PlatformStatus interface
// but silently ignores all PlatformStatus method invocations.
func NoopPlatformStatus() PlatformStatus {
	return noPlatformStatus{}
}
