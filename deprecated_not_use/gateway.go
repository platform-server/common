package common

import (
	"fmt"
	"git.tap4fun.com/platform-server/othird/goconf"
	"git.tap4fun.com/platform-server/othird/gorm"
	"git.tap4fun.com/platform-server/common/config"
)

func NewRemoteMapper(conf *goconfig.RemoteConfig) *gorm.DB {
	ip, err := conf.GetString("Database", "host")
	if err != nil {
		ELog("get %v parameter err:%v", "host", err)
		return nil
	}
	if ip == "" {
		ELog("host doesn't set")
		return nil
	}

	port, err := conf.GetString("Database", "port")
	if err != nil {
		ELog("get %v parameter err:%v", "port", err)
		return nil
	}
	if port == "" {
		ELog("port doesn't set")
		return nil
	}

	user, err := conf.GetString("Database", "user")
	if err != nil {
		ELog("get %v parameter err:%v", "user", err)
		return nil
	}
	if user == "" {
		ELog("user doesn't set")
		return nil
	}

	password, err := conf.GetString("Database", "password")
	if err != nil {
		ELog("get %v parameter err:%v", "password", err)
		return nil
	}
	if password == "" {
		ELog("password doesn't set")
		return nil
	}

	dbname, err := conf.GetString("Database", "database")
	if err != nil {
		ELog("get %v parameter err:%v", "database", err)
		return nil
	}
	if dbname == "" {
		ELog("database doesn't set")
		return nil
	}

	url := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?%s", user, password, ip, port, dbname, "charset=utf8&parseTime=True")
	NLog("connecting to:%v", url)

	mydb, err := gorm.Open("mysql", url)
	if err != nil {
		ELog("NewDataMapper err:%v", err)
		return nil
	}
	mydb.LogMode(true)
	return &mydb
}

func NewMapper(conf *conf.ConfigFile) *gorm.DB {
	ip, err := conf.GetString("Database", "host")
	if err != nil {
		ELog("get %v parameter err:%v", "host", err)
		return nil
	}
	if ip == "" {
		ELog("host doesn't set")
		return nil
	}

	port, err := conf.GetString("Database", "port")
	if err != nil {
		ELog("get %v parameter err:%v", "port", err)
		return nil
	}
	if port == "" {
		ELog("port doesn't set")
		return nil
	}

	user, err := conf.GetString("Database", "user")
	if err != nil {
		ELog("get %v parameter err:%v", "user", err)
		return nil
	}
	if user == "" {
		ELog("user doesn't set")
		return nil
	}

	password, err := conf.GetString("Database", "password")
	if err != nil {
		ELog("get %v parameter err:%v", "password", err)
		return nil
	}
	if password == "" {
		ELog("password doesn't set")
		return nil
	}

	dbname, err := conf.GetString("Database", "database")
	if err != nil {
		ELog("get %v parameter err:%v", "database", err)
		return nil
	}
	if dbname == "" {
		ELog("database doesn't set")
		return nil
	}

	url := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?%s", user, password, ip, port, dbname, "charset=utf8&parseTime=True")
	NLog("connecting to:%v", url)

	mydb, err := gorm.Open("mysql", url)
	if err != nil {
		ELog("NewDataMapper err:%v", err)
		return nil
	}
	mydb.LogMode(true)
	return &mydb

}
