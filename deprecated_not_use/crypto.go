package common

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha1"
	"encoding/hex"
	"sync"
)

var s_hmacKey = []byte("pt@*a!cd)~")

var s_hmac = hmac.New(sha1.New, s_hmacKey)
var s_sha1 = sha1.New()
var s_md5 = md5.New()
var s_cryptoLock = &sync.Mutex{}

func SetHmacKey(key []byte) {
	s_cryptoLock.Lock()
	defer s_cryptoLock.Unlock()

	s_hmacKey = key
	s_hmac = hmac.New(sha1.New, key)
}

func GetHmac(value string) string {
	return hex.EncodeToString(getHmacSlice([]byte(value)))
}

func HmacString(key, value string) string {
	m := hmac.New(sha1.New, []byte(key))
	m.Write([]byte(value))
	return hex.EncodeToString(m.Sum(nil))
}

func VerifyHmacString(key, message, messageMac string) bool {
	mMac, err := hex.DecodeString(messageMac)
	if err != nil {
		return false
	}

	m := hmac.New(sha1.New, []byte(key))
	m.Write([]byte(message))

	return hmac.Equal(m.Sum(nil), mMac)
}

func VerifyHmac(message string, messageMac string) bool {
	mMac, err := hex.DecodeString(messageMac)
	if err != nil {
		return false
	}
	return hmac.Equal(getHmacSlice([]byte(message)), mMac)
}

func getHmacSlice(value []byte) []byte {
	s_cryptoLock.Lock()
	defer s_cryptoLock.Unlock()

	s_hmac.Reset()
	s_hmac.Write(value)
	return s_hmac.Sum(nil)
}

func Sha1Bytes(value string) []byte {
	s_cryptoLock.Lock()
	defer s_cryptoLock.Unlock()

	s_sha1.Reset()
	s_sha1.Write([]byte(value))
	return s_sha1.Sum(nil)
}

func Sha1(value string) string {
	return hex.EncodeToString(Sha1Bytes(value))
}

func Md5Bytes(value string) []byte {
	s_cryptoLock.Lock()
	defer s_cryptoLock.Unlock()

	s_md5.Reset()
	s_md5.Write([]byte(value))
	return s_md5.Sum(nil)
}

func Md5(value string) string {
	return hex.EncodeToString(Md5Bytes(value))
}
