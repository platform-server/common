package common

import (
	"crypto/tls"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

const (
	USERTYPE_ANONYMOUS = "anonymous"
)

// look up gameId from clientId. current format of clientId: <game id>:<version>
func LookupGameId(clientId string) string {

	parts := strings.SplitN(clientId, ":", 2)
	if len(parts) != 2 {
		return ""
	}
	return parts[0]
}

// look up game version from clientId. current format of clientId: <game id>:<version>
func LookupGameVersion(clientId string) string {

	parts := strings.SplitN(clientId, ":", 2)
	if len(parts) != 2 {
		return ""
	}
	return parts[1]
}

func ParseUser(user string) (usertype, username string) {
	tmp := strings.SplitN(user, ":", 2)
	if len(tmp) != 2 {
		return
	}
	usertype, username = tmp[0], tmp[1]
	return
}

func CombineUser(usertype, username string) (user string) {
	return fmt.Sprintf("%s:%s", usertype, username)
}

func IsUserAnonymous(user string) bool {
	usertype, _ := ParseUser(user)
	return usertype == USERTYPE_ANONYMOUS
}

func ByteContain(token byte, src []byte) bool {
	for i := 0; i < len(src); i++ {
		if token == src[i] {
			return true
		}
	}
	return false
}

func BytesContain(tokens, src []byte) bool {
	for i := 0; i < len(tokens); i++ {
		for j := 0; j < len(src); j++ {
			if tokens[i] == src[j] {
				return true
			}
		}
	}
	return false
}

func FindFirstByte(line string, startIndex int, endIndex int,
	bytes []byte, isExcept bool) (index int) {
	index = -1
	for i := startIndex; i < len(line) && i < endIndex; i++ {
		if (isExcept && !ByteContain(line[i], bytes)) ||
			(!isExcept && ByteContain(line[i], bytes)) {
			index = i
			break
		}
	}
	return
}

func UnionBytesSlice(a, b []byte) []byte {
	u := make([]byte, len(a), len(a)+len(b))
	copy(u, a)
	u = append(u, b...)
	return u
}

/* vim: set ts=4 sw=4 sts=4 tw=100 et: */

// added by dong ai hua
func GetUrl(url1 string) (resp *http.Response, err error) {
	u, err := url.Parse(url1)
	if err != nil {
		panic(err)
	}
	if u.Scheme == "https" {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client := &http.Client{Transport: tr}
		return client.Get(url1)
	} else {
		return http.Get(url1)
	}

}

func PostForm(url1 string, data url.Values) (resp *http.Response, err error) {
	u, err := url.Parse(url1)
	if err != nil {
		panic(err)
	}
	if u.Scheme == "https" {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client := &http.Client{Transport: tr}
		return client.PostForm(url1, data)
	} else {
		return http.PostForm(url1, data)
	}
}

func NotifyResult(url1 string, resp interface{}) (*http.Response, error) {
	if url1 == "" {
		return nil, errors.New("url is null")
	}

	rspBytes, err := json.Marshal(resp)
	if err != nil {
		return nil, errors.New("Encode response to json error")
	}

	u, err := url.Parse(url1)
	if err != nil {
		panic(err)
	}
	if u.Scheme == "https" {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client := &http.Client{Transport: tr}
		return client.Post(url1, "application/json", strings.NewReader(string(rspBytes)))
	} else {
		return http.Post(url1, "application/json", strings.NewReader(string(rspBytes)))
	}

}

func ToInt(v interface{}) (int, error) {
	switch v := v.(type) {
	case int:
		return v, nil
	case string:
		return strconv.Atoi(v)
	default:
		return 0, errors.New("Covert to int error")
	}

}

func ToString(v interface{}) (string, error) {
	switch v := v.(type) {
	case int:
		return strconv.Itoa(v), nil
	case string:
		return v, nil
	default:
		return "error", errors.New("Covert to string error")
	}
}

func ParseCommandLine(boolFlags []string, stringFlags []string) (map[string]string, error) {
	m := make(map[string]string)
	boolValues := make([]bool, len(boolFlags)+1)
	stringValues := make([]string, len(stringFlags)+1)

	for i := 0; i < len(boolFlags); i++ {
		key := boolFlags[i]
		flag.BoolVar(&boolValues[i], key, false, "")
	}
	for i := 0; i < len(stringFlags); i++ {
		key := stringFlags[i]
		flag.StringVar(&stringValues[i], key, "", "")
	}
	flag.Parse()

	for i := 0; i < len(boolFlags); i++ {
		key := boolFlags[i]
		if boolValues[i] {
			m[key] = "true"
		} else {
			m[key] = "false"
		}
	}

	for i := 0; i < len(stringFlags); i++ {
		key := stringFlags[i]
		m[key] = stringValues[i]
	}
	return m, nil
}
