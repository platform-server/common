/***************************************************************************
 *
 * Copyright (c) 2013 Nibirutech.com, Inc. All Rights Reserved
 * $Id$
 *
 **************************************************************************/

/**
 * @file util_test.go
 * @author dengzhuo(dengzhuo@nibirutech.com)
 * @date 2013/09/24 14:18:42
 * @version $Revision$
 * @brief
 *
 **/

package common

import (
	//"fmt"
	"testing"
)

func TestReadLines(t *testing.T) {
	var err error

	_, err = ReadFileToBytes("error")
	if nil == err {
		t.Errorf("read file fail")
	}

	_, err = ReadFileToBytes("../../../../testData/testFile.txt")
	if nil != err {
		t.Errorf("read file succ")
	}

	fileString, err := ReadFileToString("error")
	if nil == err {
		t.Errorf("read file fail")
	}

	fileString, err = ReadFileToString("../../../../testData/testFile.txt")
	if nil != err {
		t.Errorf("read file succ")
	}

	_, err = ReadLinesFromString(fileString)
	if nil != err {
		t.Errorf("read lines succ")
	}

	_, err = ReadLines("error")
	if nil == err {
		t.Errorf("read file fail")
	}

	_, err = ReadLines("../../../../testData/testFile.txt")
	if nil != err {
		t.Errorf("read file succ")
	}

}

func TestLinesDealing(t *testing.T) {
	exceptBytes := []byte{'a', 'c', 'd', '#'}
	containBytes := []byte{'q', 'w', 'e', 'r'}

	isContain := ByteContain('a', containBytes)
	if isContain {
		t.Errorf("contains a")
	}

	isContain = ByteContain('a', exceptBytes)
	if !isContain {
		t.Errorf("not contains a")
	}

	testBytes1 := []byte{'a', 'b'}
	testBytes2 := []byte{'q', 'w'}
	testBytes3 := []byte{'a', 'c'}

	isContain = BytesContain(testBytes1, exceptBytes)
	if !isContain {
		t.Errorf("not contains a")
	}

	isContain = BytesContain(testBytes2, exceptBytes)
	if isContain {
		t.Errorf("not contains a")
	}

	isContain = BytesContain(testBytes3, exceptBytes)
	if !isContain {
		t.Errorf("contains a")
	}

	unionBytes := UnionBytesSlice(exceptBytes, containBytes)
	if len(unionBytes) != len(exceptBytes)+len(containBytes) {
		t.Errorf("union error")
	}

	line := "this # & a"
	index := FindFirstByte(line, 0, len(line), exceptBytes, true)
	if index != 0 {
		t.Errorf("find fail")
	}

	index = FindFirstByte(line, 0, len(line), exceptBytes, false)
	if index != 5 {
		t.Errorf("find fail")
	}

	index = FindFirstByte(line, len(line), len(line), exceptBytes, false)
	if index != -1 {
		t.Errorf("find fail")
	}

	index = FindFirstByte(line, len(line), 0, exceptBytes, false)
	if index != -1 {
		t.Errorf("find fail")
	}

}

func TestStruct(t *testing.T) {
	addr1 := Addr{CONNTCP, "123", 1}
	addr2 := Addr{CONNTCP, "123", 2}
	addr3 := Addr{CONNTCP, "123", 1}

	if addr1.Equal(&addr2) {
		t.FailNow()
	}

	if !addr1.Equal(&addr3) {
		t.FailNow()
	}

	var addrs1 Addrs
	var addrs2 Addrs
	addrs1.PutAddr(addr1.Type, addr1.Ip, addr1.Port)
	//fmt.Println(addrs1)

	addrs1.PutAddr(addr2.Type, addr2.Ip, addr2.Port)

	addrs2.PutAddr(addr1.Type, addr1.Ip, addr1.Port)

	if addrs1.Equal(addrs2) {
		t.FailNow()
	}

	addrs2.Clear()
	addrs2.PutAddr(addr1.Type, addr1.Ip, addr1.Port)
	addrs2.PutAddr(addr2.Type, addr2.Ip, addr2.Port)

	if !addrs1.Equal(addrs2) {
		t.FailNow()
	}
}

/* vim: set ts=4 sw=4 sts=4 tw=100 et: */
