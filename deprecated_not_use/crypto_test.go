package common

import (
	"testing"
)

func TestCrypto_Hmac(t *testing.T) {
	h := GetHmac("test")
	if !VerifyHmac("test", h) {
		t.Fatal("verifyhmac should success")
	}
}

func TestCrypto_Sha1(t *testing.T) {
	h := Sha1("test")
	if h == "" {
		t.Fatal("sha1 result should not empty")
	}
}
