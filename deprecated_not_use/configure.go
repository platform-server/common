/***************************************************************************
 *
 * Copyright (c) 2013 Nibirutech.com, Inc. All Rights Reserved
 * $Id$
 *
 **************************************************************************/

/**
 * @file configure.go
 * @author dengzhuo(dengzhuo@nibirutech.com)
 * @date 2013/09/22 10:48:28
 * @version $Revision$
 * @brief
 *
 **/

package common

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type configSlice []*Configure
type configMap map[string]*Configure
type configValue string

const (
	CONFIG_NONE = iota
	CONFIG_SLICE
	CONFIG_MAP
	CONFIG_VALUE
	CONFIG_ALL
)

const (
	CONFIG_SLICE_BEGIN = 0
	CONFIG_SLICE_END   = -1
	CONFIG_SLICE_ALL   = -2
)

const (
	CONFIG_SLICE_CAP = 5
)

const (
	CONFIG_PARSE_TAG = iota
	CONFIG_PARSE_ASSIGN
	CONFIG_PARSE_VALUE

	CONFIG_PARSE_LEFT_SECTION
	CONFIG_PARSE_RIGHT_SECTION
	CONFIG_PARSE_LEVEL
	CONFIG_PARSE_ARRAY
	CONFIG_PARSE_SECTION_NAME

	CONFIG_PARSE_OVER
)

const (
	ERROR_OPEN_FILE = 1
	ERROR_READ_FILE = 2

	ERROR_CONFIG_TYPE          = 7
	ERROR_CONFIG_VALUE_EXIST   = 8
	ERROR_CONFIG_CONVERT       = 11
	ERROR_CONFIG_PARSE_VALUE   = 12
	ERROR_CONFIG_PARSE_SECTION = 13
	ERROR_CONFIG_HAS_EXIST     = 14
)

type Configure struct {
	value     interface{}
	valueType int

	noteTokens    []byte
	sectionTokens []byte
	assignTokens  []byte
	ignoreTokens  []byte
}

//note chars
var defaultNoteTokens = []byte{
	'#',
}

//section chars
var defaultSectionTokens = []byte{
	'[',
	']',
	'.',
	'@',
}

//assign chars
var defaultAssignTokens = []byte{
	':',
	'=',
}

//ignore chars
var defaultIgornTokens = []byte{
	' ',
	'\t',
}

//new a configure
func NewConfigure() *Configure {
	config := Configure{
		valueType:     CONFIG_NONE,
		noteTokens:    defaultNoteTokens,
		sectionTokens: defaultSectionTokens,
		assignTokens:  defaultAssignTokens,
		ignoreTokens:  defaultIgornTokens,
	}
	config.valueType = CONFIG_MAP
	config.value = make(configMap)
	return &config
}

//load configure text
func (self *Configure) Load(cfgPath string) (err error) {
	var lines []string
	lines, err = ReadLines(cfgPath)
	if err != nil {
		return
	}
	self.clear()
	_, err = self.parse(lines, 0, 0)
	return
}

func (self *Configure) AddItemConfig(key string, value *Configure,
	tag int, isForce bool) (err error) {
	if self.valueType != CONFIG_MAP {
		err = Error{ERROR_CONFIG_TYPE, "can't add item to type not map"}
		return
	}

	cmap := self.value.(configMap)
	if (value.valueType == CONFIG_MAP && tag == CONFIG_MAP) ||
		value.valueType == CONFIG_VALUE {
		if isForce {
			cmap[key] = value
		} else if _, isExist := cmap[key]; isExist {
			err = Error{ERROR_CONFIG_VALUE_EXIST, "this key has exist, key : " + key}
			return
		} else {
			cmap[key] = value
		}
	} else if value.valueType == CONFIG_MAP && tag == CONFIG_SLICE {
		sconfig, isExist := cmap[key]
		if isExist && !isForce && sconfig.valueType != CONFIG_SLICE {
			err = Error{ERROR_CONFIG_VALUE_EXIST, "this key has exist, key : " + key}
			return
		} else if isExist && sconfig.valueType != CONFIG_SLICE {
			delete(cmap, key)
			isExist = false
		}

		if !isExist {
			sconfig = self.newSliceConfig()
			cmap[key] = sconfig
		}
		cslice := sconfig.value.(configSlice)
		sconfig.value = append(cslice, value)

	} else {
		err = Error{ERROR_CONFIG_TYPE, "can't recognise this type, type : " + strconv.Itoa(value.valueType)}
	}
	return
}

//check weather item exists
func (self *Configure) Exist(key string, configType int) (bool, int, error) {
	if self.valueType != CONFIG_MAP {
		err := Error{ERROR_CONFIG_TYPE, "can't exist item from type not map"}
		return false, CONFIG_NONE, err
	}
	var tmpType int
	cmap := self.value.(configMap)
	value, isExist := cmap[key]
	if isExist {
		tmpType = value.valueType
		if CONFIG_ALL == configType {
		} else if CONFIG_NONE == configType {
			isExist = false
		} else if CONFIG_MAP == configType && CONFIG_MAP != value.valueType {
			isExist = false
		} else if CONFIG_SLICE == configType &&
			CONFIG_SLICE != value.valueType {
			isExist = false
		} else if CONFIG_VALUE == configType &&
			CONFIG_VALUE != value.valueType {
			isExist = false
		} else {
			isExist = false
		}
	}
	return isExist, tmpType, nil
}

func (self *Configure) AddString(key, value string, isForce bool) (err error) {
	if self.valueType != CONFIG_MAP {
		err = Error{ERROR_CONFIG_TYPE, "can't add item to type not map"}
		return
	}
	vConfig := self.newValueConfig(value)

	return self.AddItemConfig(key, vConfig, CONFIG_VALUE, isForce)
}

func ReadLines(path string) ([]string, error) {
	var myErr Error
	file, err := os.Open(path)
	if err != nil {
		myErr = Error{ERROR_OPEN_FILE, fmt.Sprintf("open file fail: %s", path)}
		return nil, myErr
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	if scanner.Err() != nil {
		myErr := Error{ERROR_READ_FILE, scanner.Err().Error()}
		return nil, myErr
	}
	return lines, nil
}

//get item of configure
func (self *Configure) G(key string) (config *Configure) {
	if self.valueType != CONFIG_MAP {
		return nil
	}
	cmap := self.value.(configMap)

	config, _ = cmap[key]

	return
}

//get value as uint16 of item
func (self *Configure) AsUint16() (value uint16, err error) {
	if self.valueType != CONFIG_VALUE {
		err = Error{ERROR_CONFIG_TYPE, "can't get value from type not value"}
		return
	}
	vconfig := self.value.(configValue)
	value64, err := strconv.ParseUint(string(vconfig), 10, 16)
	if err != nil {
		err = Error{ERROR_CONFIG_CONVERT, "convert to uint16 fail"}
		return
	}
	value = uint16(value64)
	return
}

//get value as string of item
func (self *Configure) AsString() (value string, err error) {
	if self.valueType != CONFIG_VALUE {
		err = Error{ERROR_CONFIG_TYPE, "can't get value from type not value"}
		return
	}
	vconfig := self.value.(configValue)
	value = string(vconfig)
	return
}

//get value as int of item
func (self *Configure) AsInt() (value int, err error) {
	if self.valueType != CONFIG_VALUE {
		err = Error{ERROR_CONFIG_TYPE, "can't get value from type not value"}
		return
	}
	vconfig := self.value.(configValue)
	value64, err := strconv.ParseInt(string(vconfig), 10, 0)
	if err != nil {
		err = Error{ERROR_CONFIG_CONVERT, "convert to int fail"}
		return
	}
	value = int(value64)
	return
}

//get value as int64 of item
func (self *Configure) AsInt64() (value int64, err error) {
	if self.valueType != CONFIG_VALUE {
		err = Error{ERROR_CONFIG_TYPE, "can't get value from type not value"}
		return
	}
	vconfig := self.value.(configValue)
	value, err = strconv.ParseInt(string(vconfig), 10, 64)
	if err != nil {
		err = Error{ERROR_CONFIG_CONVERT, "convert to int64 fail"}
		return
	}
	return
}

//clear configure
func (self *Configure) clear() {
	self.valueType = CONFIG_MAP
	self.value = make(configMap)
}

//parse all of configure
func (self *Configure) parse(lines []string,
	startLine, level int) (endLine int, err error) {
	if self.valueType != CONFIG_MAP {
		err = Error{ERROR_CONFIG_TYPE, "can't parse from type not map"}
		return
	}
	endLine = startLine
	var isOver bool = false
	for i := startLine; i < len(lines) && nil == err; {
		index := FindFirstByte(lines[i], 0, len(lines[i]),
			self.ignoreTokens, true)
		if -1 == index {
			i++
			endLine = i
			continue
		} else if ByteContain(lines[i][index], self.noteTokens) {
			i++
			endLine = i
			continue
		} else if lines[i][index] == self.sectionTokens[0] {
			var subEndLine int
			isOver, subEndLine, err = self.parseSection(lines, i, index, level)
			if err != nil {
				break
			}
			i = subEndLine
			endLine = i
			if isOver {
				break
			}
		} else {
			err = self.parseField(lines[i], index)
			if err != nil {
				break
			}
			i++
			endLine = i
		}
	}
	if nil != err {
		self.clear()
	}
	return
}

//parse section to configure
func (self *Configure) parseSection(lines []string, lineIndex int,
	startIndex int, level int) (isOver bool, endIndex int, err error) {
	line := lines[lineIndex]

	endIndex = lineIndex
	err = nil

	var isArray bool
	var sectionName string = ""
	sectionBeginIndex := -1
	sectionEndIndex := -1
	state := CONFIG_PARSE_LEFT_SECTION
	state = CONFIG_PARSE_LEVEL

	levelNum := 0
	levelBeginIndex := FindFirstByte(line, startIndex+1, len(line),
		self.ignoreTokens, true)
	if -1 == levelBeginIndex {
		err = Error{ERROR_CONFIG_PARSE_SECTION, fmt.Sprintf("parse section fail, line : %d level : %d", lineIndex, level)}
		return
	}
loop:
	for i := levelBeginIndex; i < len(line) && err == nil; {
		switch state {
		case CONFIG_PARSE_LEVEL:
			if line[i] == self.sectionTokens[2] {
				levelNum++
				if levelNum > level {
					err = Error{ERROR_CONFIG_PARSE_SECTION, fmt.Sprintf("level num is error, this level : %d excepted level : %d", levelNum, level)}
					return
				}
				i++
			} else {
				if levelNum < level {
					isOver = true
					return
				}
				state = CONFIG_PARSE_ARRAY
				i = FindFirstByte(line, i, len(line),
					self.ignoreTokens, true)
				if -1 == i {
					err = Error{ERROR_CONFIG_PARSE_SECTION, fmt.Sprintf("parse section fail, state : %d line : %d index : %d level : %d", state, lineIndex, i, level)}
					return
				}
			}
		case CONFIG_PARSE_ARRAY:
			if line[i] == self.sectionTokens[3] {
				isArray = true
				i = FindFirstByte(line, i+1, len(line),
					self.ignoreTokens, true)
				if -1 == i {
					err = Error{ERROR_CONFIG_PARSE_SECTION, fmt.Sprintf("parse section fail, state : %d line : %d index : %d level : %d", state, lineIndex, i, level)}
					return
				}
			}
			sectionBeginIndex = i
			sectionEndIndex = sectionBeginIndex
			i++
			state = CONFIG_PARSE_SECTION_NAME
		case CONFIG_PARSE_SECTION_NAME:
			isContain := ByteContain(line[i], self.ignoreTokens)
			if isContain || line[i] == self.sectionTokens[1] {
				sectionName = line[sectionBeginIndex : sectionEndIndex+1]

				state = CONFIG_PARSE_RIGHT_SECTION
				i = FindFirstByte(line, i, len(line),
					self.ignoreTokens, true)
				if -1 == i {
					err = Error{ERROR_CONFIG_PARSE_SECTION, fmt.Sprintf("parse section fail, state : %d line : %d index : %d level : %d", state, lineIndex, i, level)}
					return
				}
			} else {
				sectionEndIndex = i
				i++
			}
		case CONFIG_PARSE_RIGHT_SECTION:
			if line[i] == self.sectionTokens[1] {
				state = CONFIG_PARSE_OVER
				break loop
			} else {
				err = Error{ERROR_CONFIG_PARSE_SECTION, fmt.Sprintf("parse section fail, state : %d line : %d index : %d level : %d", state, lineIndex, i, level)}
				return
			}
		}
	}
	if CONFIG_PARSE_OVER != state {
		err = Error{ERROR_CONFIG_PARSE_SECTION, fmt.Sprintf("parse section fail, line : %d state : %d level : %d", lineIndex, state, level)}
		return
	}
	if err == nil {
		cmap := self.value.(configMap)
		vconfig, isExist := cmap[sectionName]
		if (isExist && !isArray) ||
			(isExist && isArray && vconfig.valueType != CONFIG_SLICE) {
			err = Error{ERROR_CONFIG_HAS_EXIST, "this key has exist, key : " + sectionName}
			return
		}

		var tmpEndIndex int
		mconfig := self.newMapConfig()
		tmpEndIndex, err = mconfig.parse(lines, lineIndex+1, level+1)
		if err != nil {
			return
		}

		if !isArray {
			self.AddItemConfig(sectionName, mconfig, CONFIG_MAP, false)
		} else {
			self.AddItemConfig(sectionName, mconfig, CONFIG_SLICE, false)
		}
		endIndex = tmpEndIndex
	}
	return
}

//parse field to configure
func (self *Configure) parseField(line string, startIndex int) (err error) {
	state := CONFIG_PARSE_TAG
	tag := ""
	tagBeginIndex := startIndex
	tagEndIndex := startIndex
	value := ""
	valueBeginIndex := -1
	valueEndIndex := -1
loop:
	for i := startIndex + 1; i < len(line) && nil == err; {
		switch state {
		case CONFIG_PARSE_TAG:
			if ByteContain(line[i], self.assignTokens) ||
				ByteContain(line[i], self.ignoreTokens) {
				tag = line[tagBeginIndex : tagEndIndex+1]
				state = CONFIG_PARSE_ASSIGN
				isExist, _, _ := self.Exist(tag, CONFIG_ALL)
				if isExist {
					err = Error{ERROR_CONFIG_HAS_EXIST, "this key has exist, key : " + tag}
					return
				}
			} else {
				tagEndIndex = i
				i++
			}
		case CONFIG_PARSE_ASSIGN:
			assignIndex := FindFirstByte(line, i, len(line),
				self.ignoreTokens, true)
			if -1 == assignIndex {
				err = Error{ERROR_CONFIG_PARSE_VALUE, fmt.Sprintf("parse section fail, can't find assign token, index : %d", i)}
				return
			} else if !ByteContain(line[assignIndex], self.assignTokens) {
				err = Error{ERROR_CONFIG_PARSE_VALUE, fmt.Sprintf("parse section fail, can't find assign token, index : %d", i)}
				return
			} else {
				state = CONFIG_PARSE_VALUE
				i = assignIndex + 1

				valueBeginIndex = FindFirstByte(line, i, len(line),
					self.ignoreTokens, true)
				if -1 == valueBeginIndex {
					break loop
				} else {
					i = valueBeginIndex + 1
					valueEndIndex = valueBeginIndex
				}
			}
		case CONFIG_PARSE_VALUE:
			if ByteContain(line[i], self.ignoreTokens) {
				value = line[valueBeginIndex : valueEndIndex+1]
				state = CONFIG_PARSE_OVER
				break loop
			} else {
				valueEndIndex = i
				i++
			}
		}
	}
	if CONFIG_PARSE_TAG == state || CONFIG_PARSE_ASSIGN == state {
		err = Error{ERROR_CONFIG_PARSE_VALUE, "can't parse this tag:value"}
	} else if CONFIG_PARSE_VALUE == state {
		state = CONFIG_PARSE_OVER
		if 0 <= valueBeginIndex && valueEndIndex >= valueBeginIndex {
			value = line[valueBeginIndex : valueEndIndex+1]
		} else {
			value = ""
		}
	}
	if err == nil {
		err = self.AddString(tag, value, false)
	}
	return
}

//new a type of map configure
func (self *Configure) newMapConfig() *Configure {
	mconfig := NewConfigure()
	mconfig.valueType = CONFIG_MAP
	mconfig.value = make(configMap)
	self.copyTokens(mconfig, false)
	return mconfig
}

//new a type of slice configure
func (self *Configure) newSliceConfig() *Configure {
	sconfig := NewConfigure()
	sconfig.valueType = CONFIG_SLICE
	sconfig.value = make(configSlice, 0, CONFIG_SLICE_CAP)
	self.copyTokens(sconfig, false)
	return sconfig
}

//get copy of tokens
func (self *Configure) copyTokens(that *Configure, isClone bool) {
	if isClone {
		that.noteTokens = make([]byte, len(self.noteTokens))
		copy(that.noteTokens, self.noteTokens)
		that.assignTokens = make([]byte, len(self.assignTokens))
		copy(that.assignTokens, self.assignTokens)
		that.ignoreTokens = make([]byte, len(self.ignoreTokens))
		copy(that.ignoreTokens, self.ignoreTokens)
		that.sectionTokens = make([]byte, len(self.sectionTokens))
		copy(that.sectionTokens, self.sectionTokens)
	} else {
		that.noteTokens = self.noteTokens
		that.assignTokens = self.assignTokens
		that.ignoreTokens = self.ignoreTokens
		that.sectionTokens = self.sectionTokens
	}
}

//private init configure
func (self *Configure) newValueConfig(value string) *Configure {
	vconfig := NewConfigure()
	vconfig.valueType = CONFIG_VALUE
	vconfig.value = configValue(value)
	self.copyTokens(vconfig, false)
	return vconfig
}
