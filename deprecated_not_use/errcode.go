package common

import (
	"fmt"
)

type ErrorCode int

// standard error of platform
type Error struct {
	ErrCode ErrorCode `json:"error_code"`
	ErrMsg  string    `json:"error_msg"`
}

// Error returns a formated string in json.
func (self Error) Error() string {
	return self.Marshal()
}

func (self Error) Marshal() string {
	return fmt.Sprintf("{ \"error_code\" : %d, \"error_msg\" : \"%s\" }", self.ErrCode, self.ErrMsg)
}
