package common

import (
    "strings"
    "testing"
)

func TestSet(t *testing.T) {
    s := NewSet()
    if s.Find("a") {
        t.Fatal("should not find a")
    }
    if s.Size() != 0 {
        t.Fatal("size should equal 0")
    }

    s.Insert("a")
    if !s.Find("a") {
        t.Fatal("should find a")
    }

    if s.Size() != 1 {
        t.Fatal("size should equal 1")
    }

    s.Remove("b")
    if !s.Find("a") {
        t.Fatal("should find a")
    }

    if s.Size() != 1 {
        t.Fatal("size should equal 1")
    }

    s.Remove("a")
    if s.Find("a") {
        t.Fatal("should not find a")
    }

    if s.Size() != 0 {
        t.Fatal("size should equal 0")
    }
}

func TestSetSlice(t *testing.T) {
    s := NewSetWithStringSlice([]string{"a", "b", "c"})
    if s == nil {
        t.Fatal("should not equal nil")
    }
    if s.Size() != 3 {
        t.Fatal("size should equal 3")
    }
    if !s.Find("a") {
        t.Fatal("should find a")
    }
    if !s.Find("b") {
        t.Fatal("should find b")
    }
    if !s.Find("c") {
        t.Fatal("should find c")
    }

    a := "auth_game_admin"
    b := "auth auth_game_admin"
    r := strings.Split(a, " ")
    s1 := NewSetWithStringSlice(strings.Split(b, " "))
    for _, ra := range r {
        if !s1.Find(ra) {
            t.Fatalf("shoud find %s", ra)
        }
    }
}
