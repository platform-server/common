package protocol

import (
	"encoding/json"
)

type DatacenterConf struct {
	Status  string            `json:"status"`
	Env     string            `json:"env"`
	Servers map[string]string `json:"servers"`
}

/*type Datacenter struct {
    Code string `json:"code"`
    Conf DatacenterConf `json:"config"`
}*/

type EveConfRsp struct {
	M map[string]*DatacenterConf
	//Datacenters []Datacenter `json:"datacenters"`
	//MsgType int
}

func (self *EveConfRsp) Marshal() []byte {
	//self.MsgType = EVE_EVECONF
	bytes, _ := json.Marshal(self.M)
	return bytes
}
