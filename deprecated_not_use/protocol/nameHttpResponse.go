package protocol

import (
	"encoding/json"
)

type LocateRsp struct {
	Host      string `json:"host"`
	HttpsPort uint16 `json:"https_port"`
	//Addr string `json:"addr"`
	//MsgType int
}

func (self *LocateRsp) Marshal() []byte {
	//self.MsgType = NAME_LOCATE
	bytes, _ := json.Marshal(self)
	return bytes
}

type ServersRsp struct {
	Addrs []string `json:"addrs"`
	//MsgType int
}

func (self *ServersRsp) Marshal() []byte {
	//self.MsgType = NAME_SERVERS
	bytes, _ := json.Marshal(self)
	return bytes
}

type BatchLocateRsp struct {
	M map[string]*LocateRsp `json:"servers"`
}

func (self *BatchLocateRsp) Marshal() []byte {
	//self.MsgType = NAME_LOCATE
	bytes, _ := json.Marshal(self)
	return bytes
}
