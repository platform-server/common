package protocol

import (
	"encoding/json"
)

type HttpVerifySnsUserRsp struct {
	Success bool `json:"success"`
	//MsgType int
}

func (self HttpVerifySnsUserRsp) Marshal() []byte {
	//self.MsgType = VERIFY_SNS_USER
	bytes, _ := json.Marshal(self)
	return bytes
}

type HttpAuthorizeRsp struct {
	AccessToken string `json:"access_token"`
	//MsgType     int
}

func (self HttpAuthorizeRsp) Marshal() []byte {
	//self.MsgType = AUTHORIZE
	bytes, _ := json.Marshal(self)
	return bytes
}

type SetAppUserRsp struct {
	Success bool `json:"success"`
	//MsgType int
}

func (self *SetAppUserRsp) Marshal() []byte {
	//self.MsgType = AUTH_SET_APP_USER
	bytes, _ := json.Marshal(self)
	return bytes
}

type GetAppUserRsp struct {
	GameId    string `json:"game_id"`
	UserType  string `json:"user_type"`
	AppKey    string `json:"app_key"`
	AppSecret string `json:"app_secret"`
	//Env       string `json:"env"`
	//MsgType   int
}

func (self *GetAppUserRsp) Marshal() []byte {
	//self.MsgType = AUTH_GET_APP_USER
	bytes, _ := json.Marshal(self)
	return bytes
}

// gateway
type CreateCharRsp struct {
	CharacterId    uint64 `json:"character_id"`
	CharacterCount int    `json:"character_count"`
}

func (self *CreateCharRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type LoginCharRsp struct {
	Success bool `json:"success"`
}

func (self *LoginCharRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type DelCharRsp struct {
	Success bool `json:"success"`
}

func (self *DelCharRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type EnableCharRsp struct {
	Success bool `json:"success"`
}

func (self *EnableCharRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type ForbidCharRsp struct {
	Success bool `json:"success"`
}

func (self *ForbidCharRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type AllowCharRsp struct {
	Success bool `json:"success"`
}

func (self *AllowCharRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type Character struct {
	CharacterId uint64 `json:"character_id"`
	ServerId    string `json:"server_id"`
	LastLogin   string `json:"last_login"`
	MetaInfo    string `json:"meta_info"`
	Forbidden   bool   `json:"forbidden"`
}

type GetCharsRsp struct {
	Characters []*Character `json:"characters"`
}

func (self *GetCharsRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type LinkUserRsp struct {
	Success bool `json:"success"`
}

func (self *LinkUserRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type GetAccountInfo struct {
	Users []string `json:"users"`
}

func (self *GetAccountInfo) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type ResetUser struct {
	Success bool `json:"success"`
}

func (self *ResetUser) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type TransferCharacter struct {
	Success bool `json:"success"`
}

func (self *TransferCharacter) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type GameServer struct {
	ServerId  string `json:"server_id"`
	Name      string `json:"name"`
	Addr      string `json:"addr"`
	Language  string `json:"language"`
	ExtraData string `json:"extra_data"`
}

type GetGameServersRsp struct {
	Servers []*GameServer `json:"servers"`
}

func (self *GetGameServersRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type CheckCharacterRsp struct {
	Success bool `json:"success"`
}

func (self *CheckCharacterRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}
