package protocol

import (
	"encoding/json"
)

type NameRsp struct {
	Name string `json:"name"`
	//MsgType int
}

func (self *NameRsp) Marshal() []byte {
	//self.MsgType = DINGXIANG_NAME
	bytes, _ := json.Marshal(self)
	return bytes
}
