package protocol

import (
	"encoding/json"
)

type IosPayRsp struct {
	CheckCount uint64 `json:"count"`
	Quantity   uint64 `json:"quantity"`
	ProductId  string `json:"product_id"`
}

func (self *IosPayRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type GplayPayRsp struct {
	CheckCount uint64 `json:"count"`
}

func (self *GplayPayRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

// for alipay order verification
type AlipayPayRsp struct {
	CheckCount uint64  `json:"count"`
	TotalFee   float32 `json:"total_fee"`
}

func (self *AlipayPayRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

// for signing alipay order
type AlipaySignRsp struct {
	SignedData string `json:"signed_data"`
}

func (self *AlipaySignRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}
