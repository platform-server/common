package protocol

import (
	"encoding/json"
)

type GetStoreUrlRsp struct {
	Url string `json:"url"`
	//MsgType int
}

func (self *GetStoreUrlRsp) Marshal() []byte {
	//self.MsgType = ADS_STORE_URL
	bytes, _ := json.Marshal(self)
	return bytes
}

type SetStoreUrlRsp struct {
	Success bool `json:"success"`
	//MsgType int
}

func (self *SetStoreUrlRsp) Marshal() []byte {
	//self.MsgType = ADS_STORE_SET_URL
	bytes, _ := json.Marshal(self)
	return bytes
}
