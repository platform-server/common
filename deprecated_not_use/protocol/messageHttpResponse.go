package protocol

import (
	"encoding/json"
)

type RegEndpointRsp struct {
	Success bool `json:"success"`
}

func (self *RegEndpointRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type UnregEndpointRsp struct {
	Success bool `json:"success"`
}

func (self *UnregEndpointRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type SendMsgRsp struct {
	MsgId   int64 `json:"message_id,omitempty"`
	Success bool  `json:"success"`
}

func (self *SendMsgRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type BroadcastMsgRsp struct {
	Success bool `json:"success"`
}

func (self *BroadcastMsgRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type UpdateApnsKeyRsp struct {
	Success bool `json:"success"`
}

func (self *UpdateApnsKeyRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type UpdateGcmKeyRsp struct {
	Success bool `json:"success"`
}

func (self *UpdateGcmKeyRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

type GetArrivalRateRsp struct {
	Rate string `json:"arrival_rate"`
}

func (self *GetArrivalRateRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}
