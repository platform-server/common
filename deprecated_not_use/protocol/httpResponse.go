/***************************************************************************
 *
 * Copyright (c) 2013 Nibirutech.com, Inc. All Rights Reserved
 * $Id$
 *
 **************************************************************************/

/**
 * @file httpResponse.go
 * @author dengzhuo(dengzhuo@nibirutech.com)
 * @date 2013/10/21 13:21:17
 * @version $Revision$
 * @brief
 *
 **/

package protocol

import (
	"encoding/json"
	. "git.tap4fun.com/platform-server/common/protocol"
)

/*
const (
    ERROR = 1
    //LOCATION = 2

    // auth
    VERIFY_SNS_USER   = 10
    AUTHORIZE         = 11
    AUTH_SET_APP_USER = 12
    AUTH_GET_APP_USER = 13

    GATEWAY_CREATE_CHARACTER = 20
    CreateCharRsp

    // pay
    VERIFY_PAY = 30

    // ads
    ADS_STORE_URL     = 40
    ADS_STORE_SET_URL = 41

    // name
    NAME_LOCATE  = 50
    NAME_SERVERS = 51

    // eve
    EVE_EVECONF = 60

    // dingxiang
    DINGXIANG_NAME = 70
)*/

type HttpVerifyPayRsp struct {
	//MsgType int
	Count uint
}

func (self HttpVerifyPayRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

func MarshalHttpVerifyPayRsp(count uint) []byte {
	return HttpVerifyPayRsp{ /*MsgType: VERIFY_PAY, */ Count: count}.Marshal()
}

/*type HttpLocRsp struct {
    MsgType int
    Locs    common.Addrs
    Names   []string
}

func (self HttpLocRsp) Marshal() []byte {
    bytes, _ := json.Marshal(self)
    return bytes
}

func MarshalHttpLocRsp(locs common.Addrs, names []string) []byte {
    return HttpLocRsp{MsgType: LOCATION, Locs: locs, Names: names}.Marshal()
}*/

/*type LocationResponse struct {
    ErrCode     int
    ErrMsg      string
    Type        int
    Addrs       []Addr
}*/

// http error response
type HttpErrorRsp struct {
	ErrorCode Errcode `json:"error_code"`
	ErrorMsg  string  `json:"error_msg"`
	//MsgType   int
}

// serialize to get byte stream
func (self HttpErrorRsp) Marshal() []byte {
	bytes, _ := json.Marshal(self)
	return bytes
}

// generate HttpErrorRsp and serialize it to get byte stream
func MarshalHttpErrorRsp(errCode Errcode, msg string) []byte {
	return HttpErrorRsp{errCode, msg /*, ERROR*/}.Marshal()
}

type PtRsp interface {
	Marshal() []byte
}

/* vim: set ts=4 sw=4 sts=4 tw=100 et: */
