/**
 *	liudan@nibirutech.com
 *	2014.09.03
 */

package protocol

import (
	_ "fmt"
)

type SimpleRsp struct {
	Success bool `json:"success"`
}

type AccountsCreateRsp struct {
	ChatAccount string `json:"chat_account"`
	Password    string `json:"password"`
}
