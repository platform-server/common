package protocol

import (
	"fmt"
	"net/http"
)

// 检查在http请求参数中是否包含了必须的参数，否则返回false
func CheckHttpParams(r *http.Request, params ...string) bool {
	for _, param := range params {
		if r.FormValue(param) == "" {
			return false
		}
	}
	return true
}

// 检查在http的Post请求参数中是否包含了必须的参数，否则返回false
func CheckHttpPostParams(r *http.Request, params ...string) bool {
	for _, param := range params {
		if r.PostFormValue(param) == "" {
			fmt.Printf("%s doesn't exist", param)
			return false
		}
	}
	return true
}
