package common

//
type Set struct {
	m map[interface{}]bool
}

func NewSet() *Set {
	return &Set{make(map[interface{}]bool)}
}

func NewSetWithStringSlice(slice []string) *Set {
	s := NewSet()
	s.InitWithStringSlice(slice)
	return s
}

func (self *Set) InitWithStringSlice(slice []string) {
	for _, i := range slice {
		self.Insert(i)
	}
}

func (self *Set) Insert(i interface{}) {
	self.m[i] = true
}

func (self *Set) Remove(i interface{}) {
	delete(self.m, i)
}

func (self *Set) Find(i interface{}) bool {
	_, present := self.m[i]
	return present
}

func (self *Set) Size() int {
	return len(self.m)
}
