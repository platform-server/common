package common

import (
	"git.tap4fun.com/platform-server/common/logext/forward"
)

// WARNING: when u initializes logger, set the defualt stack to 1.
func DLog(format string, args ...interface{}) {
	forward.GetGlobalLogger().Debugf(format, args...)
}

func ILog(format string, args ...interface{}) {
	forward.GetGlobalLogger().Infof(format, args...)
}

func NLog(format string, args ...interface{}) {
	forward.GetGlobalLogger().Noticef(format, args...)
}

func WLog(format string, args ...interface{}) {
	forward.GetGlobalLogger().Warningf(format, args...)
}

func ELog(format string, args ...interface{}) {
	forward.GetGlobalLogger().Errorf(format, args...)
}
