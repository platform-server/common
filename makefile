currentDir=$(shell pwd)
sourceDirName=
sourceDir=$(join ${currentDir},${sourceDirName})
compiler=go
compileCmd=install
goPath=$(join $(currentDir), /../../../../)
projects=$(shell ls -d */)


all:
	@for projectName in ${projects};\
	do \
	echo "compiling "$$projectName;\
	cd ${sourceDir}/$$projectName;\
	[ -e makefile ]||GOPATH=${goPath} ${compiler} ${compileCmd};\
	[ ! -e makefile ]||make;\
	done;\
	echo "finished"
