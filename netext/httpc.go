/*
* @Author: Jingkai Mao (oammix@gmail.com)
* @Date:   2015-01-19 17:07:04
 */

package netext

import (
	"crypto/tls"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var (
	httpClient = &http.Client{
		Timeout: 2 * time.Second,
	}
)
var (
	ErrFailedNetworkRequest = errors.New("failed-network-request.")
)

type HttpClient interface {
	Get(method string, values map[string]string) (string, error)
	Delete(method string, values map[string]string) (string, error)
	Put(method string, values map[string]string) (string, error)
	Post(method string, values map[string]string) (string, error)
	SetHeader(key, valus string)
}

type tHttpClient struct {
	baseurl string
	client  *http.Client
	header  map[string]string
}

func NewHttpClient(baseurl string) HttpClient {

	secure := false
	if strings.HasPrefix(baseurl, "https") {
		secure = true
	}

	if !strings.HasPrefix(baseurl, "http") {
		secure = true
		baseurl = "https://" + baseurl
	}

	client := &http.Client{}
	if secure {
		transport := &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}
		client.Transport = transport
	}

	return &tHttpClient{
		baseurl: strings.TrimRight(baseurl, "/"),
		client:  client,
	}
}

func (self *tHttpClient) Get(method string, values map[string]string) (string, error) {

	url := ContUrl(self.baseurl, method, EncodeUrlValues(values))
	rq, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}

	return SendHttpRequest(self.client, rq, self.header)
}

func (self *tHttpClient) Delete(method string, values map[string]string) (string, error) {

	url := ContUrl(self.baseurl, method, EncodeUrlValues(values))
	rq, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return "", err
	}

	return SendHttpRequest(self.client, rq, self.header)
}

func (self *tHttpClient) Put(method string, values map[string]string) (string, error) {

	reader := strings.NewReader(EncodeUrlValues(values))
	rq, err := http.NewRequest("PUT", ContUrl(self.baseurl, method, ""), reader)
	if err != nil {
		return "", err
	}

	self.SetHeader("Content-Type", "application/x-www-form-urlencoded")

	return SendHttpRequest(
		self.client,
		rq,
		self.header)
}

func (self *tHttpClient) Post(method string, values map[string]string) (string, error) {

	reader := strings.NewReader(EncodeUrlValues(values))
	rq, err := http.NewRequest("POST", ContUrl(self.baseurl, method, ""), reader)
	if err != nil {
		return "", err
	}

	self.SetHeader("Content-Type", "application/x-www-form-urlencoded")

	return SendHttpRequest(
		self.client,
		rq,
		self.header)
}

func (self *tHttpClient) SetHeader(key, value string) {
	if self.header == nil {
		self.header = map[string]string{key: value}
	} else {
		self.header[key] = value
	}
}

func ContUrl(base, method, suffix string) string {

	tmethod := strings.Trim(method, "/")
	if suffix != "" {
		return base + "/" + tmethod + "?" + suffix
	}
	return base + "/" + tmethod
}

func EncodeUrlValues(values map[string]string) string {

	rsp := url.Values{}
	for k, v := range values {
		rsp.Set(k, v)
	}

	return rsp.Encode()
}

func SendHttpRequest(client *http.Client, request *http.Request, header map[string]string) (string, error) {

	if header != nil {
		for k, v := range header {
			request.Header.Set(k, v)
		}
	}

	response, err := client.Do(request)
	if err != nil {
		return "", err
	}

	if response.StatusCode != http.StatusOK {
		return "", ErrFailedNetworkRequest
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}
