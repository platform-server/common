/*
* @Author: Jingkai Mao (oammix@gmail.com)
* @Date:   2015-01-19 17:01:37
 */

package netext

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"runtime"
	"strings"
)

var (
	ErrFailedAddressExternalIpv4     = errors.New("failed-address-external-ipv4.")
	ErrFailedGetAddressFromDingxiang = errors.New("failed-get-address-from-dingxiang.")
)

var InnerIps = [][]net.IP{
	{net.ParseIP("10.0.0.0"), net.ParseIP("10.255.255.255")},
	{net.ParseIP("172.16.0.0"), net.ParseIP("172.31.255.255")},
	{net.ParseIP("192.168.0.0"), net.ParseIP("192.168.255.255")},
}

//判断是否内网ip
func IsInnerIp(ip_str string) bool {
	ip := net.ParseIP(ip_str)
	if ip.To4() == nil {
		// fmt.Printf("not ipv4 addr %s", ip_str)
		return false
	}
	for _, ips := range InnerIps {
		if bytes.Compare(ip, ips[0]) >= 0 && bytes.Compare(ip, ips[1]) <= 0 {
			return true
		}
	}
	return false
}

//读取网卡获取内网ip，如果网卡是空，则读默认的网卡eth0,en0
func GetPrivateIpByEth(eth string) (string, error) {
	if len(eth) == 0 {
		switch runtime.GOOS {
		case "darwin":
			eth = "en0"
		case "linux":
			eth = "eth0"
		default:
			err := fmt.Errorf("unsupport os %s", runtime.GOOS)
			// fmt.Printf(err.Error())
			return "", err
		}
	}

	i, err := net.InterfaceByName(eth)
	if err != nil {
		fmt.Printf("find net interface error %s", err.Error())
		return "", err
	}
	// fmt.Printf("get interface %s inner ip", i.Name)
	addrs, err := i.Addrs()
	if err != nil {
		// fmt.Printf("get addrs error %s", err.Error())
		return "", err
	}
	for _, addr := range addrs {
		fmt.Printf("find addr %s for %s", addr.String(), eth)
		add := strings.Split(addr.String(), "/")[0]
		if IsInnerIp(add) {
			return add, nil
		}
	}
	err = fmt.Errorf("no inner ip for %s", eth)
	// fmt.Println(err.Error())
	return "", err
}

var eveUrlMap = map[string]string{
	"en": "https://gateway.pf.tap4fun.com:10443",
	"cn": "https://gateway-bj.pf.t4f.cn:10443",
}

type LocationInfo struct {
	PublicIp string `json:"client_ip"`
	Region   string `json:"user_region"`
}

var g_location_info *LocationInfo

func GetLocationInfoFromEve() (info *LocationInfo, err error) {
	if g_location_info != nil {
		return g_location_info, nil
	}
	if os.Getenv("REGION") == "cn" {
		g_region_default = "cn"
	}
	if info, err = _getLocationInfoFromEve(eveUrlMap[g_region_default]); err == nil {
		return info, err
	}
	for r, url_str := range eveUrlMap {
		if r == g_region_default {
			continue
		}
		info, err := _getLocationInfoFromEve(url_str)
		if err == nil {
			return info, nil
		}
	}
	return info, err
}

func _getLocationInfoFromEve(eve_url string) (*LocationInfo, error) {
	resp, err := httpClient.Get(fmt.Sprintf("%s/client_info", eve_url))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	info := LocationInfo{}
	if err := json.Unmarshal(b, &info); err != nil {
		return nil, err
	}
	g_location_info = &info
	return &info, nil
}

// 检查在http请求参数中是否包含了必须的参数，否则返回false
func CheckHttpParams(r *http.Request, params ...string) bool {
	for _, param := range params {
		if r.FormValue(param) == "" {
			return false
		}
	}
	return true
}

// 检查在http的Post请求参数中是否包含了必须的参数，否则返回false
func CheckHttpPostParams(r *http.Request, params ...string) bool {
	for _, param := range params {
		if r.PostFormValue(param) == "" {
			fmt.Printf("%s doesn't exist", param)
			return false
		}
	}
	return true
}

// GetExternalIpv4Address returns first available external ip, just a workaround [TODO].
func GetExternalIpv4Address() (string, error) {

	faces, err := net.Interfaces()
	if err != nil {
		return "", err
	}

	for _, iface := range faces {

		if iface.Flags&net.FlagUp == 0 {
			continue //interface down
		}

		if iface.Flags&net.FlagLoopback != 0 {
			continue // 127.0.0.1 loopback interface
		}

		addrs, err := iface.Addrs()
		if err != nil {
			return "", err
		}

		var ip net.IP
		for _, addr := range addrs {
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}

			if ip == nil || ip.IsLoopback() {
				continue
			}

			ip = ip.To4()
			if ip == nil {
				continue // not an ipv4 address
			}
			return ip.String(), nil
		}
	}

	return "", ErrFailedAddressExternalIpv4
}

//
func GetRedirectLocalUrl(localAddr string) (string, error) {

	addr := fmt.Sprintf("http://%s/name", localAddr)
	rsp, err := http.Get(addr)
	//rsp, err := http.Get("http://localhost:6000/name")
	if err != nil {
		return "", err
	}

	body, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return "", err
	}

	type tDingxiangResponse struct {
		Url string `json:"name"`
	}
	var response tDingxiangResponse
	err = json.Unmarshal(body, &response)
	if err != nil {
		return "", err
	}

	return response.Url, nil
}
