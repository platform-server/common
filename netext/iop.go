package netext

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	// "net/http"
	"net/url"
	"os"
	"sort"
	"strings"
	"sync"
)

// "https://iop-api.tap4fun.com/real_servers/unsecure_list?private_ip_addresses-in=10.0.3.59&only=location"

//https://iop-external.pf.tap4fun.com/real_servers?private_ip_addresses-in=10.0.3.59&only=location"

var (
	ErrIopNotFind             = errors.New("iop not find info")
	ErrIopNotInvaliedTag      = errors.New("iop tags must has project, roles and env")
	ErrIopNotInvaliedLocation = errors.New("iop location must be set")
)

// var gIopServerLocation *IopServerLocation

type IopServerTag struct {
	Project string   `json:"project,omitempty"`
	Name    string   `json:"name,omitempty"`
	Roles   []string `json:"roles,omitempty"`
	Env     string   `json:"env,omitempty"`
}

func (tag *IopServerTag) RoleStrings() string {
	return strings.Join(tag.Roles, ",")
}

type IopServerLocation struct {
	ReginCode string `json:"region_code"`
	Name      string `json:"name"`
	Zone      string `json:"zone"`
}
type IopProject struct {
	Name        string `json:"name"`
	DisplayName string `json:"display_name"`
	Description string `json:"description"`
}
type IopServerInfo struct {
	Tags     *IopServerTag      `json:"tags,omitempty"`
	Location *IopServerLocation `json:"location,omitempty"`
	Project  *IopProject        `json:"project,omitempty"`
}

type IopInfo struct {
	Total int             `json:"total"`
	Data  []IopServerInfo `json:"data,omitempty"`
}

var iopHostMap = map[string]string{
	"en": "https://iop-external.pf.tap4fun.com",
	"cn": "https://cn-proxy.pf.tap4fun.com:32018",
}

var g_region_default = "en"

func QueryIopInfo(ip string) (info *IopServerInfo, err error) {
	if os.Getenv("REGION") == "cn" {
		g_region_default = "cn"
	}
	if info, err = _queryIopInfo(ip, iopHostMap[g_region_default]); err == nil {
		return info, err
	}
	for r, host := range iopHostMap {
		if r == g_region_default {
			continue
		}
		info, err = _queryIopInfo(ip, host)
		if err == nil {
			g_region_default = r
			return info, nil
		}
	}
	return info, err
}

var g_iop_cache = make(map[string]*IopServerInfo, 0)
var g_iop_cache_locker sync.RWMutex

func _queryIopInfo(ip string, iop_url string) (*IopServerInfo, error) {
	values := url.Values{}
	values.Add("only", "location,tags,project")
	if IsInnerIp(ip) {
		values.Add("private_ip_addresses-in", ip)
	} else {
		values.Add("public_ip_addresses-in", ip)
	}

	urlStr := fmt.Sprintf("%s/real_servers?%s", iop_url, values.Encode())
	resp, err := httpClient.Get(urlStr)
	if err != nil {
		g_iop_cache_locker.RLock()
		defer g_iop_cache_locker.RUnlock()
		if i, ok := g_iop_cache[ip]; ok {
			return i, nil
		}
		return nil, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		g_iop_cache_locker.RLock()
		defer g_iop_cache_locker.RUnlock()
		if i, ok := g_iop_cache[ip]; ok {
			return i, nil
		}
		return nil, err
	}
	info := IopInfo{}
	if err := json.Unmarshal(b, &info); err != nil {
		g_iop_cache_locker.RLock()
		defer g_iop_cache_locker.RUnlock()
		if i, ok := g_iop_cache[ip]; ok {
			return i, nil
		}
		return nil, err
	}
	if info.Total*len(info.Data) == 0 {
		return nil, ErrIopNotFind
	}
	server_info := info.Data[0]
	if server_info.Tags == nil ||
		server_info.Project == nil ||
		len(server_info.Tags.Roles) == 0 {
		return nil, ErrIopNotInvaliedTag
	}
	sort.Strings(server_info.Tags.Roles)
	// var env string
	for _, r := range server_info.Tags.Roles {
		arr := strings.Split(r, "_")
		if len(arr) == 2 && arr[1] == "env" {
			server_info.Tags.Env = arr[0]
		}
	}
	if server_info.Tags.Env == "" {
		return nil, ErrIopNotInvaliedTag
	}
	if server_info.Location == nil {
		return nil, ErrIopNotInvaliedLocation
	}
	g_iop_cache_locker.Lock()
	defer g_iop_cache_locker.Unlock()
	g_iop_cache[ip] = &server_info
	return &server_info, nil
}
