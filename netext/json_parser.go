package netext

import (
	"encoding/json"
)

type JsonParser struct {
	value interface{}
}

func NewJsonParser(data []byte) (*JsonParser, error) {
	var value interface{}
	err := json.Unmarshal(data, &value)
	if err != nil {
		return nil, err
	}
	return &JsonParser{value}, nil
}

func (self *JsonParser) G(key string) *JsonParser {
	m, ok := self.value.(map[string]interface{})
	if !ok {
		return nil
	}

	var value interface{}
	value, ok = m[key]
	if !ok {
		return nil
	}

	return &JsonParser{value}
}

func (self *JsonParser) asNumber() (float64, bool) {
	v, ok := self.value.(float64)
	if !ok {
		return 0.0, false
	}
	return v, true
}

func (self *JsonParser) AsInt8() (int8, bool) {
	v, ok := self.asNumber()
	if !ok {
		return 0, false
	}
	return int8(v), true
}

func (self *JsonParser) AsInt16() (int16, bool) {
	v, ok := self.asNumber()
	if !ok {
		return 0, false
	}
	return int16(v), true
}

func (self *JsonParser) AsInt32() (int32, bool) {
	v, ok := self.asNumber()
	if !ok {
		return 0, false
	}
	return int32(v), true
}

func (self *JsonParser) AsInt64() (int64, bool) {
	v, ok := self.asNumber()
	if !ok {
		return 0, false
	}
	return int64(v), true
}

func (self *JsonParser) AsUint8() (uint8, bool) {
	v, ok := self.asNumber()
	if !ok {
		return 0, false
	}
	return uint8(v), true
}

func (self *JsonParser) AsUint16() (uint16, bool) {
	v, ok := self.asNumber()
	if !ok {
		return 0, false
	}
	return uint16(v), true
}

func (self *JsonParser) AsUint32() (uint32, bool) {
	v, ok := self.asNumber()
	if !ok {
		return 0, false
	}
	return uint32(v), true
}

func (self *JsonParser) AsUint64() (uint64, bool) {
	v, ok := self.asNumber()
	if !ok {
		return 0, false
	}
	return uint64(v), true
}

func (self *JsonParser) AsFloat32() (float32, bool) {
	v, ok := self.asNumber()
	if !ok {
		return float32(0.0), false
	}
	return float32(v), true
}

func (self *JsonParser) AsFloat64() (float64, bool) {
	return self.asNumber()
}

func (self *JsonParser) AsString() (string, bool) {
	v, ok := self.value.(string)
	return v, ok
}

func (self *JsonParser) AsBool() (bool, bool) {
	v, ok := self.value.(bool)
	return v, ok
}

func (self *JsonParser) GInt8(key string) (int8, bool) {
	j := self.G(key)
	if j == nil {
		return 0, false
	}
	return j.AsInt8()
}

func (self *JsonParser) GInt16(key string) (int16, bool) {
	j := self.G(key)
	if j == nil {
		return 0, false
	}
	return j.AsInt16()
}

func (self *JsonParser) GInt32(key string) (int32, bool) {
	j := self.G(key)
	if j == nil {
		return 0, false
	}
	return j.AsInt32()
}

func (self *JsonParser) GInt64(key string) (int64, bool) {
	j := self.G(key)
	if j == nil {
		return 0, false
	}
	return j.AsInt64()
}

func (self *JsonParser) GUint8(key string) (uint8, bool) {
	j := self.G(key)
	if j == nil {
		return 0, false
	}
	return j.AsUint8()
}

func (self *JsonParser) GUint16(key string) (uint16, bool) {
	j := self.G(key)
	if j == nil {
		return 0, false
	}
	return j.AsUint16()
}

func (self *JsonParser) GUInt32(key string) (uint32, bool) {
	j := self.G(key)
	if j == nil {
		return 0, false
	}
	return j.AsUint32()
}

func (self *JsonParser) GUint64(key string) (uint64, bool) {
	j := self.G(key)
	if j == nil {
		return 0, false
	}
	return j.AsUint64()
}

func (self *JsonParser) GFloat32(key string) (float32, bool) {
	j := self.G(key)
	if j == nil {
		return float32(0.0), false
	}
	return j.AsFloat32()
}

func (self *JsonParser) GFloat64(key string) (float64, bool) {
	j := self.G(key)
	if j == nil {
		return 0.0, false
	}
	return j.AsFloat64()
}

func (self *JsonParser) GString(key string) (string, bool) {
	j := self.G(key)
	if j == nil {
		return "", false
	}
	return j.AsString()
}

func (self *JsonParser) GBool(key string) (bool, bool) {
	j := self.G(key)
	if j == nil {
		return false, false
	}
	return j.AsBool()
}
