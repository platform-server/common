package netext

import (
	"crypto/tls"
	//"fmt"
	"net"
	"net/http"
	"strconv"
)

// get listen port of listener
func GetListenPort(l net.Listener) uint16 {
	if l == nil {
		return 0
	}

	_, portStr, _ := net.SplitHostPort(l.Addr().String())
	portInt, _ := strconv.Atoi(portStr)
	return uint16(portInt)
}

// http server. support listen on random port and get the port
// if set port to 0(i.e. Addr ":0"), random port will be used
//
// s := HttpServer{Server:http.Server{Addr:":0"},}
// s.Listen()
// port := s.GetListenPort()
// s.Serve1()
//
type HttpServer struct {
	http.Server

	listener net.Listener
}

// get listen port
func (self *HttpServer) GetListenPort() uint16 {
	return GetListenPort(self.listener)
}

// http listen
func (self *HttpServer) Listen() error {
	addr := self.Addr
	if addr == "" {
		addr = ":http"
	}

	l, e := net.Listen("tcp", addr)
	if e != nil {
		return e
	}
	self.listener = l
	return nil
}

// https listen
func (self *HttpServer) ListenTLS(certFile, keyFile string) error {
	addr := self.Addr
	if addr == "" {
		addr = ":https"
	}
	config := &tls.Config{}
	config.MinVersion = tls.VersionTLS10
	if self.TLSConfig != nil {
		*config = *self.TLSConfig
	}
	if config.NextProtos == nil {
		config.NextProtos = []string{"http/1.1"}
	}

	var err error
	config.Certificates = make([]tls.Certificate, 1)
	config.Certificates[0], err = tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		return err
	}

	conn, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}

	self.listener = tls.NewListener(conn, config)
	return nil
}

// serve
func (self *HttpServer) Serve1() {
	self.Serve(self.listener)
}

func (self *HttpServer) Close() {
	if self.listener != nil {
		self.listener.Close()
		self.listener = nil
	}
}

/*func main() {
	l, e := net.Listen("tcp", ":0")
	if e != nil {
		fmt.Println(e)
		return
	}
	defer l.Close()

	fmt.Println(l.Addr().Network(), l.Addr().String())

	_, port, _ := net.SplitHostPort(l.Addr().String())
	fmt.Println(port)
}*/
