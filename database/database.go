package database

import (
	"database/sql"
	"fmt"
	"git.tap4fun.com/platform-server/common/database/gomapper"
	_ "github.com/go-sql-driver/mysql"
)

const (
	kFmtDataSourceName = "%s:%s@tcp(%s:%d)/%s?timeout=%s"
	kDriverName        = "mysql"
	kMaxConns          = 100
	kConnTimeout       = "10s"
)

// new mysqldb object
func NewMysqlDB(
	host string, port uint16, database string,
	user string, password string, args ...string) (db *sql.DB, err error) {

	return NewGoSqlDriverDB(host, port, database, user, password, args...)
}

// timeout is set as kConnTimeout, no need to change
// other args in the format of: key=value, and must be url.QueryEscape'ed!
func NewGoSqlDriverDB(
	host string, port uint16, database string,
	user string, password string, args ...string) (*sql.DB, error) {

	protocal := fmt.Sprintf(kFmtDataSourceName,
		user, password, host, port, database, kConnTimeout)

	// append args to protocal
	for _, arg := range args {
		protocal += "&" + arg
	}

	db, err := sql.Open(kDriverName, protocal)
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(kMaxConns)
	db.SetMaxIdleConns(5)
	return db, nil
}

func NewGoMapper(db *sql.DB, xml []byte) (*gomapper.GoMapper, error) {
	return gomapper.NewGoMapper(db, xml)
}

func NewGoMapperByFile(db *sql.DB, xml string) (*gomapper.GoMapper, error) {
	return gomapper.NewGoMapperByFile(db, xml)
}
