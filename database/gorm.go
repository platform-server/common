package database

import (
	"fmt"
	goconfig "git.tap4fun.com/platform-server/common/config"
	"git.tap4fun.com/platform-server/mthird/utils"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func NewGormDB(conf goconfig.IConfig) (*gorm.DB, error) {
	ip, err1 := conf.GetString("Database", "host")

	port, err2 := conf.GetString("Database", "port")

	user, err3 := conf.GetString("Database", "user")

	password, err4 := conf.GetString("Database", "password")

	dbname, err5 := conf.GetString("Database", "database")
	if err := utils.GetFirstError(err1, err2, err3, err4, err5); err != nil {
		return nil, err
	}

	return NewGormDB1(ip, port, user, password, dbname)
}

func NewGormDB1(ip, port, user, password, dbname string) (*gorm.DB, error) {
	url := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?%s", user, password, ip, port, dbname, "charset=utf8&parseTime=True")

	mydb, err := gorm.Open("mysql", url)
	if err != nil {
		return nil, err
	}
	// mydb.LogMode(true)
	return mydb, nil
}

func NewGormDB2(user, name, db string) (*gorm.DB, error) {
	url := fmt.Sprintf("%s:%s@/%s?charset=utf8&parseTime=True&loc=Local", user, name, db)
	mydb, err := gorm.Open("mysql", url)
	if err != nil {
		return nil, err
	}
	// mydb.LogMode(true)
	return mydb, nil
}
